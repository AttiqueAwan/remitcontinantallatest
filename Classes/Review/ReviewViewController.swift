//
//  ReviewViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import Jelly
import DropDown

class ReviewViewController: UIViewController {

    @IBOutlet weak var userinfoView: UIView!
    @IBOutlet weak var lblSendingAmount: UILabel!
    @IBOutlet weak var lblReceivingAmount: UILabel!
    @IBOutlet weak var lblBeneName: UILabel!
    @IBOutlet weak var lblBenePhone: UILabel!
    @IBOutlet weak var lblBeneCountry: UILabel!
    @IBOutlet weak var lblTransactionFee: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var txtSelectReason: UITextField!
    @IBOutlet weak var btnSelectReason: UIButton!
    @IBOutlet weak var PayoutMethod: UILabel!
    @IBOutlet weak var lblExchangeRate: UILabel!
    @IBOutlet weak var SendingReasonView: UIView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var txtSendingPaymentMethod: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSendingPaymentMethod: UIButton!
    
    @IBOutlet weak var lblCardNumber: UILabel!
    
    @IBOutlet weak var viewCardNumber: ViewStyle!
    var isTermAndConditionChecked = false
    
    
    var jellyAnimator: Animator?
    let round = RoundedCorner()
    
    var parms:[String:Any]!
    var Reason = [String]()
    
    let SendingReasonDropDown = DropDown()
    
    let PaymentMethodDropDown = DropDown()
    var PaymentMethodListResponse : SendingPaymentMethodListResult!
    var PaymentMethodList = [String]()
    var PaymentMethodindex = 0
    
    var SendingPurposeList:ListingTypesresult!
    var transactionListResponse : TransactionList!
    var UserInfo:RecipientListResult!
    
    let uc = UtilitySoftTechMedia()
    
    var UpdateTrans : TransactionListresult!
  
    override func viewWillAppear(_ animated: Bool) {
        
        if(isTermAndConditionChecked)
        {
            self.InsertTransaction()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
       
        if PayoutLocation != "" && PayoutLocationAddress != "" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {[weak self] in
                self?.populatData()
                
            }
            
        }
                
                
                
                // Do any additional setup after loading the view.
            //ala
            }
            
    func populatData() {
        self.GetSendingPurpose()
        self.getSendingPaymentMethods()
        
        if PayoutLocation != "" && PayoutLocationAddress != "" {
            self.lblBeneCountry.text = "\(PayoutLocation)\n\(PayoutLocationAddress)"
        }
        
        
        
        var paymentMethodName = ""
        if PayoutMethodobj == "9" {
            paymentMethodName = "cash"
            lblBenePhone.isHidden = false
            viewCardNumber.isHidden = true
        }
        else if PayoutMethodobj == "10" {
            paymentMethodName = "Bank"
            lblBenePhone.isHidden = false
            viewCardNumber.isHidden = true
        }
        else if PayoutMethodobj == "751" {
            paymentMethodName = "Wallet"
            lblBenePhone.isHidden = true
            viewCardNumber.isHidden = false
            
        }
        else if PayoutMethodobj == "752" {
            paymentMethodName = "Bill Payment"
            lblBenePhone.isHidden = true
            viewCardNumber.isHidden = false
        }
        else if PayoutMethodobj == "753" {
            paymentMethodName = "Prepaid Card"
            lblBenePhone.isHidden = true
            viewCardNumber.isHidden = false
            
        }
        
        if UserInfo.PrepaidCardNumber != nil && UserInfo.PrepaidCardNumber != "" {
            lblCardNumber.text = UserInfo.PrepaidCardNumber
        }else if paymentMethodName == "Bank" {
            lblCardNumber.text = UserInfo.BeneAccountNumber
        }else if paymentMethodName == "cash"{
            lblCardNumber.text = UserInfo.BenePhone
        }else {
            lblCardNumber.text = "N/A"
        }
        
        self.PayoutMethod.text = paymentMethodName
        Initial_Payment_Method = paymentMethodName
        
        self.PaymentMethodDropDown.anchorView = self.btnSendingPaymentMethod
        self.PaymentMethodDropDown.dataSource = self.PaymentMethodList
        
        PaymentMethodDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            PaymentMEthodCOde = (self.PaymentMethodListResponse?.PaymentMethodList?[index].PaymentMethodCode)!
            self.txtSendingPaymentMethod.text = item
            
        }
        
        
        SendingReasonDropDown.anchorView = self.btnSelectReason
        SendingReasonDropDown.dataSource = Reason
        
        
        SendingReasonDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            
            self.txtSelectReason.text = item
            
        }
        
        //crash from recepient list
        if let SendingCurrency = self.parms["Sending_Currency"]{
            if let SendingAmount = self.parms["Sending_Amount"]{
                
                
                self.lblSendingAmount.text = "\((SendingAmount as? String)!) \((SendingCurrency as? String)!)"
                
            }
            if let TransferFee = parms["Service_Charges"]{
                
                self.lblTransactionFee.text = "\((TransferFee as? String)!) \((SendingCurrency as? String)!)"
            }
            
            
            if let TotalAmount = parms["Total_Amount"]{
                
                self.lblTotalAmount.text = "\((TotalAmount as? String)!) \((SendingCurrency as? String)!)"
                
            }
            
        }
        
        
        if let ReceivingCUrrency = self.parms["CurrencyIsoCode"]{
            
            if let ReceivingAmount = self.parms["Receiving_Amount"]{
                
                self.lblReceivingAmount.text = "\((ReceivingAmount as? String)!) \((ReceivingCUrrency as? String)!)"
                
            }
            
            if let SendingCurrency = self.parms["Sending_Currency"]{
                
                if let ExchangeRate = self.parms["Exchange_Rate"]{
                    
                    self.lblExchangeRate.text = "1 \((SendingCurrency as? String)!) = \(String(format:"%.1f", ExchangeRate as! CVarArg)) \((ReceivingCUrrency as? String)!)"
                    
                }
            }
        }
        
        //no crashe
        if let ReceiverName = self.UserInfo!.BeneName{
            
            self.lblBeneName.text = ReceiverName
            
        }
        if let ReceiverPhone = self.UserInfo!.BenePhone{
            
            self.lblBenePhone.text = ReceiverPhone
        }
        
    }
    
    
    @IBAction func btnSendingPaymentMethodClick(_ sender: Any) {
        
        self.PaymentMethodDropDown.show()
        
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        //        self.navigationController?.popViewController(animated: true)
        //         ReviewViewController
        
//        if let viewControllers = self.navigationController?.viewControllers {
//            for vc in viewControllers {
//
//                if vc.isKind(of: RecipientViewController.self) {
//                    print("isKind find VC")
//                    navigationController?.popToViewController(vc, animated: true)
//                    return
//
//                }
//            }
//        }
        
        self.navigationController?.popViewController(animated: true)
//        self.navigationController?.pushViewController(RecipientViewController(), animated: true)
        
        
        
    }
    
    
    @IBAction func btnSelectReasonClick(_ sender: Any) {
        
        self.SendingReasonDropDown.show()
        
    }
    
    
    @IBAction func btnConfirmClick(_ sender: Any) {
        
       
            if((self.txtSelectReason.text?.isEmpty)!){
            
            uc.errorSuccessAler("", "Please select sending reason", self)
            
        }else if(ApiUrls.TotalDoc == 0){
            ApiUrls.TotalDoc = 0
            let alertController = UIAlertController(title: "", message: "Please add government issued to continue with your transaction. Please add your Document to make a transaction", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Ok", style: .default, handler: { (action:UIAlertAction) in
                
                self.performSegue(withIdentifier: "AddDocID", sender: nil)
                
            })
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            
                if(Initial_Payment_Method == "Bank")
                {
                    
                    var customBlurFadeInPresentation = FadePresentation()
                    customBlurFadeInPresentation.presentationUIConfiguration.backgroundStyle = .blurred(effectStyle: .dark)
                    customBlurFadeInPresentation.presentationSize.width = .custom(value: 320)
                    customBlurFadeInPresentation.presentationSize.height = .custom(value: 280)
                    
                    
                    customBlurFadeInPresentation.presentationUIConfiguration.cornerRadius = 15.0
                    let viewcontroller =   self.storyboard?.instantiateViewController(withIdentifier: "TermsPopUp") as? TermsPopUpViewController
                
                    self.jellyAnimator = Animator(presentation:customBlurFadeInPresentation)
                    self.jellyAnimator?.prepare(presentedViewController: viewcontroller!)
                    //self.present(viewcontroller!, animated: true, completion: nil)
                    self.navigationController!.pushViewController(viewcontroller!, animated: true)
            
                }else{
                    self.InsertTransaction()
                }
            
        }
        
       
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if(segue.identifier == "PAYATLOCATION"){
            
            let Payat = segue.destination as? PayAtLocationVC
            
            if(self.UpdateTrans !=  nil)
            {
               
                Payat?.TransDetail = self.UpdateTrans
               
            }
            
        }else if(segue.identifier == "Payment"){
            
            let completeTrans = segue.destination as? PaymentViewController
            
            if(self.UpdateTrans !=  nil){
                
                let url =  "\((self.UpdateTrans.PaymentPageURL!))?id=\((self.UpdateTrans.PaymentID)!)"
                completeTrans?.weburl = url
            }
            
        
        }
        else if(segue.identifier == "Bank")
               {
                    let BankPage = segue.destination as? DepositToBankVC
                   BankPage?.UpdateTrans =  self.UpdateTrans
               }
      
        
    }
    
    
    func getSendingPaymentMethods() {
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"CountryIso3Code":CountryIso3Code]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.sendingmethods, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getSendingPaymentMethods()
                return
            }
            
            self.PaymentMethodListResponse = SendingPaymentMethodListResult(JSONString:result)
            
            if self.PaymentMethodListResponse?.myAppResult?.Code == 0 {
                
                for i in 0..<(self.PaymentMethodListResponse?.PaymentMethodList?.count)!
                {
                   
                   if((self.PaymentMethodListResponse?.PaymentMethodList?[i].PaymentMethodName!)!.lowercased() == "bank")
                   {
                       self.PaymentMethodList.append("Online Bank Transfer")
                   }
                   else
                   {
                    self.PaymentMethodList.append((self.PaymentMethodListResponse?.PaymentMethodList?[i].PaymentMethodName!)!)
                    }
                }
                
                self.PaymentMethodDropDown.dataSource = self.PaymentMethodList
                self.txtSendingPaymentMethod.text = self.PaymentMethodList[0]
                PaymentMEthodCOde = (self.PaymentMethodListResponse?.PaymentMethodList?[0].PaymentMethodCode!)!
            }else if self.PaymentMethodListResponse?.myAppResult?.Code == 101 {
                
                
                self.uc.logout(self)
                
                
            }else if self.PaymentMethodListResponse?.myAppResult?.Code == 102 {
                self.uc.errorSuccessAler("", "Payment Methods not found", self)
                
                
            }else{
                
                if(self.PaymentMethodListResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.PaymentMethodListResponse?.myAppResult?.Message)!, self)
                }
            }
            
        }
    }
    
    func InsertTransaction(){
        
        
        
        if(PaymentMEthodCOde == "Cash"){
            
            UserInfo!.BeneIBAN = ""
            UserInfo!.BeneAccountNumber! = ""
            UserInfo!.BeneBankBranchCode! = ""
            UserInfo!.BeneBankCode! = ""
            UserInfo.BeneBankName! = ""
            UserInfo!.BeneBranchName! = ""
            UserInfo!.BeneName! = ""
            
        }
        
        let parmss =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                       "Token":ApiUrls.AuthToken,
                       "AppID":ApiUrls.AppID,
                       "PaymentNumber":"",
                       "PayerID":(parms["PayerId"] as? String)!,
                       "PaymentLimit":"",
                       "PaymentStatus":"",
                       "PaymentDate":Date(),
                       "SendingCountry":"",
                       "SendingCountryIso3Code":"",
                       "ReceivingCountry":"",
                       "ReceivingCountryIso3Code":"",
                       "SendingCurrency":"",
                       "ReceivingCurrency":"",
                       "ServiceCharges":(self.parms["Service_Charges"] as? String)!,
                       "ExchangeRate":"",
                       "SenderName":"",
                       "BeneName":"",
                       "BenePhone":"",
                       "PayerBranchCode":"",
                       "PayerName":"",
                       "PayOutBranchName":"",
                       "SendingPaymentMethod":PaymentMEthodCOde,
                       "PaymentMethod":Initial_Payment_Method,
                       "PayoutBranchAddress":PayoutLocationAddress,
                       "PayOutBranchCode":(parms["Payout_branchCode"] as? String)!,
                       "PayInCurrencyCode":(self.parms["Sending_Currency"] as? String)!,
                       "PayOutCurrencyCode":(self.parms["CurrencyIsoCode"] as? String)!,
                       "PayInAmount":(self.parms["Sending_Amount"] as? String)!,
                       "PayOutAmount":(self.parms["Receiving_Amount"] as? String)!,
                       "SendingReason":self.txtSelectReason.text!,
                       "BeneID":UserInfo!.BeneID!,
                       "ReceiverBankAccountNumber":UserInfo!.BeneAccountNumber!,
                       "ReceiverBankAccountTitle":UserInfo!.BeneName!,
                       "ReceiverBankIBAN":UserInfo!.BeneIBAN!,
                       "ReceiverBankCode":UserInfo!.BeneBankCode!,
                       "ReceiverBankName":UserInfo!.BeneBankName!,
                       "ReceiverBranchName":UserInfo!.BeneBranchName!,
                       "ReceiverBankBranchCode":UserInfo!.BeneBankCode!,
                       "MobileAppPayment":"",
                       "SenderNationality":"",
                       "DeviceType":"IOS"]as [String : Any]
        
       
        
        print("parms : \(parmss)")
        
        uc.webServicePosthttp(urlString: ApiUrls.InsertTransaction, params:parmss , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.InsertTransaction()
                return
            }
            
            self.transactionListResponse = TransactionList(JSONString:result)
            
            if self.transactionListResponse?.myAppResult?.Code == 0 {
                
                self.UpdateTrans = self.transactionListResponse.AceTransaction
                
                if(PaymentMEthodCOde.lowercased() == "cash")
                {
                    self.uc.errorSuccessAler("", "Please go to nearest Top Connect Branch to pay at location", self)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                         self.performSegue(withIdentifier: "PAYATLOCATION", sender: nil)
                    })
                    
                   
                }
                else if(PaymentMEthodCOde.lowercased() == "bank")
                {
                    
                    self.performSegue(withIdentifier: "Bank", sender: nil)
                    
                }
                else
                {
                    self.performSegue(withIdentifier: "Payment", sender: nil)
                }
                
                
            }else if self.transactionListResponse?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.transactionListResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.transactionListResponse?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }
    
    
    
    func GetSendingPurpose(){
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ListType":"SENDINGPURPOSE"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetLists, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetSendingPurpose()
                return
            }
            
            self.SendingPurposeList = ListingTypesresult(JSONString:result)
            
            if  self.SendingPurposeList?.myAppResult?.Code == 0 {
                
                for i in 0..<(self.SendingPurposeList.ListType?.count)!{
                    
                    self.Reason.append(self.SendingPurposeList.ListType![i].Text!)
                }
                self.txtSelectReason.text = self.SendingPurposeList.ListType![0].Text!
                self.SendingReasonDropDown.dataSource = self.Reason
                self.SendingReasonDropDown.reloadAllComponents()
                
            }else if  self.SendingPurposeList?.myAppResult?.Code == 102{
                
                self.uc.errorSuccessAler("", "Sending purpose not found", self)
                
            }else{
                
                if( self.SendingPurposeList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.SendingPurposeList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
    

}

