//
//  TermsPopUpViewController.swift
//  TopConnect
//
//  Created by Asad Zahoor on 24/03/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import FRHyperLabel

class TermsPopUpViewController: UIViewController {

    @IBOutlet weak var termLbl: FRHyperLabel!

    @IBOutlet weak var checkBox: UIImageView!
    var isChecked = false

    
    let uc = UtilitySoftTechMedia()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "By creating a transaction you agree with the Privacy Policy and Terms & Conditions of Remit Continental.")
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:46,length:14))
         
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:65,length:18))
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:83,length:11))
        
        //self.btnTermsAndCondition.attributedText = myMutableString
        self.termLbl.attributedText = myMutableString
        
        
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            if(substring == "Privacy Policy"){
                
                guard let url = URL(string: "https://www.remitcontinental.com/page.php?slug=terms-and-conditions") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            else if(substring == "Terms & Conditions"){
                
                guard let url = URL(string: "https://www.remitcontinental.com/contact.php") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            
        }
    
        self.termLbl.setLinksForSubstrings(["Terms & Conditions","Privacy Policy"], withLinkHandler: handler)
        
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    @IBAction func checkBoxImgBtn(_ sender: Any) {
        
        if(isChecked)
        {
            isChecked = false
            self.checkBox.image = UIImage(named: "uncheck-ico.png")
        }else{
            isChecked = true
            self.checkBox.image = UIImage(named: "red check icon.png")
        }
    }
    @IBAction func agreeBtn(_ sender: Any) {
        
        if(isChecked)
        {
            let i = navigationController?.viewControllers.firstIndex(of: self)
            let previousViewController = navigationController?.viewControllers[i!-1]
            
            if((previousViewController?.isKind(of: ReviewViewController.self))!)
            {
                let reviewVC = previousViewController as! ReviewViewController
                reviewVC.isTermAndConditionChecked = true
                self.navigationController?.popToViewController(reviewVC, animated: true)
            }
            //self.navigationController?.popViewController(animated: true)
            
        }else{
            uc.makeToast(message: "Must agree with terms and conditions")
        }
        }
    
    @IBAction func cancelBtn(_ sender: Any) {
        
       self.navigationController?.popViewController(animated: true)
    }
    
    


}


