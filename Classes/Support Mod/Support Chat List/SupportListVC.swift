//
//  SupportListVC.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import UIKit

class SupportListVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var SupportTblVIew: UITableView!
    @IBOutlet weak var NoLogView: UIView!
    
    let uc = UtilitySoftTechMedia()
    var SupportModelResp : SupportModelResponse!
    var PaymentId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        SupportTblVIew.delegate = self
        SupportTblVIew.dataSource = self
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Show the Navigation Bar
   
        self.GetSupportLoglist()

      
    }
    
 
    @IBAction func backBtnFunc(_ sender: UIButton) {
    
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: SupportListVC.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func AddNewComplaint(_ sender: UIButton) {
        if(self.SupportModelResp != nil )
        {
        if (self.SupportModelResp.SupportModelList != nil &&  self.SupportModelResp.SupportModelList!.count > 0)
        {
            if(self.SupportModelResp.SupportModelList![0].Status! == "Open")
            {
            DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: "You already have an active support. Please view that chat.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "View", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
              
                SupportModelDATA = self.SupportModelResp.SupportModelList![0]
                self.performSegue(withIdentifier: "Chat", sender: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            }
            }
            else
            {
                self.performSegue(withIdentifier: "New", sender: nil)
            }
        }
        else
        {
            self.performSegue(withIdentifier: "New", sender: nil)
        }
            
        }
        else
        {
            self.performSegue(withIdentifier: "New", sender: nil)
        }
       
    }
    
    

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
     }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        SupportModelDATA = self.SupportModelResp.SupportModelList![indexPath.row]
        self.performSegue(withIdentifier: "Chat", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (self.SupportModelResp) != nil else {
            return 0
        }
        
        guard let count = self.SupportModelResp.SupportModelList?.count else {
            return 0
        }
        return count
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //animation 1
        cell.alpha = 0.5
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.05,
            animations: {
                cell.alpha = 1
                self.view.layoutIfNeeded()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SupportTblVIew.dequeueReusableCell(withIdentifier: "cell") as! SupportLogCell
       
        _ = self.SupportModelResp.SupportModelList![indexPath.row].ComplaintType
        let ComplaintDate = self.SupportModelResp.SupportModelList![indexPath.row].ComplaintDate
        let Body = self.SupportModelResp.SupportModelList![indexPath.row].Body
        let Status = self.SupportModelResp.SupportModelList![indexPath.row].Status
        let User = uc.getUserInfo()
        let name = (User?.UserInfo?.FirstName)!
        
        
        cell.PrefixOfUserName.text = String(name.first!)
        cell.LblSupportType.text = "Me"
        cell.LblComplaintBody.text = Body
        if(Status == "Open")
        {
          cell.LblComplaintStatus.text = "Active"
        }
        else
        {
            cell.LblComplaintStatus.text = Status
        }
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let Date = dateformatter.date(from: ComplaintDate!)
        dateformatter.dateFormat = "MMM,dd yyyy"
        let ComplainDAte = dateformatter.string(from: Date!)
        cell.LblComplaintDate.text = ComplainDAte
        if(self.SupportModelResp.SupportModelList![indexPath.row].isReadSender == "0")
        {
           cell.NewView.isHidden = false
           cell.contentView.backgroundColor = #colorLiteral(red: 1, green: 0.05036290358, blue: 0.1484533791, alpha: 0.3468535959)
        }
        else
        {
            cell.NewView.isHidden = true
            cell.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        return cell
    }
    
    func GetSupportLoglist(){
        
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetComplaintlist, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetSupportLoglist()
                return
            }
            
            self.SupportModelResp = SupportModelResponse(JSONString:result)
            
            if self.SupportModelResp?.myAppResult?.Code == 0 {

                if(self.SupportModelResp.SupportModelList != nil && (self.SupportModelResp.SupportModelList?.count)! > 0){

                    self.SupportTblVIew.isHidden = false
                    self.NoLogView.isHidden = true
                    self.SupportTblVIew.reloadData()


                }else{

                    self.SupportTblVIew.isHidden = true
                    self.NoLogView.isHidden = false
        
                }


            }else if self.SupportModelResp?.myAppResult?.Code == 101 {

                self.uc.logout(self)

            }else{
                self.NoLogView.isHidden = false
                if(self.SupportModelResp?.myAppResult?.Message == nil){

                    self.uc.errorSuccessAler("Error", result, self)

                }else{
                    self.uc.errorSuccessAler("Error", (self.SupportModelResp?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    
    

}
