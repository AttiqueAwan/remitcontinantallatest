//
//  ChattingVC.swift
//  Bakhter Money
//
//  Created by Mac on 30/05/2019.
//  Copyright © 2019 BakhterMoney. All rights reserved.
//

import UIKit
 var IsComplaintRead = false
 var SupportModelDATA : SupportModel!

class ChattingVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblMessage: UITableView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    
    
    let uc = UtilitySoftTechMedia()
    
    var complanceresult : Complanceresult!
    
    var chatID = 0
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        tblMessage.delegate = self
        tblMessage.dataSource = self
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.lblTitle.text = SupportModelDATA.ComplaintType!
        self.GetComplance()
        IsComplaintRead = true
        NotificationCenter.default.addObserver(self, selector: #selector(Reload(_:)), name: ApiUrls.ComplaintNotification, object: nil)
    }
    
  
    
    
    
    @objc  func Reload(_ notification:NSNotification) {
        DispatchQueue.main.async {
            self.GetComplance()
        }
        
        IsComplaintRead = true
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: ChattingVC.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSendClick(_ sender: Any) {
        
        if(self.txtMessage.text.isEmpty){
            
            uc.errorSuccessAler("", "Please type message to send", self)
            
        }else{
            
            self.view.endEditing(true)
            self.SaveComplance()
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }

}
