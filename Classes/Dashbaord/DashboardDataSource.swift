//
//  DashboardDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 24/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension DashboardViewController{
    
    func gettingDashboard(){
        
        let authToken = uc.getAuthToken()
        
        if(authToken == nil){
            
            self.uc.errorSuccessAler("", "Server can't give rights to this user id!", self)
            
        }else{
            
            let parms =  ["Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ID":(authToken?.AuthToken?.user_id)!]as [String : Any]
            
            uc.webServicePosthttp(urlString: ApiUrls.GetDashbaord, params:parms , message: "Loading...", currentController: self){result in
                
                if(result == "fail")
                {
                    self.gettingDashboard()
                    return
                }
                
                self.DashboardResponce = Dashboardresult(JSONString:result)
                
                if self.DashboardResponce?.myAppResult?.Code == 0 {
                    
                    UserDefaults.standard.set(result, forKey: "UserLogin")
                    
                    ApiUrls.TotalDoc = (self.DashboardResponce.dashboard?.TotalIDoc)!
                    
                    if(self.DashboardResponce.dashboard!.TotalUnreadMessages > 0)
                    {
                        self.notificationLbl.isHidden = false
                        self.notificationLbl.text = "\(self.DashboardResponce.dashboard!.TotalUnreadMessages!)"
                    
                        
                    }else{
                        self.notificationLbl.isHidden = true
                    }
            
                }else if self.DashboardResponce?.myAppResult?.Code == 101 {
                    
                    self.uc.logout(self)

                }else{
                    
                    if(self.DashboardResponce.myAppResult?.Message == nil){
                        
                      //  self.uc.errorSuccessAler("", result, self)
                        
                    }else{
                      //  self.uc.errorSuccessAler("", (self.DashboardResponce.myAppResult?.Message)!, self)
                    }
                }
                
            }
        }
    }
    
    
}
