//
//  DashboardViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import Jelly
var RecipientSelected = false
var SendTransactionAfterProfileComplete = false
var Dashboard = false
class DashboardViewController: UIViewController {

    
    @IBOutlet weak var TransactionView: UIView!
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var RecipientView: UIView!
    @IBOutlet weak var DocumentView: UIView!
    @IBOutlet weak var DashboardView: UIView!
    
    //Tab bar buttons and label outlets
    
    @IBOutlet weak var btnTransfer: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnRecipients: UIButton!
    @IBOutlet weak var BtnDocs: UIButton!
    @IBOutlet weak var imgSend: UIImageView!
    
    
    @IBOutlet weak var notificationLbl: UILableX!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCustomerID: UILabel!
    //End of tab bar outlets
    
    //Side Menu Presentation
    var jellyAnimator: Animator?
    let round = RoundedCorner()
    
    //Manage api calling and alert
    let uc = UtilitySoftTechMedia()
    
    var DashboardResponce : Dashboardresult!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        Dashboard = true
          UpdateRecipientPickupMoneyInfo = false
          let Hmac = "cf104d6dd05073dee7cdb6c19c444ff13310".digest(.sha256, key: ApiUrls.PublicKey)
         print(Hmac)
        let message = "cf104d6dd05073dee7cdb6c19c444ff13310"
        let hmacResult:String = message.hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        print(hmacResult)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {[weak self] in
            
            self?.TransactionView.isHidden = true
            self?.ProfileView.isHidden = true
            self?.RecipientView.isHidden = true
            self?.DocumentView.isHidden = true
            self?.DashboardView.isHidden = false
//            let userInfo = self?.uc.getUserInfo()
            
           
            
            
//            if userInfo != nil {
//                self?.lblUserName.text = userInfo?.UserInfo?.FullName
//                self?.lblCustomerID.text = "Customer iD: \(String(describing: userInfo!.UserInfo!.CustomerID!))"
//            }
        }
        let userInfo = self.uc.getUserInfo()
        
        if(userInfo != nil)
        {
            
            var clientId = "RC"
            
            let userCountryISO3 = userInfo?.UserInfo?.CountryIsoCode
            
            if(userCountryISO3 != nil && userCountryISO3 != "")
            {
                let country = uc.getCountryByISO3(userCountryISO3!)
                
                if(country.Iso2Code != nil )
                {
                    if(country.Iso2Code == "GB")
                    {
                        clientId += "UK"
                    }else{
                        clientId += country.Iso2Code!
                    }
                }
            }
            var customerId = "\(userInfo?.UserInfo!.CustomerID! ?? 0)"
            
            
            if(customerId.count == 2 )
            {
                customerId = "00\(customerId)"
            }else if(customerId.count == 3 )
            {
                customerId = "0\(customerId)"
            }
            
            let ID = customerId
            
            
            self.lblUserName.text = userInfo?.UserInfo?.FullName
            self.lblCustomerID.text = "Customer ID: \(ID)"
            
        }

        
//        btnTransfer.setImage(UIImage(named: "transfer-icons-enable"), for: .normal)
//        lblTransfer.textColor = #colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.2470588235, alpha: 1)
        
        
        
//        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1)
       
         UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgSend.isUserInteractionEnabled = true
        imgSend.addGestureRecognizer(tapGestureRecognizer)
        NotificationCenter.default.addObserver(self, selector: #selector(Reload(_:)), name: ApiUrls.ComplaintNotification, object: nil)
        
        // Manage Dashbaord view when back form other controller
         NotificationCenter.default.addObserver(self, selector: #selector(showDashboar(_:)), name: ApiUrls.ShowDashboardNotification, object: nil)
        
       animation()
        
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        if !(uc.ProfileCompleteCheck()){
                    
                    let alertCOntroller = UIAlertController(title: "", message: "Your profile is 25% completed.\nPlease complete your profile to make a transaction", preferredStyle: UIAlertController.Style.alert)
                    
                    let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
        //                SendTransactionAfterProfileComplete = true
                        self.performSegue(withIdentifier: "ProfileUpdate", sender: nil)
                    })
                    alertCOntroller.addAction(action)
                    self.present(alertCOntroller,animated: true,completion: nil)
                    
                }else{
                    RecipientSelected = false
                    
                    self.performSegue(withIdentifier: "sendNowClick", sender: nil)
                }
        
    }
    func animation(){
        UIView.animateKeyframes(withDuration: 0.8, delay: 1, options: .calculationModeLinear, animations: {
           
            self.lblCustomerID.center.x += self.view.bounds.width
            self.lblUserName.center.x += self.view.bounds.width
            
        }, completion:{finished in
            self.view.layer.removeAllAnimations()
        })
    }

    @IBAction func btnSupport(_ sender: UIButton) {
//        self.performSegue(withIdentifier: "SupportList", sender: nil)
    }
    
    @objc  func showDashboar(_ notification:NSNotification) {
        
        if let userInfo = notification.userInfo  {
             print(userInfo) // [AnyHashable("progressPercentage"): 0.82530790852915281]
          if let viewTagNo = userInfo["viewtag"] as? Int {
            if viewTagNo > 953 {
                //Show Recipient List
                DispatchQueue.main.async {
                    self.TransactionView.isHidden = true
                    self.ProfileView.isHidden = true
                    self.RecipientView.isHidden = false
                    self.DocumentView.isHidden = true
                    self.DashboardView.isHidden = true
                }
            }
          }
        }else {
            DispatchQueue.main.async {
                self.TransactionView.isHidden = true
                self.ProfileView.isHidden = true
                self.RecipientView.isHidden = true
                self.DocumentView.isHidden = true
                self.DashboardView.isHidden = false
            }
        }
    }
    
  @objc  func Reload(_ notification:NSNotification) {
      DispatchQueue.main.async {
          self.gettingDashboard()
      }
  }
    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {[weak self] in
            
//            self?.TransactionView.isHidden = true
//            self?.ProfileView.isHidden = true
//            self?.RecipientView.isHidden = true
//            self?.DocumentView.isHidden = true
//            self?.DashboardView.isHidden = false
            
            
        }
        
        Dashboard = true
//        if(SendTransactionAfterProfileComplete == true)
//        {
//            SendTransactionAfterProfileComplete = false
//            RecipientSelected = false
//            self.performSegue(withIdentifier: "sendNowClick", sender: nil)
//        }
//        else
//        {
//            SendTransactionAfterProfileComplete = false
//        }
        DispatchQueue.main.async {
            
            self.gettingDashboard()
            
        }
       
    }

    
    @IBAction func btnsidemenu(_ sender: Any) {
           let vc = storyboard?.instantiateViewController(withIdentifier: "P") as! SideMenuViewController
           self.navigationController?.pushViewController(vc, animated: true)
    }

    
    @IBAction func supportView(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Support2") as! SupportListVC
                 self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnProfileClick(_ sender: Any) {
//        TransactionView.isHidden = true
//        ProfileView.isHidden = false
//        RecipientView.isHidden = true
//        DocumentView.isHidden = true
//        DashboardView.isHidden = true
        
         
        

    }
    
    @IBAction func btnSendNowClick(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "Profile") as! ProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func SupportBtnView(_ sender: Any) {
        
    }
    @IBAction func RecipientFunc(_ sender: UIButton) {
        
        RecipientSelected = true
        TransactionView.isHidden = true
        ProfileView.isHidden = true
        RecipientView.isHidden = false
        DocumentView.isHidden = true
        DashboardView.isHidden = true
        
    
        NotificationCenter.default.post(name: ApiUrls.FromDashboardNotification, object: nil, userInfo: nil)
        
       

//        btnTransfer.setImage(UIImage(named: "transfer-normal"), for: .normal)
//        lblTransfer.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        btnRecipients.setImage(UIImage(named: "recipt-active"), for: .normal)
//        lblRecipients.textColor = #colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.2470588235, alpha: 1)
//        BtnDocs.setImage(UIImage(named: "Inact-doc"), for: .normal)
//        lblDocs.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        btnAccount.setImage(UIImage(named: "acount"), for: .normal)
//        lblAccount.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func TransactionListCLicked(_ sender: UIButton) {
        
            RecipientSelected = false
            TransactionView.isHidden = false
            ProfileView.isHidden = true
            RecipientView.isHidden = true
            DocumentView.isHidden = true
            DashboardView.isHidden = true
    
//            btnTransfer.setImage(UIImage(named: "transfer-icons-enable"), for: .normal)
//            lblTransfer.textColor = #colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.2470588235, alpha: 1)
//            btnRecipients.setImage(UIImage(named: "recipients"), for: .normal)
//            lblRecipients.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//            BtnDocs.setImage(UIImage(named: "doc file1"), for: .normal)
//            lblDocs.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//            btnAccount.setImage(UIImage(named: "user-icon-unactive"), for: .normal)
//            lblAccount.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       }

    @IBAction func btnDocCLicked(_ sender: UIButton) {
        TransactionView.isHidden = true
        ProfileView.isHidden = true
        RecipientView.isHidden = true
        DocumentView.isHidden = false
        DashboardView.isHidden = true
        
//        btnTransfer.setImage(UIImage(named: "transfer-normal"), for: .normal)
//        lblTransfer.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        btnRecipients.setImage(UIImage(named: "recipt"), for: .normal)
//        lblRecipients.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        BtnDocs.setImage(UIImage(named: "doc-active"), for: .normal)
//        lblDocs.textColor = #colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.2470588235, alpha: 1)
//        btnAccount.setImage(UIImage(named: "acount"), for: .normal)
//        lblAccount.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifer = segue.identifier  {
            if identifer == "ProfileUpdate" {
                let vc = segue.destination as? ProfileViewController
                vc?.isTrueComeFromDashboard = true
                
            }else if identifer == "sendNowClick" {
                print("sendNowClick")
            }
//            else if identifer == "R"{
//                let vc = segue.destination as? RecipientViewController
//                vc?.isFromDashboard = true
//            }
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let selectedPath = tableView.indexPathForSelectedRow else { return }
//        if let target = segue.destination as? UserViewController {
//            target.selectedUser = selectedPath.row
//        }
//    }
    
}
