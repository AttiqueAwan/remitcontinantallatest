//
//  DocumentListCell.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class DocumentListCell: UITableViewCell {

    @IBOutlet weak var Btnimage1: UIButton!
    @IBOutlet weak var btnImage2: UIButton!
    @IBOutlet weak var lblDocName: UILabel!
    @IBOutlet weak var lblDocDate: UILabel!
    @IBOutlet weak var docNumber: UILabel!
    @IBOutlet weak var LnlDocExpiryDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
