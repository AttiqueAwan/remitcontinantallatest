//
//  DocumentListViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class DocumentListViewController: UIViewController {
    
    @IBOutlet weak var noDocumentView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblDocumentList: UITableView!
    @IBOutlet weak var backBtnView: UIView!
    
    
    let uc = UtilitySoftTechMedia()
    
    var documentResponce : DocumentList!
    var DocumentUpdate : DocumentListResult!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       if(Dashboard == true)
        {
//            self.backBtnView.isHidden = true
        }
        else
        {
            self.backBtnView.isHidden = false
        }
        self.tblDocumentList.tableFooterView = UIView()
        self.getUserDocument()
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
//        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: nil)
    }
    

    @IBAction func AddBtn(_ sender: Any) {
        
        self.DocumentUpdate = nil
        self.performSegue(withIdentifier: "AddDocument", sender: nil)
    }
    
    @IBAction func btnAddFirstDocument(_ sender: Any) {
        
        self.DocumentUpdate = nil
        self.performSegue(withIdentifier: "AddDocument", sender: nil)
        
    }
    
    func getUserDocument(){
        
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.getDocumentList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getUserDocument()
                return
            }
            
            self.documentResponce = DocumentList(JSONString:result)
            
            if self.documentResponce?.myAppResult?.Code == 0 {
                
                
                if(self.documentResponce.AceDocList != nil && (self.documentResponce.AceDocList?.count)! > 0){
                    
                   
                    self.tblDocumentList.isHidden = false
                    self.noDocumentView.isHidden = true
                    self.tblDocumentList.reloadData()
                    
                }else{
                    
                    self.tblDocumentList.isHidden = true
                    self.noDocumentView.isHidden = false
                   
                }
                
                
                
            }else if self.documentResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else if self.documentResponce?.myAppResult?.Code == 102 {
                
                self.tblDocumentList.isHidden = true
                self.noDocumentView.isHidden = false
                
            }else{
                
                if(self.documentResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("", (self.documentResponce?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if(segue.identifier == "ShowImage2"){
            
            let destination = segue.destination as? DocumentImageViewController
            destination?.DocumentURL = (sender as? String)!
        }
        else
        {
        let destination = segue.destination as? AddDocuentViewController
        if(self.DocumentUpdate != nil){
            
             destination?.DocumentUpdate = self.DocumentUpdate
            
        }
        }
       
    }
  
    

}

extension DocumentListViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.DocumentUpdate = self.documentResponce.AceDocList![indexPath.row]
        self.performSegue(withIdentifier: "AddDocument", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //animation 1
        cell.alpha = 0.5
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.05,
            animations: {
                cell.alpha = 1
                self.view.layoutIfNeeded()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        guard (self.documentResponce) != nil else {
            return 0
        }
        
        guard let count = self.documentResponce.AceDocList?.count else {
            return 0
        }
        return count
        
    }
    
    
    //Show Image 1
    @objc func buttonSelected(sender: UIButton){
        print("Btn 1 clicked at index # \(sender.tag)")
       
            if let url = self.documentResponce.AceDocList![sender.tag].DocFileUrl{
                   print(url)
                  self.performSegue(withIdentifier: "ShowImage2", sender: url)
            }
       
    }
    //Show Image 2
    @objc func buttonSelected2(sender: UIButton){
         print("Btn 2 clicked at index # \(sender.tag)")
     
            if let url = self.documentResponce.AceDocList![sender.tag].DocFileBackUrl{
                print(url)
                if url != "" {
                    self.performSegue(withIdentifier: "ShowImage2", sender: url)
                }
                
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblDocumentList.dequeueReusableCell(withIdentifier: "DocumentListCell") as? DocumentListCell
        
        if let DocName = self.documentResponce.AceDocList![indexPath.row].DocTypeName{
            
            cell?.lblDocName.text = DocName
        }

        cell!.Btnimage1.tag = indexPath.row
        cell!.Btnimage1.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
        cell!.btnImage2.tag = indexPath.row
       
        cell!.btnImage2.addTarget(self, action: #selector(buttonSelected2), for: .touchUpInside)
        print(self.documentResponce.AceDocList![indexPath.row].DocFileBackUrl!)
        if self.documentResponce.AceDocList![indexPath.row].DocFileBackUrl == nil || self.documentResponce.AceDocList![indexPath.row].DocFileBackUrl == ""{
            cell!.btnImage2.alpha = 0
        }
        else
        {
            cell!.btnImage2.alpha = 1
        }
        
        if let DocName = self.documentResponce.AceDocList![indexPath.row].DocTypeName{
            
            if (DocName == "National ID Card" || DocName == "Resident Card") || DocName == "Driving License" {
                cell?.btnImage2.isHidden = false
            }else {
                cell?.btnImage2.isHidden = true
            }
        }
        
  
        if let DocNumber = self.documentResponce.AceDocList![indexPath.row].DocNumber{
            
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "Ref. No: \(DocNumber)")
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:8))

            cell?.docNumber.attributedText = myMutableString
        }

        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        var Todaydate = dateFormatter.date(from: "\((self.documentResponce.AceDocList![indexPath.row].DocExpireDate?.components(separatedBy: "T")[0])!)")
        
        if(Todaydate == nil){
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            Todaydate = dateFormatter.date(from: "\((self.documentResponce.AceDocList![indexPath.row].DocExpireDate?.components(separatedBy: "T")[0])!)")
            
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "Issue: \((self.documentResponce.AceDocList![indexPath.row].DocIssueDate?.components(separatedBy: "T")[0])!)  ")
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:6))

            
            var myMutableString2 = NSMutableAttributedString()
            myMutableString2 = NSMutableAttributedString(string:  "Expiry: \((self.documentResponce.AceDocList![indexPath.row].DocExpireDate?.components(separatedBy: "T")[0])!)")
            myMutableString2.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:7))
          
            cell?.lblDocDate.attributedText = myMutableString
            cell?.LnlDocExpiryDate.attributedText = myMutableString2
            
        }else{
            
          let seperatedIssue = self.documentResponce.AceDocList![indexPath.row].DocIssueDate?.components(separatedBy: "T")[0]
          let seperatedExpiry = self.documentResponce.AceDocList![indexPath.row].DocExpireDate?.components(separatedBy: "T")[0]
            
            let issue = seperatedIssue?.components(separatedBy: "-")
            let expiry = seperatedExpiry?.components(separatedBy: "-")
            
            let issueDate = "\((issue?[2])!)-\((issue?[1])!)-\((issue?[0])!)"
            let ExpiryDate = "\((expiry?[2])!)-\((expiry?[1])!)-\((expiry?[0])!)"
            
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "Issue: \(issueDate)  ")
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:6))
        
            var myMutableString2 = NSMutableAttributedString()
            myMutableString2 = NSMutableAttributedString(string:  "Expiry: \(ExpiryDate)")
            myMutableString2.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:7))

            
            cell?.lblDocDate.attributedText = myMutableString
            cell?.LnlDocExpiryDate.attributedText = myMutableString2
        }
        
        
        if(Todaydate! <= Date()){
            
            cell?.LnlDocExpiryDate.textColor = UIColor.red
            
        }else{
            
            
        }
        
        return cell!
        
    }
    
    
    
    
}
