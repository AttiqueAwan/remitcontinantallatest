//
//  AmountDetailsViewController.swift
//  AyanExpress
//
//  Created by Softech Media on 01/03/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import DropDown



var PaymentMEthodCOde = String()

var UpdateRecipientPickupMoneyInfo = false
var recivingCountryM = String()
class AmountDetailsViewController: UIViewController , UITextFieldDelegate{


    
    @IBOutlet weak var btnContinue: UIButton!   //yes
    

    @IBOutlet weak var lblServiceCharges: UILabel!   //yes
    @IBOutlet weak var lblTotalAmount: UILabel!      //yes
   
  
    @IBOutlet weak var lblSendingCurrency: UILabel!   //yes
    @IBOutlet weak var LblReceivingCurrency: UILabel!
    
    @IBOutlet weak var CalculationView: ViewStyle!
    @IBOutlet weak var lblExchangeRate: UILabel!        //yes

    @IBOutlet weak var txtSendingAmount: JVFloatLabeledTextField!    //yes
    
    @IBOutlet weak var LblReceivingAmount: UILabel!
    @IBOutlet weak var TxtReceivingAmount: UITextField!
    
    @IBOutlet weak var imgSendingCountryFlag: UIImageView!
    @IBOutlet weak var imgRecievingCountryFlag: UIImageView!
    
    let txtFieldSetting = textFieldSetting()
    
    let round = RoundedCorner()
    
    let uc = UtilitySoftTechMedia()
    
    let PaymentMethodDropDown = DropDown()
    var PaymentMethodListResponse : SendingPaymentMethodListResult!
    var PaymentMethodList = [String]()
    
    let PaymentCurrencyDropDown = DropDown()
    var PaymentCurrencyList = [String]()
             //yes
    var PickUpLocationList = [String]()
    var PickUpBranchesList = [String]()
    
    var PayerId = 0                      //yes
    var countryindex = 0                    //yes
    var PaymentMethodindex = 0               //yes
    var PickUpLocationindex = 0              //yes
    var PickUpBranchesindex = 0              //yes
    
    var MaximumAmount = 0.0                //yes
    var MinimumAmount = 0.0                //yes
    
    var ExchangeRates = 0.0            //yes
    var Payments = "Cash"                //yes
    var ReceivingCountry = ""            //yes
    var BranchCode = ""                  //yes
    var currentCurrency = ""
    var UserInfo:RecipientListResult!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        txtSendingAmount.delegate = self
        TxtReceivingAmount.delegate = self
        ExchangeRates = Double(Exchange_Rate)
        
        MaximumAmount = Double(MaxAmount)
        MinimumAmount = Double(MinAmount)
        print(MaxAmount,MinAmount)
        self.lblExchangeRate.text = "1 \(Sending_Currency) = \(Double(Exchange_Rate)) \(RecievingCurrency)"
        self.txtSendingAmount.text = "10"  //100
        self.lblSendingCurrency.text = Sending_Currency
        self.LblReceivingCurrency.text = RecievingCurrency
        PaymentCurrencyList = [Sending_Currency,RecievingCurrency]
       
        currentCurrency = self.lblSendingCurrency.text!
        self.AddingValues(self.txtSendingAmount)
        
       
        //Set top image\

        let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
        let AllCountries = CountryListResult(JSONString: result)
        for i in 0..<(AllCountries?.AceCountryList?.count)!
        {
            if(AllCountries?.AceCountryList![i].Iso3Code == uc.getUserInfo()?.UserInfo?.CountryIsoCode) //uc.getUserInfo().UserInfo.CountryIsoCode)
            {
                if let Country = AllCountries?.AceCountryList![i].CountryName
                {
                    
                   self.imgSendingCountryFlag.image = UIImage(named: "\(Country).png")
                }
                break
            }
        }
    
        //set image
    
        if let myImage = UIImage(named: "\(Country).png"){
           self.imgRecievingCountryFlag.image  = myImage
        }
        
    }

    @IBAction func backBtn(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinue(_ sender: UIButton) {
      //  var recieve = self.LblReceivingAmount.text!.split(separator: " ")
        
        if(self.txtSendingAmount.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please select sending amount", self)
            
        }
        else if(self.TxtReceivingAmount.text?.isEmpty)!{

            uc.errorSuccessAler("", "Please put some sending amount", self)
        }
        else if(self.lblTotalAmount.text == "0.00 \(Sending_Currency)  Payable amount"){
            
            uc.errorSuccessAler("", "Please put some sending amount", self)
        }
        else if( self.TxtReceivingAmount.text == "0" || self.TxtReceivingAmount.text == ""){

            uc.errorSuccessAler("", "Receiving amount must be greater then \(self.MinimumAmount)", self)
        }
        else if(self.lblTotalAmount.text == "0.00 \(String(describing: uc.getUserInfo()?.UserInfo?.CurrencyIsoCode!))  Payable amount"){
        
            uc.errorSuccessAler("", "Total amount must be greater than zero", self)
            
        }else if(self.UserInfo != nil)
        {
            self.performSegue(withIdentifier: "RecipientAlreadySelectedSegue", sender: nil)
        }
        else
        {
            UpdateRecipientPickupMoneyInfo = true
            self.performSegue(withIdentifier: "Transaction", sender: nil)
        }
        
    }
    
    
    @IBAction func btnSendingPaymentMethodClick(_ sender: Any) {
        
        self.PaymentMethodDropDown.show()
        
    }
    
    
    
  
    
    
    @objc func AddingValues(_ textField:UITextField){
        
        
        if textField == self.txtSendingAmount {
            
            let checkamount = Double(textField.text!)!
            let maxamount =    Double(String(format:"%0.2f", Double(self.MaximumAmount)/Double(self.ExchangeRates)))!
            
            if (checkamount <= maxamount){
                
                self.TxtReceivingAmount.text = String(format:"%0.2f",(Double( self.txtSendingAmount.text!)!*Double(self.ExchangeRates)))
                
            }
            if !(self.txtSendingAmount.text?.isEmpty)!{
                
                
            }
        }else if textField == self.TxtReceivingAmount {
            
            if ((Double(textField.text!)!) <=  self.MaximumAmount){
                
                self.txtSendingAmount.text = String(format:"%0.2f", (Double( self.TxtReceivingAmount.text!)! / Double(self.ExchangeRates)))
                self.lblTotalAmount.text = String(format:"%0.2f \(Sending_Currency)", (Double( self.TxtReceivingAmount.text!)! / Double(self.ExchangeRates)))
            }
            
            if !(self.txtSendingAmount.text?.isEmpty)!{
                
            }
        }
        self.GetServiceCharges()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var checkval = 0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            
            if !((textField.text?.isEmpty)!){
                
                if(checkval == 0){
                    
                    self.AddingValues(textField)
                }
                
                
            }
            
        })
        
        if(string == ""){
            
            if(textField.text?.count == 1){
                
                self.TxtReceivingAmount.text = ""
                self.txtSendingAmount.text = ""
                self.lblTotalAmount.text = "0.00 \(Sending_Currency)"
                self.lblServiceCharges.text = "0.00 \(Sending_Currency)"
                
            }else{
                
            }
            
        }else{
            
            if textField == self.txtSendingAmount {
                
                let checkamount = Double(textField.text!+string)!
                let maxamount =  Double(String(format:"%0.2f", self.MaximumAmount/Double(self.ExchangeRates)))!
                
                if !(checkamount <= maxamount){
                    
                    checkval = 1
                    self.uc.errorSuccessAler("", "Maximum sending amount is \(maxamount)", self)
                    return false
                }
                
            }else if textField == self.TxtReceivingAmount {
                
                if !((Double(textField.text!+string)!) <=  self.MaximumAmount){
                    
                    checkval = 1
                    self.uc.errorSuccessAler("", "Maximum receiving amount is \(self.MaximumAmount)", self)
                    
                    return false
                }
                
            }
        }
        
        return true
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "Transaction"){
            
            var totalAm = self.lblTotalAmount.text
            totalAm = totalAm?.components(separatedBy: " \(Sending_Currency)")[0].trimmingCharacters(in: .whitespacesAndNewlines)
            let RecievingAmount = self.TxtReceivingAmount.text!
            var ServiceChargess = self.lblServiceCharges.text!
            ServiceChargess = ServiceChargess.components(separatedBy: " \(Sending_Currency)")[0].trimmingCharacters(in: .whitespacesAndNewlines)

            let sendingpaisa = self.txtSendingAmount.text!.trimmingCharacters(in: .whitespacesAndNewlines)
           
            let parms = ["Country":Country,
                         "PayerId":PayerID,
                         "CountryIso3Code":CountryIso3Code,
                         "CurrencyIsoCode":RecievingCurrency,
                         "Sending_Amount":sendingpaisa,
                         "Sending_Currency":Sending_Currency,
                         "Payment_Method":PayoutMethodobj,
                         "Receiving_Amount":RecievingAmount,
                         "Payment_Type":"Credit Card",
                         "Payout_branchCode":Payout_branchCode,
                         "Service_Charges":ServiceChargess,
                         "Total_Amount":totalAm!,
                         "Exchange_Rate":Exchange_Rate] as [String : Any]
           
            let vc = segue.destination as?  RecipientViewController
            vc?.isFromAmountDetail = true
            
            vc?.parms = parms
        }
        if(segue.identifier == "RecipientAlreadySelectedSegue")
        {
            var totalAm = self.lblTotalAmount.text
            totalAm = totalAm?.components(separatedBy: " \(Sending_Currency)")[0].trimmingCharacters(in: .whitespacesAndNewlines)
            let RecievingAmount = self.TxtReceivingAmount.text!
            var ServiceChargess = self.lblServiceCharges.text!
            ServiceChargess = ServiceChargess.components(separatedBy: " \(Sending_Currency)")[0].trimmingCharacters(in: .whitespacesAndNewlines)
            
            let sendingpaisa = self.txtSendingAmount.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            let parms = ["Country":Country,
                         "PayerId":PayerID,
                         "CountryIso3Code":CountryIso3Code,
                         "CurrencyIsoCode":RecievingCurrency,
                         "Sending_Amount":sendingpaisa,
                         "Sending_Currency":Sending_Currency,
                         "Receiving_Amount":RecievingAmount,
                         "Payment_Method":PayoutMethodobj,
                         "Payment_Type":"Credit Card",
                         "Payout_branchCode":Payout_branchCode,
                         "Service_Charges":ServiceChargess,
                         "Total_Amount":totalAm!,
                         "Exchange_Rate":Exchange_Rate] as [String : Any]
            
            let vc = segue.destination as?  ReviewViewController
            vc?.UserInfo = self.UserInfo
            vc?.parms = parms
        }
        
    }
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }

}

extension AmountDetailsViewController {
    
    //yeah
    func GetServiceCharges() {
        var amount = Double()
        if(currentCurrency == Sending_Currency)
        {
            amount = Double(self.txtSendingAmount.text!)!
        }
        else if(currentCurrency == RecievingCurrency)
        {
            amount = Double(Double(self.txtSendingAmount.text!)! / Double(self.ExchangeRates))
        }
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"Amount":amount,"PayerID":PayerID]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetServiceCharges, params:parms , message: "Loading...", currentController: self ){result in
            
            if(result == "fail")
            {
                self.GetServiceCharges()
                return
            }
            
            let SERVICECHARGES = BankList(JSONString:result)
            
            if SERVICECHARGES?.myAppResult?.Code == 0 {
                
                self.lblServiceCharges.text = String(format: "%d \(Sending_Currency)", (SERVICECHARGES?.AceChargesResponce?.Charges!)!)
                
                if !(self.txtSendingAmount.text?.isEmpty)!{
                Service_Charges = "\((SERVICECHARGES?.AceChargesResponce?.Charges!)!)"
                    self.lblTotalAmount.text =  String(format: "%0.2f \(Sending_Currency)", Double((SERVICECHARGES?.AceChargesResponce?.Charges)!) + Double(amount))
                }
                else
                {
                    Service_Charges = "0"
                }
                
                
                
            }else if SERVICECHARGES?.myAppResult?.Code == 101 {
                
                
                self.uc.logout(self)
                
                
            }else if SERVICECHARGES?.myAppResult?.Code == 102 {
                self.lblServiceCharges.text = "0 \(Sending_Currency)"
                self.lblTotalAmount.text =  String(format: "%0.2f \(Sending_Currency)", Double(self.txtSendingAmount.text!)!)
                self.uc.errorSuccessAler("", "Service Charges Not Found", self)
                
                
            }else{
                
                if(SERVICECHARGES?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (SERVICECHARGES?.myAppResult?.Message)!, self)
                }
            }
            
        }
    }
    
}

//Yeah

