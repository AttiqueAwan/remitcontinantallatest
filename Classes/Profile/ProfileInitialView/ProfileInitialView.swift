//
//  ProfileInitialView.swift
//  Sangarwal
//
//  Created by Mac on 17/07/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
var isProfileUpdated = false
class ProfileInitialView: UIViewController {

    @IBOutlet weak var LblUserName: UILabel!
    @IBOutlet weak var LblCountryName: UILabel!
    @IBOutlet weak var LblEmail: UILabel!
    @IBOutlet weak var LblNumber: UILabel!
    @IBOutlet weak var LblAddress: UILabel!
    
    let uc = UtilitySoftTechMedia()
    var DashboardResponce : Dashboardresult!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.FillUserInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(isProfileUpdated == true)
        {
            DispatchQueue.main.async {
                self.gettingDashboard()
            }
        }
    }
    
    //Fill User Info
    func FillUserInfo(){
        
        let UserInfo = uc.getUserInfo()
        
        if let Email = UserInfo!.UserInfo?.Email
        {
            self.LblEmail.text = Email
        }
       
        if let FullName = UserInfo!.UserInfo?.FullName
        {
            self.LblUserName.text = FullName
        }
        
        if let Email = UserInfo!.UserInfo?.Email
        {
            self.LblEmail.text = Email
        }
        
        if let PhoneNo = UserInfo!.UserInfo?.Phone
        {
            if(PhoneNo == "To Be Updated" || PhoneNo == "")
            {
               self.LblNumber.text = "To Be Updated"
            }
            else
            {
                self.LblNumber.text = PhoneNo
            }
        }
        
        
        if let HouseNo = UserInfo!.UserInfo?.HouseNo
        {
             if let StreetAddress = UserInfo!.UserInfo?.Address
             {
                 if let City = UserInfo!.UserInfo?.City
                 {
                    if let PostalCode = UserInfo!.UserInfo?.PostalCode
                    {
                        if(HouseNo == "To Be Updated" || HouseNo == "" || StreetAddress == "To Be Updated" || StreetAddress == "" || City == "To Be Updated" || City == "" || PostalCode == "To Be Updated" || PostalCode == "")
                        {
                
                            self.LblAddress.text = "To Be Updated"
                
                        }
                        else
                        {
                            let address = "\(HouseNo) \(StreetAddress)\n\(PostalCode) \(City)"
                            self.LblAddress.text = address
                        }
                    
                    }
                }
            }
        }
        
        
        if let Country = UserInfo!.UserInfo?.CountryIsoCode
        {
            
            if(Country == "To Be Updated")
            {
                
                self.LblCountryName.text = ""
                
            }
            else
            {
                let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                let AllCountries = CountryListResult(JSONString: result)
                for i in 0..<(AllCountries?.AceCountryList?.count)!
                {
                    if(AllCountries?.AceCountryList![i].Iso3Code == UserInfo!.UserInfo?.CountryIsoCode)
                    {
                        if let Country = AllCountries?.AceCountryList![i].CountryName
                        {
                            self.LblCountryName.text = Country
                        }
                        break
                    }
                }
            }
        }
        
    }
    
    @IBAction func BackBtn(_ sender: UIButton) {
       
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: ProfileInitialView.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func ChangeEmailorName(_ sender: UIButton) {
    
        self.performSegue(withIdentifier: "Support2", sender: nil)

    }
    
    @IBAction func EditProfileBtnCLicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "ProfileEdit", sender: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }

}

extension ProfileInitialView
{
    func gettingDashboard(){
          
          let authToken = uc.getAuthToken()
          
          
          
          if(authToken == nil){
              
              self.uc.errorSuccessAler("", "Server can't give rights to this user id!", self)
              
          }else{
              
              let parms =  ["Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ID":(authToken?.AuthToken?.user_id)!]as [String : Any]
              
              uc.webServicePosthttp(urlString: ApiUrls.GetDashbaord, params:parms , message: "Loading...", currentController: self){result in
                  
                  if(result == "fail")
                  {
                      self.gettingDashboard()
                      return
                  }
                  
                  self.DashboardResponce = Dashboardresult(JSONString:result)
                  
                  if self.DashboardResponce?.myAppResult?.Code == 0 {
                      
                      UserDefaults.standard.set(result, forKey: "UserLogin")
                    isProfileUpdated = false
                      self.FillUserInfo()
                  }else if self.DashboardResponce?.myAppResult?.Code == 101 {
                      
                      self.uc.logout(self)
                      
                      
                  }else{
                      
                      if(self.DashboardResponce.myAppResult?.Message == nil){
                          
                        //  self.uc.errorSuccessAler("", result, self)
                          
                      }else{
                        //  self.uc.errorSuccessAler("", (self.DashboardResponce.myAppResult?.Message)!, self)
                      }
                  }
                  
              }
          }
      }
}
