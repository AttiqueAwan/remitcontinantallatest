//
//  ProfileViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 21/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import DropDown
import Jelly
import MaterialControls
import MobileCoreServices

class ProfileViewController: UIViewController , UITextFieldDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtlastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtDateofBirth: UITextField!
    @IBOutlet weak var txtPlaceofBirth: UITextField!
    @IBOutlet weak var btnDateofBirth: UIButton!
    @IBOutlet weak var txtHouseNo: UITextField!
    @IBOutlet weak var txtStreetAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtNationality: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    
     @IBOutlet weak var btnGender: UIButton!
    
    @IBOutlet weak var btnNationality: UIButton!
    
    
    let txtFieldSetting = textFieldSetting()
    let uc = UtilitySoftTechMedia()
    var Toast = MDToast()
    var Nationality = [String]()
    var NationalityISOCode = [String]()
    var nationalityIndex = 0
    
    var datePicker : UIDatePicker = UIDatePicker()
    var datePickerContainer = UIView()
    
    let NationalityDropDown = DropDown()
    
    let GenderDropDown = DropDown()
    var GenderList = [String]()
    
    
    var datepicker = MDDatePickerDialog()
    
    
    var checkpicker = ""
    var checkimage = ""
    var image1URL = ""
    
    var documentResponce : DocumentList!
    
    //show jelly popup
    var jellyAnimator: Animator?
    
    var isTrueComeFromDashboard = false
    var isTrueClickedUpdate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
          AllCountryList()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
//        self.txtPhoneNo.textColor = UIColor.black
        self.txtPhoneNo.textColor = .black
//          NotificationCenter.default.addObserver(self, selector: #selector(SelectImage(_:)), name: ApiUrls.PNotification, object: nil)
         self.createDatePicker()
        self.txtEmail.isUserInteractionEnabled = false
         NotificationCenter.default.addObserver(self, selector: #selector(FillPostCodeValues(_:)), name: ApiUrls.PostCodeNotification, object: nil)
        
        txtPlaceofBirth.delegate = self
        txtCity.delegate = self
        
        datepicker.minimumDate = NSDateHelper.mdDate(withYear: 1900, month: 1, day: 1)!

        //self.txtGender = txtFieldSetting.textFieldBaseLine(self.txtGender, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtPhoneNo = txtFieldSetting.textFieldBaseLine(self.txtPhoneNo, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtDateofBirth = txtFieldSetting.textFieldBaseLine(self.txtDateofBirth, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtHouseNo = txtFieldSetting.textFieldBaseLine(self.txtHouseNo, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtStreetAddress = txtFieldSetting.textFieldBaseLine(self.txtStreetAddress, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtCity = txtFieldSetting.textFieldBaseLine(self.txtCity, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtPostalCode = txtFieldSetting.textFieldBaseLine(self.txtPostalCode, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtCountry = txtFieldSetting.textFieldBaseLine(self.txtCountry, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        //self.txtNationality = txtFieldSetting.textFieldBaseLine(self.txtNationality, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        
        
        self.FillNationalityDropDown()
         datepicker.delegate = self
        
        //setting  dropdown
        NationalityDropDown.anchorView = self.btnNationality
        NationalityDropDown.dataSource = Nationality
        
        
        NationalityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.nationalityIndex = index
            self.txtNationality.text = item
        }
        
        GenderList = ["Male","Female"]
        GenderDropDown.anchorView = self.btnGender
        GenderDropDown.dataSource = GenderList
        
        
        
        GenderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtGender.text = item
        }
        
        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: .letters) != nil || string == " " || string == ""
        {
            return true
        }else {
            Toast.text = "This character is not allowed"
            Toast.show()
            Toast.duration = 1
            return false
        }
    }

    
    
    @objc  func FillPostCodeValues(_ notification:NSNotification) {
        
        let userinfo =  notification.userInfo as? [String:Any]
        
        let PostCode =     (userinfo!["PostalCode"] as? String?)!
        let City =  (userinfo!["City"] as? String?)!
        let StreetAddress =  (userinfo!["StreetAddress"] as? String?)!
        let HouseNo =  (userinfo!["HouseNo"] as? Int?)!
        
        
        if let Postcode = PostCode{
            
            self.txtPostalCode.text = Postcode.uppercased()
        }
        
        if let city = City{
            
            self.txtCity.text = city.uppercased()
        }
        
        if let Streetaddress = StreetAddress{
            
            self.txtStreetAddress.text = Streetaddress.uppercased()
        }
        
        
        if let Houseno = HouseNo{
            
            self.txtHouseNo.text = "\((Houseno))"
        }
        
    }
    
    //Datepicker code for issue and expiry date
      
      func createDatePicker()
      {
          datePicker.datePickerMode = .date
          self.txtDateofBirth.inputView = datePicker
          
          let toolbar = UIToolbar()
          toolbar.sizeToFit()
          let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
          let doneBtn = UIBarButtonItem(barButtonSystemItem: .done , target: nil, action: #selector(doneClicked))
          toolbar.setItems([flexSpace,doneBtn], animated: true)
          self.txtDateofBirth.inputAccessoryView = toolbar
      }
      
      @objc func doneClicked()
      {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "dd-MM-YYYY"
          self.txtDateofBirth.text = dateFormatter.string(from: datePicker.date)
          let age = calcAge(birthday: "\(dateFormatter.string(from: datePicker.date))")
          if(age < 18)
          {
              uc.errorSuccessAler("Error", "Age must be 18+ to continue", self)
              self.txtDateofBirth.text = ""
          }
          self.view.endEditing(true)
      }
      
      //Calculate Age
      func calcAge(birthday: String) -> Int {
          let dateFormater = DateFormatter()
          dateFormater.dateFormat = "dd/MM/yyyy"
          var birthdayDate = dateFormater.date(from: birthday)
          
          if(birthdayDate == nil){
              
              dateFormater.dateFormat = "MM/dd/yyyy"
              birthdayDate = dateFormater.date(from: birthday)
              
          }
          if(birthdayDate == nil){
              
              dateFormater.dateFormat = "yyyy/MM/dd"
              birthdayDate = dateFormater.date(from: birthday)
              
          }
          
          
          let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
          let now = Date()
          let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
          let age = calcAge.year
          return age!
      }
    
    // Fill Nationality DropDown
    func FillNationalityDropDown(){
        
        
        let UserInfo = uc.getUserInfo()
        
        for i in 0..<(Nationality.count) {
            
            if let Nationali = UserInfo!.UserInfo?.NationalityIsoCode {
                
                if(NationalityISOCode[i] == Nationali){
                    self.nationalityIndex = i
                    self.txtNationality.text = Nationality[i]
                }
            }
        }
    }
    
    //Fill User Info
    func FillUserInfo(){
        
        let UserInfo = uc.getUserInfo()
        
        if let PhoneNo = UserInfo!.UserInfo?.Phone {
            
            if(PhoneNo == "To Be Updated"){
                
                self.txtPhoneNo.text = ""
                
            }else{
                
                self.txtPhoneNo.text = PhoneNo
            }
            
            
            
        }
        
        
        if let PlaceOfBirth = UserInfo!.UserInfo?.CountryOfBirthIsoCode {
            
            if(PlaceOfBirth == "To Be Updated"){
                
                self.txtPlaceofBirth.text = ""
                
            }else{
            
                self.txtPlaceofBirth.text = PlaceOfBirth.uppercased()
            
            }
            
            
            
        }
        
        if let HouseNo = UserInfo!.UserInfo?.HouseNo {
            
            if(HouseNo == "To Be Updated"){
                
                 self.txtHouseNo.text = ""
                
            }else{
                
                 self.txtHouseNo.text = HouseNo.uppercased()
            }
            
           
            
        }
        
        if let StreetAddress = UserInfo!.UserInfo?.Address {
            
            if(StreetAddress == "To Be Updated"){
                
                self.txtStreetAddress.text = ""
                
            }else{
                
                self.txtStreetAddress.text = StreetAddress.uppercased()
            }
            
            
        }
        if let email  = UserInfo!.UserInfo?.Email {
            
            if(email == "To Be Updated"){
                
                self.txtEmail.text = ""
                
            }else{
                
                self.txtEmail.text = email
            }
            
            
        }
        if let firstName  = UserInfo!.UserInfo?.FirstName {
            
            if(firstName == "To Be Updated"){
                
                self.txtFirstName.text = ""
                
            }else{
                
                self.txtFirstName.text = firstName
            }
            
            
        }
        if let lastName  = UserInfo!.UserInfo?.LastName {
            
            if(lastName == "To Be Updated"){
                
                self.txtlastName.text = ""
                
            }else{
                
                self.txtlastName.text = lastName
            }
            
            
        }
        
        if let City = UserInfo!.UserInfo?.City {
            
            if(City == "To Be Updated"){
                
                self.txtCity.text = ""
                
            }else{
                
                self.txtCity.text = City.uppercased()
            }
            
        }
        
        if let PostalCode = UserInfo!.UserInfo?.PostalCode {
            
            if(PostalCode == "To Be Updated"){
                
                self.txtPostalCode.text = ""
                
            }else{
                
                self.txtPostalCode.text = PostalCode.uppercased()
            }
            
        }
        
        if let Gender = UserInfo!.UserInfo?.Gender {
            
            if(Gender == "To Be Updated"){
                
                self.txtGender.text = ""
                
            }else{
                
                self.txtGender.text = Gender
                
            }
            
        }
        
        if let Nationaly = UserInfo!.UserInfo?.NationalityIsoCode {
          
            if(Nationaly == "To Be Updated"){
                
                self.txtNationality.text = ""
                
            }else{
                
                for i in 0..<(Nationality.count) {
                    
                    if let Nationali = UserInfo!.UserInfo?.NationalityIsoCode {
                        
                        if(NationalityISOCode[i] == Nationali){
                            self.nationalityIndex = i
                            self.txtNationality.text = Nationality[i]
                            break
                        }
                    }
                }
                
            }
           
        }
    
        
        if let Country = UserInfo!.UserInfo?.CountryIsoCode {
            
            if(Country == "To Be Updated"){
                
                self.txtCountry.text = ""
                
            }else{
                
                let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                
                let AllCountries = CountryListResult(JSONString: result)
                
                
                for i in 0..<(AllCountries?.AceCountryList?.count)! {
                    
                    if(AllCountries?.AceCountryList![i].Iso3Code == UserInfo!.UserInfo?.CountryIsoCode){
                        
                        
                        
                        if let Country = AllCountries?.AceCountryList![i].CountryName {
                            
                            self.txtCountry.text = Country
                        }
                        
                        
                        
                        break
                    }
                }
                
                
            }
            
            
        }
        
        if let DateofBirth = UserInfo!.UserInfo?.DOB {
            
            if(DateofBirth == "To Be Updated"){
                
                self.txtDateofBirth.text = ""
                
            }else{
                
                guard let BirthDay = UserInfo?.UserInfo?.BirthDay , let BirthMonth = UserInfo?.UserInfo?.BirthMonth , let BirthYear = UserInfo?.UserInfo?.BirthYear else{
                    
                    self.txtDateofBirth.text = DateofBirth.components(separatedBy: "T")[0]
                    return
                }
                
                self.txtDateofBirth.text = "\(BirthDay)-\(BirthMonth)-\(BirthYear)"
                
            }
            
            
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        //go back to dashboard
        if isTrueComeFromDashboard {
            self.navigationController?.popViewController(animated: true)
        }else if isTrueClickedUpdate {
            self.navigationController?.pushViewController(CreateTransViewController(), animated: true)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        
        
//        if let viewControllers = self.navigationController?.viewControllers {
//            for vc in viewControllers {
//                if vc.isKind(of: ProfileViewController.classForCoder()) {
//                    print("It is in stack")
//                    self.navigationController?.popViewController(animated: true)
//                }
//            }
//        }
//        else
//        {
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    @IBAction func btnPostCodeSearchClick(_ sender: Any) {
        
        
        self.performSegue(withIdentifier: "PostCodeSearch", sender: nil)
    }
    
    
  
    
    func AllCountryList(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"9"]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.AllCountryList()
                return
            }
            
            let AllCountries = CountryListResult(JSONString: result)
            
            if AllCountries?.myAppResult?.Code == 0 {
                
                let AllCountries = CountryListResult(JSONString: result)
                
                let count = AllCountries!.AceCountryList!.count
                for i in 0..<count {
                    self.Nationality.append((AllCountries?.AceCountryList![i].Nationality) ?? "")
                    self.NationalityISOCode.append((AllCountries?.AceCountryList![i].Iso3Code) ?? "")
                }
                self.NationalityDropDown.dataSource = self.Nationality
                self.NationalityDropDown.reloadAllComponents()
                
                UserDefaults.standard.set(result, forKey: "AllNationalityList")
                self.FillUserInfo()
                
            }else if AllCountries?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(AllCountries?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (AllCountries?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    

    @IBAction func btnGenderClick(_ sender: Any) {
        
         self.GenderDropDown.show()
    }
    
    @IBAction func btnNationalityClick(_ sender: Any) {
        
        self.NationalityDropDown.show()
    }
    
    @IBAction func btnUpdateClick(_ sender: Any) {
        
        if (self.txtPhoneNo.text?.isEmpty)! {
        
            uc.errorSuccessAler("", "Please enter phone no", self)
            
        }else if (self.txtDateofBirth.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please select date of birth", self)
            
        }else if (self.txtDateofBirth.text?.contains("0-0-0"))! {
            
            uc.errorSuccessAler("", "Please select date of birth", self)
            
        }else if (self.txtPlaceofBirth.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please enter place of birth", self)
            
        }else if (self.txtGender.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please select gender", self)
            
        }else if (self.txtHouseNo.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please enter house no", self)
            
        }else if (self.txtStreetAddress.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please enter street address", self)
            
        }else if (self.txtCity.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please enter city name", self)
            
        }else if (self.txtPostalCode.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please enter postal code", self)
            
        }else if (self.txtNationality.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please enter nationalilty", self)
            
        }else if (uc.calcAge(birthday: self.txtDateofBirth.text!) < 18) {
            
            uc.errorSuccessAler("", "Your age must be 18+ for sending money", self)
            
        }else{
            
            self.UpdateProfile()
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if(segue.identifier == "PostCodeSearch"){
            
            let destination = segue.destination as? PostCodeFinderViewController
            
            if !(self.txtPostalCode.text?.isEmpty)!{
                
                destination?.postCode = self.txtPostalCode.text!
            }
        }
    }
    

}

// setting up date picker delegate method
extension ProfileViewController:MDDatePickerDialogDelegate{
    
    //open datepicker dialog and set up value to label on done
    func datePickerDialogDidSelect(_ date: Date) {
        
        let formater = DateFormatter()
        formater.dateFormat = "dd-MM-yyyy"
        
        let newDate = formater.string(from: date)
        
        self.txtDateofBirth.text = newDate
        
        
    }
}
