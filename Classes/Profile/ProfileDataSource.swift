//
//  ProfileDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 27/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension ProfileViewController{
    
    //Update Profile
    func UpdateProfile(){
        
        let UserInfo = uc.getUserInfo()
        
        var UserName = ""
        var Password = ""
       
        if let username = UserDefaults.standard.value(forKey: "Email") as? String {
            UserName = username
        }
        
        if let password = UserDefaults.standard.value(forKey:"Password") as? String  {
            
            Password = password
        }
        
        let DOB = self.txtDateofBirth.text?.components(separatedBy: "-")
        
        let userInfo = uc.getUserInfo()?.UserInfo
        let authToken = uc.getAuthToken()
        
        let parms =  ["Token":ApiUrls.AuthToken,
                      "AppID":ApiUrls.AppID,
                      "ID":(authToken?.AuthToken?.user_id)!,
                      "Password":Password,
                      "Username":UserName,
                      "DomainID":"0",
                      "FirstName":self.txtFirstName.text!,
                      "LastName":self.txtlastName.text!,
                      "FullName":"\(self.txtFirstName.text!) \(self.txtlastName.text!)",
                      "Email":userInfo!.Email!,
            "Phone":self.txtPhoneNo.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "DOB":"\((DOB?[2])!)-\((DOB?[1])!)-\((DOB?[0])!)",
            "BirthDay":self.txtDateofBirth.text!.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: "-")[0],
            "BirthMonth":self.txtDateofBirth.text!.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: "-")[1],
            "BirthYear":self.txtDateofBirth.text!.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: "-")[2],
            "Gender":(self.txtGender.text)!,
            "CountryIsoCode":(UserInfo!.UserInfo?.CountryIsoCode)!,
            "CurrencyIsoCode":(UserInfo!.UserInfo?.CurrencyIsoCode)!,
            "NationalityIsoCode":NationalityISOCode[self.nationalityIndex],
            "CountryOfBirthIsoCode":self.txtPlaceofBirth.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "HouseNo":self.txtHouseNo.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "Address":self.txtStreetAddress.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "City":self.txtCity.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "Occupation":(UserInfo!.UserInfo?.Occupation)!,
            "Employer":(UserInfo!.UserInfo?.Employer)!,
            "PostalCode":self.txtPostalCode.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "CustomerID":(UserInfo!.UserInfo?.CustomerID)!,
            "DeviceType":"IOS",
            "ReferralCode":(UserInfo!.UserInfo?.ReferralCode ?? ""),
            "AgentSign":(UserInfo!.UserInfo?.AgentPromoCode)!]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.UpdateProfile, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.UpdateProfile()
                return
            }
            
            let AllCountiresResponse = AppUser(JSONString:result)
            
            if AllCountiresResponse?.myAppResult?.Code == 0 {
                
                self.isTrueClickedUpdate = true

                let alertCOntroller = UIAlertController(title: "", message: "You have successfully updated your profile", preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
                    isProfileUpdated = true
                    if let viewControllers = self.navigationController?.viewControllers {
                        for vc in viewControllers {
                            if vc.isKind(of: ProfileViewController.classForCoder()) {
                                print("It is in stack")
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                    else
                    {
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                })
                alertCOntroller.addAction(action)
                self.present(alertCOntroller,animated: true,completion: nil)
                
                
                
                
            }else if AllCountiresResponse?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(AllCountiresResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (AllCountiresResponse?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    

}
