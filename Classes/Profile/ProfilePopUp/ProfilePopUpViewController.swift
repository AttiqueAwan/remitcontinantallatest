//
//  ProfilePopUpViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 07/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class ProfilePopUpViewController: UIViewController {

   
    @IBOutlet weak var btnGallery: UIButton!
    
    @IBOutlet weak var btnCamera: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    let imagePicker = UIImagePickerController()
    
    var checkimage = ""
    var img1 = UIImageView()
    var image1URL = ""
    var imgData : Data!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.definesPresentationContext = true
        imagePicker.delegate = self
        
        self.btnCancel.layer.cornerRadius = 8.0
        self.btnCancel.layer.masksToBounds = true
        self.preferredContentSize = CGSize(width: 280, height: 250)
        self.view.layoutIfNeeded()
        self.view.setNeedsDisplay()
        self.view.setNeedsLayout()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCameraClick(_ sender: Any) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnGalleryClick(_ sender: Any) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension ProfilePopUpViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        
        
        let image = info[.originalImage] as? UIImage
        
        if(picker.sourceType == .camera){
            
            let imageURL = self.saveImage(imageName: "\(arc4random()).png", image!)
            self.image1URL = imageURL
            
        }else{
            
            if #available(iOS 11.0, *) {
                let imageURL = info[UIImagePickerController.InfoKey.imageURL] as! URL
                self.image1URL =  imageURL.absoluteString
            } else {
                // Fallback on earlier versions
            }
            
        }
        
        
        
        self.img1.image = image
        let  imageData = image!.pngData()
        self.imgData = imageData
        
        
        NotificationCenter.default.post(name: ApiUrls.PNotification, object: nil, userInfo: ["image":self.img1.image!,"checkimage":self.checkimage,"imageData":imgData!,"imgURL":self.image1URL])
        
        
        dismiss(animated:false, completion: {
            
            self.dismiss(animated:true, completion: nil)
        })
        
    }
    
    func saveImage(imageName: String,_ imageselected:UIImage)-> String{
        //create an instance of the FileManager
        let fileManager = FileManager.default
        //get the image path
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        //get the image we took with camera
        let image = imageselected
        //get the PNG data for this image
        let data = image.pngData()
        //store it in the document directory
        fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
        return imagePath
        
    }
    
    
    
}
