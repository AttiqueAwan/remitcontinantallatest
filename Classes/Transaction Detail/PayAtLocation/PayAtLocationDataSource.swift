//
//  PayAtLocationDataSource.swift
//  AyanExpress
//
//  Created by Softech Media on 08/04/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
extension PayAtLocationVC
{
    func getPayLocations(){
        
        
        let CountryIso3Code = (uc.getUserInfo()?.UserInfo?.CountryIsoCode)!
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"CountryIso3Code":CountryIso3Code]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.payinlocationlist, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getPayLocations()
                return
            }
            
            self.documentResponce = PayInResponseListResult(JSONString:result)
            
            if self.documentResponce?.myAppResult?.Code == 0 {
                
                
                if(self.documentResponce.PayInResponseList != nil && (self.documentResponce.PayInResponseList?.count)! > 0){
                    
                    self.PayAtLocationTblView.reloadData()
                    
                }
                
            }else if self.documentResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.documentResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.documentResponce?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    
}
