//
//  PayAtLocationTVCell.swift
//  AyanExpress
//
//  Created by Softech Media on 08/04/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class PayAtLocationTVCell: UITableViewCell {

    @IBOutlet weak var Address: UILabel!
    @IBOutlet weak var CellPhone: UILabel!
    @IBOutlet weak var OfficeNumber: UILabel!
    @IBOutlet weak var BussinessNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
