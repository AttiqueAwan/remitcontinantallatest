//
//  PayAtLocationVC.swift
//  AyanExpress
//
//  Created by Softech Media on 08/04/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class PayAtLocationVC: UIViewController,UITableViewDelegate,UITableViewDataSource{ 
    @IBOutlet weak var TimeLbl: UILabel!
    @IBOutlet weak var ExpireTimeorDateLbl: UILabel!
    @IBOutlet weak var PaymentNumLbl: UILabel!
    @IBOutlet weak var IdentificationDocumentInfoLbl: UILabel!
    @IBOutlet weak var PayAtLocationTblView: UITableView!
    
    var documentResponce : PayInResponseListResult!
    let uc = UtilitySoftTechMedia()
    var TransDetail : TransactionListresult!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PayAtLocationTblView.dataSource = self
        PayAtLocationTblView.delegate = self
        self.getPayLocations()
        
        if let TransactionPIN = self.TransDetail.PaymentNumber{
            self.PaymentNumLbl.text = "Payment No. \(TransactionPIN)"
        }
        if let TransactionDate = self.TransDetail.PaymentDate{
            let ExactDate = TransactionDate.toDate(format: "yyyy-MM-dd")
            let df = DateFormatter()
            df.dateFormat = "dd-MM-yyyy"
            let date = df.string(from: ExactDate!)
            let dattee = date.toDate(format: "dd-MM-yyyy")
            let datewith1dayADD = dattee!.add(component: .day , value: 1)
            let dateString = datewith1dayADD?.toString(format: "dd-MM-yyyy")
            self.ExpireTimeorDateLbl.text = dateString
        }
        
        if let TransactionTIME = self.TransDetail.PaymentTime{
            let dateFormatterw = DateFormatter()
            if( Calendar.current.locale?.identifier == "en_GB")
            {
                dateFormatterw.locale = Locale(identifier:"en_GB_POSIX")
            }
            else
            {
                dateFormatterw.locale = Locale(identifier:"en_US_POSIX")
            }
            dateFormatterw.dateFormat = "H:mm:ss"
            let date12 = dateFormatterw.date(from: TransactionTIME)
            let dateFormatter = DateFormatter()
            if( Calendar.current.locale?.identifier == "en_GB")
            {
                dateFormatter.locale = Locale(identifier:"en_GB_POSIX")
            }
            else
            {
                dateFormatter.locale = Locale(identifier:"en_US_POSIX")
            }
            dateFormatter.dateFormat = "h:mm a"
            let dateString = dateFormatter.string(from: date12!)
            self.TimeLbl.text = dateString
        }
        
        
        if let amount = self.TransDetail.PayInAmount{
            if let SendingCurrency = self.TransDetail.SendingCurrency{
                
                self.IdentificationDocumentInfoLbl.text = "Bring \(amount) \(SendingCurrency) cash and your identification document to a participating location"
            }
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func BackBtnFunc(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.documentResponce != nil){
            
            if(self.documentResponce.PayInResponseList != nil && (self.documentResponce.PayInResponseList!.count) > 0){
                
                return (self.documentResponce.PayInResponseList!.count)
            }
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.PayAtLocationTblView.dequeueReusableCell(withIdentifier: "Cell") as! PayAtLocationTVCell
        let address = self.documentResponce.PayInResponseList![indexPath.row].Address
        let houseNum = self.documentResponce.PayInResponseList![indexPath.row].HouseNo
        let PostCOde = self.documentResponce.PayInResponseList![indexPath.row].PostCode
        let City = self.documentResponce.PayInResponseList![indexPath.row].City
        
        cell.Address.text = "\(String(describing: houseNum!)) \(String(describing: address!)) \(String(describing: PostCOde!)) \(String(describing: City!))"
        cell.CellPhone.text = self.documentResponce.PayInResponseList![indexPath.row].Phone
        cell.OfficeNumber.text = self.documentResponce.PayInResponseList![indexPath.row].CellPhone
        cell.BussinessNameLbl.text = self.documentResponce.PayInResponseList![indexPath.row].BusinessName
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 105
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
    
}


extension String {
    
    func toDate(format: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self)
    }
}

extension Date {
    
    func toString(format: String) -> String? {
        
        let df = DateFormatter()
        df.dateFormat = format
        return df.string(from: self)
    }
    
    func add(component: Calendar.Component, value: Int) -> Date? {
        
        return Calendar.current.date(byAdding: component, value: value, to: self)
    }
}
