//
//  TransDetailDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 31/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension TransDetailViewController{
    
    func FillValues() {
        
        if let SendingCurrency = self.UpdateTrans.SendingCurrency{
            if let fee = self.UpdateTrans.ServiceCharges{
            if let SendingAmount = self.UpdateTrans.PayInAmount{
                
                self.lblSendingAmount.text = "\(SendingAmount) \(SendingCurrency)"
                self.lblSendingAmount2.text = "Amount To Send: \(SendingAmount) \(SendingCurrency)"
                let total = Double(Double(fee)! + Double(SendingAmount)!)
                self.lblTotalAmount.text = "Total: \(total) \(SendingCurrency)"
                }
            }
        }
        
        if let fee = self.UpdateTrans.ServiceCharges
        {
            self.lblTransactionFee.text = "Transaction Fee: \(fee)"
        }
        
        if let SendingCurrency = self.UpdateTrans.SendingCurrency{
        if let ReceivingCUrrency = self.UpdateTrans.ReceivingCurrency{
            if let ExchangeRate = self.UpdateTrans.ExchangeRate{
            if let ReceivingAmount = self.UpdateTrans.PayOutAmount{
                
                self.lblReceivingAmount.text = "\(ReceivingAmount) \(ReceivingCUrrency)"
                self.lblReceivingAmount2.text = "Receiver Amount: \(ReceivingAmount) \(ReceivingCUrrency)"
                self.lblExchangeRate.text = "Rate Applied for: 1 \(SendingCurrency) = \(ExchangeRate) \(ReceivingCUrrency)"
                }
            }
        }
    }
    
        if let PaymentDate = self.UpdateTrans.PaymentDate{
            
            let transDate = PaymentDate.components(separatedBy: "T")[0].components(separatedBy:"-")
            
            self.lblTransactionDate.text =  "\((transDate[2]))-\((transDate[1]))-\((transDate[0]))"
        }
        
        
        if let PaymentNumber = self.UpdateTrans.PaymentNumber{
            
            self.lblTransactionPIN.text = "PIN:\(PaymentNumber)"
            
        }
        if let BeneName = self.UpdateTrans.BeneName{
            
            self.lblBeneName.text = BeneName
            
        }
        
        if let BenePhone = self.UpdateTrans.BenePhone{
            
            self.lblBenePhone.text = BenePhone
            
        }
        
        if let SendingPaymentMethod = self.UpdateTrans.SendingPaymentMethod{
            if(SendingPaymentMethod.lowercased() == "cash")
            {
                self.lblSendingMethod.text = "Cash Pickup"
            }
            else if(SendingPaymentMethod.lowercased() == "bank")
            {
                self.lblSendingMethod.text = "Online Bank Transfer"
            }
            else
            {
                self.lblSendingMethod.text = SendingPaymentMethod
            }
        }
    
        if let PaymentMethod = self.UpdateTrans.PaymentMethod{
            if(PaymentMethod.uppercased() == "CASH")
            {
                self.PayoutMethod.text = "Cash Pickup"
                if let payerName = self.UpdateTrans.PayerName
                {
                    if let payerBranchAddress = self.UpdateTrans.PayoutBranchAddress
                    {
                        self.lblBeneCountry.text = "\(payerName)\n\(payerBranchAddress)"
                    }
                }
                
            }
            else if (PaymentMethod.uppercased() == "BANK") {
                self.PayoutMethod.text = "Account Transfer"
                if let BankName = self.UpdateTrans.ReceiverBankName{
                    self.lblBeneCountry.text = BankName
                }
                
                if let BranchName = self.UpdateTrans.ReceiverBranchName{
                    
                    self.lblBeneCountry.text = "\(self.lblBeneCountry.text!)\n\(BranchName)"
                }
            }else if (PaymentMethod.uppercased() == "WALLET") {
                
                self.PayoutMethod.text = "Mobile Wallet"
                if let BankName = self.UpdateTrans.ReceiverBankName{
                    self.lblBeneCountry.text = BankName
                }
                
                if let BranchName = self.UpdateTrans.ReceiverBranchName{
                    
                    self.lblBeneCountry.text = "\(self.lblBeneCountry.text!)\n\(BranchName)"
                }
            }else if (PaymentMethod.uppercased() == "BILLPAYMENT") {
                
                self.PayoutMethod.text = "Bill Payment"
                if let BankName = self.UpdateTrans.ReceiverBankName{
                    self.lblBeneCountry.text = BankName
                }
                
                if let BranchName = self.UpdateTrans.ReceiverBranchName{
                    
                    self.lblBeneCountry.text = "\(self.lblBeneCountry.text!)\n\(BranchName)"
                }
            }else if (PaymentMethod.uppercased() == "PREPAIDCARD") {
                
                self.PayoutMethod.text = "Prepaid Card"
                if let BankName = self.UpdateTrans.ReceiverBankName{
                    self.lblBeneCountry.text = BankName
                }
                
                if let BranchName = self.UpdateTrans.ReceiverBranchName{
                    
                    self.lblBeneCountry.text = "\(self.lblBeneCountry.text!)\n\(BranchName)"
                }
            }
            
        }
        
        
        
        
        guard let paymentStatus = self.UpdateTrans.PaymentStatus else {
            
            return
        }
        
        self.lblTransactionStatus.text = paymentStatus
        
        if(paymentStatus == "Canceled"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "cancelled_icon_white")
//            self.backView?.backgroundColor  = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.5475491751, green: 0.07416537723, blue: 0.058784036, alpha: 1)
            self.btnBottomBack.setTitle("Take Screenshot", for: .normal)
            
        }else if(paymentStatus == "Canceling"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "cancelled_icon_white")
//            self.backView?.backgroundColor  = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0.6, green: 0, blue: 0, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.5475491751, green: 0.07416537723, blue: 0.058784036, alpha: 1)
            self.btnBottomBack.setTitle("Take Screenshot", for: .normal)
            
        }else if(paymentStatus == "Completed"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "complete_icon_white")
//            self.backView?.backgroundColor  = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            self.btnBottomBack.setTitle("Take Screenshot", for: .normal)
            if(self.UpdateTrans.SendingPaymentMethod == "Bank")
            {
                self.btnBottomBack.setTitle("View Bank Details", for: .normal)
            }
            
        }else if(paymentStatus == "In-process" || paymentStatus == "Compliance Hold"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "in_process_icon_white")
//            self.backView?.backgroundColor  =  #colorLiteral(red: 0, green: 0.5450980392, blue: 0.8, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0, green: 0.5450980392, blue: 0.8, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0, green: 0.5450980392, blue: 0.8, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0, green: 0.5450980392, blue: 0.8, alpha: 1)
            self.btnBottomBack.setTitle("Take Screenshot", for: .normal)
            if(self.UpdateTrans.SendingPaymentMethod == "Bank")
            {
                self.btnBottomBack.setTitle("View Bank Details", for: .normal)
            }
            
        }else if(paymentStatus == "Incomplete"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "in_complete_icon_white")
//            self.backView?.backgroundColor  =  #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
            self.btnBottomBack.setTitle("Click To Complete", for: .normal)
            if(self.UpdateTrans.SendingPaymentMethod == "Bank")
            {
                self.btnBottomBack.setTitle("View Bank Details", for: .normal)
            }
        }
        else if(paymentStatus == "Pending"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "in_complete_icon_white")
//            self.backView?.backgroundColor  =  #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8, green: 0.4, blue: 0, alpha: 1)
            self.btnBottomBack.setTitle("Take Screenshot", for: .normal)
            if(self.UpdateTrans.SendingPaymentMethod == "Bank")
            {
                self.btnBottomBack.setTitle("View Bank Details", for: .normal)
            }
        }
        else if(paymentStatus == "ok"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "complete_icon_white")
//            self.backView?.backgroundColor  = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            self.btnBottomBack.setTitle("Take Screenshot", for: .normal)
            if(self.UpdateTrans.SendingPaymentMethod == "Bank")
            {
                self.btnBottomBack.setTitle("View Bank Details", for: .normal)
            }
            
        }
        else if(paymentStatus == "Paid"){
            
            self.imgTransactionStatus.image = #imageLiteral(resourceName: "complete_icon_white")
//            self.backView?.backgroundColor  = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            self.btnBottomBack.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            self.topView.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
//            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0, green: 0.6, blue: 0, alpha: 1)
            self.btnBottomBack.setTitle("Take Screenshot", for: .normal)
            if(self.UpdateTrans.SendingPaymentMethod == "Bank")
            {
                self.btnBottomBack.setTitle("View Bank Details", for: .normal)
            }
        }
    }
    
}
