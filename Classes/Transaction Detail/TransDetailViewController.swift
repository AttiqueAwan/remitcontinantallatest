//
//  TransDetailViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 31/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import MaterialControls

class TransDetailViewController: UIViewController {

    var Toast = MDToast()
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var ScreenshotView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblSendingAmount: UILabel!
    @IBOutlet weak var imgTransactionStatus: UIImageView!
    @IBOutlet weak var lblReceivingAmount: UILabel!
    @IBOutlet weak var lblTransactionStatus: UILabel!
    @IBOutlet weak var lblTransactionPIN: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!

    @IBOutlet weak var userinfoView: UIView!
    @IBOutlet weak var lblSendingAmount2: UILabel!
    @IBOutlet weak var lblReceivingAmount2: UILabel!
    @IBOutlet weak var lblBeneName: UILabel!
    @IBOutlet weak var lblBenePhone: UILabel!
    @IBOutlet weak var lblBeneCountry: UILabel!
    @IBOutlet weak var lblSendingMethod: UILabel!
    @IBOutlet weak var lblTransactionFee: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var PayoutMethod: UILabel!
    @IBOutlet weak var lblExchangeRate: UILabel!
    
    
    @IBOutlet weak var btnBottomBack: UIButton!
    
    @IBOutlet weak var backView: UIView!
    
    var UpdateTrans : TransactionListresult!
    let uc = UtilitySoftTechMedia()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        self.FillValues()
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func screenshot() -> UIImage {
        if #available(iOS 10.0, *) {
            return UIGraphicsImageRenderer(size: self.ScreenshotView.bounds.size).image { _ in
                self.ScreenshotView.drawHierarchy(in: CGRect(origin: .zero, size: self.ScreenshotView.bounds.size), afterScreenUpdates: true)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(self.ScreenshotView.bounds.size, false, UIScreen.main.scale)
            self.ScreenshotView.drawHierarchy(in: self.ScreenshotView.bounds, afterScreenUpdates: true)
            let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
            UIGraphicsEndImageContext()
            return image
        }
    }
    
    
    func screenShotMethod() {
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(screenshot(), nil, nil, nil)
        Toast.text = "ScreenShot saved in image gallery"
        Toast.show()
        Toast.duration = 3
    }
    
    @IBAction func btnBottomBackClick(_ sender: Any) {
       
        if(self.btnBottomBack.title(for: .normal) == "View Bank Details")
        {
            self.performSegue(withIdentifier: "BankDetail", sender: nil)
        }
        else if(self.btnBottomBack.title(for: .normal) == "Back")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(self.btnBottomBack.title(for: .normal) == "Take Screenshot")
        {
            screenShotMethod()
        }
        else if(self.btnBottomBack.title(for: .normal) == "Click To Complete")
        {
            self.performSegue(withIdentifier: "PaymentPage", sender: nil)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "PaymentPage"){
            
            let destination = segue.destination as? PaymentViewController
            destination?.weburl = "\((self.UpdateTrans.PaymentPageURL!))?id=\((self.UpdateTrans.PaymentID)!)"
        }
        else if(segue.identifier == "BankDetail")
        {
            let BankPage = segue.destination as? DepositToBankVC
            BankPage?.UpdateTrans =  self.UpdateTrans
        }
    }
    

    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
    
}
