//
//  AddRecipientDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 27/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

extension AddRecipientViewController{
    
    
    func getRelationType(){
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ListType":"RELATIONSHIPS"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetLists, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getRelationType()
                return
            }
            
            self.RelationTypeList = ListingTypesresult(JSONString:result)
            
            if  self.RelationTypeList?.myAppResult?.Code == 0 {
                
                for i in 0..<(self.RelationTypeList.ListType?.count)!{
                    
                    self.RelationShip.append(self.RelationTypeList.ListType![i].Text!)
                }
                
                self.RelationDropDown.dataSource = self.RelationShip
                self.RelationDropDown.reloadAllComponents()
                
            }else if  self.RelationTypeList?.myAppResult?.Code == 102{
                
            }else{
                
                if( self.RelationTypeList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("", ( self.RelationTypeList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    
    
    
    func getBankList(bankCode:String){
        
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
                      "ReceivingCountryIso3Code":bankCode]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.CountryBankList, params:parms , message: "Loading...", currentController: self){result in
            
            print(result)
            if(result == "fail")
            {
                self.getBankList(bankCode: bankCode)
                return
            }
            
            self.BankResponce = BankList(JSONString:result)
            
            if self.BankResponce?.myAppResult?.Code == 0 {
                
                self.Bank = [String]()
                for i in 0 ..< (self.BankResponce?.AceCountryBankList!.count)!{
                    
                    if(self.UpdateRecipient != nil){
                        
                        if(self.UpdateRecipient.BeneBankCode == self.BankResponce.AceCountryBankList![i].BankCode){
                            
                            
                            self.BankIndex = i
                        }
                    }
                    
                    self.Bank.append((self.BankResponce?.AceCountryBankList![i].BankName)!)
                }
                
               
                
                if(self.Bank.count == 0)
                {
                     self.btnBankName.isHidden = true
                     self.BankDropDownIcon.isHidden = true
                }
                
                
                if(self.BankResponce != nil){
                    
                    if((self.BankResponce.AceCountryBankList?.count)! > 0){
                        
                        if(self.UpdateRecipient == nil){
                            
                             //self.txtBankName.text = self.BankResponce.AceCountryBankList![0].BankName
                        }
                       
                        
                    }
                }
                
            }else if self.BankResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.Bank.count == 0)
                {
                    self.btnBankName.isHidden = true
                    self.BankDropDownIcon.isHidden = true
                }
                
                if(self.BankResponce?.myAppResult?.Message == nil){
                    
                   // self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                  //  self.uc.errorSuccessAler("Error", (self.BankResponce?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }
    
    func getCountryList(){
        
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"2"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getCountryList()
                return
            }
            
            let CountryListingResponce = CountryListResult(JSONString:result)
            
            if CountryListingResponce?.myAppResult?.Code == 0 {
                
                self.ReceivingCountryList =  CountryListResult(JSONString:result)
            
                
                for i in 0 ..< (self.ReceivingCountryList?.AceCountryList!.count)!{
                    self.Countries.append((self.ReceivingCountryList?.AceCountryList![i].CountryName)!)
                }
                
                self.CountryDropDown.dataSource = self.Countries
                self.CountryDropDown.reloadAllComponents()
                
                if(self.ReceivingCountryList != nil){

                    if((self.ReceivingCountryList.AceCountryList?.count)! > 0){
                        
                        if self.parmsDiction != nil {
                            for country in (self.ReceivingCountryList.AceCountryList!)  {
                                print(country)
                            
                                if country.CountryName == self.parmsDiction["Country"] as? String {
                                                                        
                                    self.CountryDropDown.dataSource = [country.CountryName!]
                                    self.CountryDropDown.reloadAllComponents()
                                    self.txtCountry.text = country.CountryName
                                    self.btnCountry.isEnabled = false
                                    self.txtCountryCode.text = String("+\(country.DialingCode!)")
                                    //self.parmsDiction[""]
                                    self.txtDeliveryOption.text = self.parmsDiction["Payment_Method"] as? String
//                                    self.BtnDeliveryOption.isEnabled = false
                                    self.GetAlowPaymentMethod()
                                }
                            }
                        }
                    }
                }
                
                if(self.UpdateRecipient == nil){
                    
                    for i in 0..<(self.ReceivingCountryList?.AceCountryList?.count)! {
                    
                    
                        if(self.ReceivingCountryList?.AceCountryList![i].CountryName == "Pakistan"){
                            
                            self.countryindex = i
                            self.txtCountry.text = self.ReceivingCountryList?.AceCountryList![i].CountryName
                            self.txtCountryCode.text = "+\((self.ReceivingCountryList?.AceCountryList![i].DialingCode)!)"
                            
                        }
                    }
                    //if(self.sendingMethod != "9")
                    //{
                    self.GetAlowPaymentMethod()
                    self.getBankList(bankCode: self.ReceivingCountryList.AceCountryList![0].Iso3Code!)
                    //}
                }else{
                    
                    if let Country = self.UpdateRecipient.BeneCountryIsoCode {
                        for i in 0..<(self.ReceivingCountryList?.AceCountryList?.count)! {
                            
                            if(self.ReceivingCountryList?.AceCountryList![i].Iso3Code == Country){
                                
                                if let Country = self.ReceivingCountryList?.AceCountryList![i].CountryName {
                                    
                                    self.countryindex = i
                                    self.GetAlowPaymentMethod()
                                    
                                }
                            }
                        }
                    }
                }
            }else if CountryListingResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(CountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("", (CountryListingResponce?.myAppResult?.Message)!, self)
                }
            }
            
        }
    }
    
    func AddRecipient(){
        
        var BankCode = ""
        var url = ApiUrls.AddRecipient
        var BeneID = 0
        var BeneAddress = ""
        var BeneCity = ""
        var BenePoastalCode = ""
        var BeneIBAN = ""
        
        
        if(UpdateRecipient != nil){
            
            self.btnDeliveryOption1.isEnabled = false
            
            url = ApiUrls.UpdateRecipient
            
            if let beneID = self.UpdateRecipient.BeneID{
                
                BeneID = beneID
            }
            
            if let beneAddress = self.UpdateRecipient.BeneAddress {
                
                BeneAddress = beneAddress
            }
            
            
            if let beneCity = self.UpdateRecipient.BeneCity {
                
                BeneCity = beneCity
            }
            
            if let benePostalCode = self.UpdateRecipient.BenePostCode {
                
                BenePoastalCode = benePostalCode
                
            }
            
            if let beneIBAN = self.UpdateRecipient.BeneIBAN {
                
                BeneIBAN = beneIBAN
                
            }
            
            if let beneAddress = self.UpdateRecipient.BeneAddress{
                
                BeneAddress = beneAddress
            }
            
            self.mobilePaymentTypeId = "\(String(describing: UpdateRecipient.BenePayMethod!))" //self.paymentMethodOptionsResult.PaymentMethodOptionsList![index].PaymentTypeId!
            self.mobileCompanyName = "\(String(describing: UpdateRecipient.MobileCompanyName!))"
//                self.paymentMethodOptionsResult.PaymentMethodOptionsList![index].PaymentCompanyName!
        
            
        }
        
        if(BankResponce == nil){
            
            if(UpdateRecipient != nil){
                
                if let BeneBankCode = self.UpdateRecipient.BeneBankCode{
                    
                    BankCode = BeneBankCode
                    
                }
                
            }
            
            
        }else {
            if(BankResponce == nil){
                //if self.sendingMethod == "9" {
                     BankCode = ""
                //}
            }else {
                
                if self.sendingMethod == "9" {
                     BankCode = ""
                }else {
                    BankCode = "\((self.BankResponce.AceCountryBankList![BankIndex].BankCode!))"

                }
        
                //BankCode = ""
            }
//            if(BankResponce == nil){
//                if(self.sendingMethod != "9")
//                {
//                    BankCode = "\((self.BankResponce.AceCountryBankList![BankIndex].BankCode!))"
//                }
//            }
//            else
//            {
//                BankCode = ""
//            }
        }
        
        
        //MobileAccountNumber":self.mobileAccountNumber,
        //        "PrepaidCardNumber":self.prepaidCardNumber
        
        if self.sendingMethod == "752" {
            self.mobileAccountNumber = "00000"
        }else {
            self.mobileAccountNumber = self.txtAccountNumber.text!
        }
        
        let cardStr = self.txtSelectCardNumber.text!
        
        self.prepaidCardNumber = cardStr.replacingOccurrences(of: "-", with: "") //self.txtSelectCardNumber.text!
        print(self.prepaidCardNumber)
//        self.paymentMethodOptionsResult.PaymentMethodOptionsList![index].PaymentTypeId!
        
        let parms = ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                     "Token":ApiUrls.AuthToken,
                     "AppID":ApiUrls.AppID,
                     "BeneID":BeneID,
                     "BeneName":"\(self.txtFirstName.text!.trimmingCharacters(in: .whitespacesAndNewlines)) \(self.txtLastName.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneFirstName":"\(self.txtFirstName.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneLastName":"\(self.txtLastName.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneAddress":self.txtReceiverAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "BeneCity":BeneCity,
            "BenePostCode":BenePoastalCode,
            "BeneCountryIsoCode":self.ReceivingCountryList.AceCountryList![countryindex].Iso3Code!,
            "BenePhone":"\(self.txtCountryCode.text!.trimmingCharacters(in: .whitespacesAndNewlines))\(self.txtPhoneNo.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneBankName":"\(self.txtBankName.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneBranchName":"\(self.txtBranchName.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneAccountNumber":"\(self.txtAccountNumber.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneIBAN":BeneIBAN,
            "BeneBankCode":BankCode,
            "BeneBankBranchCode":"\(self.txtIFSECode.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BeneRelationWithSender":"\(self.txtRelationShip.text!.trimmingCharacters(in: .whitespacesAndNewlines))",
            "BenePayMethod":self.sendingMethod,
            "MobilePaymentTypeId":self.mobilePaymentTypeId,
            "MobileCompanyName":self.mobileCompanyName, "MobileAccountNumber":self.mobileAccountNumber,
            "PrepaidCardNumber":self.prepaidCardNumber] as [String : Any]
        
            
        
        uc.webServicePosthttp(urlString: url, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.AddRecipient()
                return
            }
            
            self.recipientResponce = AddRecipientResponce(JSONString:result)
            
            if self.recipientResponce?.myAppResult?.Code == 0 {
                
                let alertCOntroller = UIAlertController(title: "Success", message: self.recipientResponce?.myAppResult?.Message, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
                    
//                    self.navigationController?.popViewController(animated: true)
//                    PayoutLocation = ""
//                    PayoutLocationAddress = ""
                    let storyb = UIStoryboard(name: "Main", bundle: nil)
//                    let nextVC = storyb.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController
                    
                    if self.isFromDashboard {
                        NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: ["viewtag" : 953])
                        var isReceipientViewFound = false
                        if let viewControllers = self.navigationController?.viewControllers {
                            for vc in viewControllers {
                                if vc.isKind(of: RecipientViewController.self) {
                                    isReceipientViewFound = true
                                    if let recipientVC = vc as? RecipientViewController {
                                        let UserInfo = self.recipientResponce.Recipient
                                        if UserInfo != nil {
                                            recipientVC.UserInfo = UserInfo
                                            recipientVC.parms = self.parmsDiction
                                            PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
                                        }
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        return
                                    }
                                }
                            }
                        }
                        if !isReceipientViewFound {self.navigationController?.popViewController(animated: true)}
                        
                    }else {
                        var isReviewController = false
                        if let viewControllers = self.navigationController?.viewControllers {
                            for vc in viewControllers {
                                if vc.isKind(of: ReviewViewController.self) {
                                    isReviewController = true
                                    if let recipientVC = vc as? ReviewViewController {
                                        let UserInfo = self.recipientResponce.Recipient
                                        if UserInfo != nil {
                                            recipientVC.UserInfo = UserInfo
                                            recipientVC.parms = self.parmsDiction
                                            PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
                                        }
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        return
                                    }
                                }
                            }
                            if !isReviewController{
                                let nextVC = storyb.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController
                                let UserInfo = self.recipientResponce.Recipient
                                if UserInfo != nil {
                                    nextVC?.UserInfo = UserInfo
                                    nextVC?.parms = self.parmsDiction
                                    PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
                                }
                                self.navigationController?.pushViewController(nextVC!, animated: true)
                            }
                            
                        }else {
//                            let nextVC = storyb.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController
//                            let UserInfo = self.recipientResponce.Recipient
//                            if UserInfo != nil {
//                                nextVC?.UserInfo = UserInfo
//                                nextVC?.parms = self.parmsDiction
//                                PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
//                            }
//                            self.navigationController?.pushViewController(nextVC!, animated: true)
                        }
                        
                       /*
                        if let viewControllers = self.navigationController?.viewControllers {
                            for vc in viewControllers {
                                if vc.isKind(of: RecipientViewController.self) {
                                    if let recipientVC = vc as? RecipientViewController {
                                        let UserInfo = self.recipientResponce.Recipient
                                        if UserInfo != nil {
                                            recipientVC.UserInfo = UserInfo
                                            recipientVC.parms = self.parmsDiction
                                            PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
                                        }
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        return
                                    }
                                }
                            }
                        }else {
                            let nextVC = storyb.instantiateViewController(withIdentifier: "R") as? RecipientViewController
                            let UserInfo = self.recipientResponce.Recipient
                            if UserInfo != nil {
                                nextVC?.UserInfo = UserInfo
                                nextVC?.parms = self.parmsDiction
                                PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
                            }
                            self.navigationController?.pushViewController(nextVC!, animated: true)
                        }
                        */
                        
//                        if let viewControllers = self.navigationController?.viewControllers {
//                            for vc in viewControllers {
//                                if vc.isKind(of: DashboardViewController.self) {
//                                    print("isKind find VC")
//                                    navigationController?.popToViewController(vc, animated: false)
//                                    return
//                                }
//                            }
//                        }
                    }
                    
                    
//                    if let viewControllers = self.navigationController?.viewControllers {
//                        for vc in viewControllers {
//                            if vc.isKind(of: RecipientViewController.self) {
//                                print("isKind find VC")
////                                let UserInfo = self.recipientResponce.Recipient
////                                vc?.UserInfo = UserInfo
////                                vc?.parms = self.parmsDiction
//                                //PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
//                                self.navigationController?.popToViewController(vc, animated: false)
//                                return
//                            }
//                        }
//                    }else {
//                        let nextVC = storyb.instantiateViewController(withIdentifier: "R") as? RecipientViewController
//                        let UserInfo = self.recipientResponce.Recipient
//                        if UserInfo != nil {
//                            nextVC?.UserInfo = UserInfo
//                            nextVC?.parms = self.parmsDiction
//                            PayoutMethodobj = String(describing: UserInfo!.BenePayMethod!)
//                        }
//                        self.navigationController?.pushViewController(nextVC!, animated: true)
//                    }
                })
                alertCOntroller.addAction(action)
                self.present(alertCOntroller,animated: true,completion: nil)
                
            }else if self.recipientResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.recipientResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("", (self.recipientResponce?.myAppResult?.Message)!, self)
                }
            }
        }
    }

        func getAllowedPaymentMethodOptions() {
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultMaskType(.gradient)
                SVProgressHUD.show(withStatus: "Loading")
            }

            let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        
            let parms =  ["Token": token,"AppID":ApiUrls.AppID,"CountryIso3Code":self.ReceivingCountryList.AceCountryList![self.countryindex].Iso3Code!, "PaymentMethod":self.paymentMethodCode/*"PaymentMethodCode"*/]as [String : Any]
        
            uc.webServicePosthttp22(urlString: ApiUrls.paymentMethodOptions, params:parms , message: "Loading...", currentController: self){result in
                
                if(result == "fail")
                {
                    self.getAllowedPaymentMethodOptions()
                    return
                }
                print(result)
               self.paymentMethodOptionsResult = PaymentMethodOptionsResult(JSONString:result)
                
                if self.paymentMethodOptionsResult?.myAppResult?.Code == 0 {
                    
                    if(self.paymentMethodOptionsResult != nil && self.paymentMethodOptionsResult.PaymentMethodOptionsList != nil)
                    {
                        self.CompanyName.removeAll()
                        for i in 0 ..< (self.paymentMethodOptionsResult?.PaymentMethodOptionsList!.count)!{
                            self.CompanyName.append((self.paymentMethodOptionsResult?.PaymentMethodOptionsList![i].PaymentCompanyName)!)
                        }
                        
                        self.CompanyDropDown.dataSource = self.CompanyName
                        self.CompanyDropDown.reloadAllComponents()
                        
//                        PayoutMethodobj = self.PaymentMethodList[0]
//                        Initial_Payment_Method = self.PaymentMethodList[0]
                        
                        print(self.paymentMethodOptionsResult.PaymentMethodOptionsList!.count)
                        
                        
                        SVProgressHUD.dismiss()

                    }
                    else
                    {
                        SVProgressHUD.dismiss()
                        self.PaymentMethodList = [String]()
                        self.PaymentMethodName = [String]()
                        PayoutMethodobj = ""
                        Initial_Payment_Method = ""
                        self.uc.errorSuccessAler("Alert", "Payout method not found for this Country", self)
                    }
                    
                }else if self.paymentMethodOptionsResult?.myAppResult?.Code == 101 {
                    
                    
                    self.uc.logout(self)
                    
                    
                }else if self.paymentMethodOptionsResult?.myAppResult?.Code == 102 {
                    SVProgressHUD.dismiss()
                    self.uc.errorSuccessAler("Error", "Payment Method Not Found for the Country", self)
                    
                    
                }else{
                    
                    if(self.paymentMethodOptionsResult?.myAppResult?.Message == nil){
                        SVProgressHUD.dismiss()
                        self.uc.errorSuccessAler("Error", result, self)
                        
                    }else{
                        SVProgressHUD.dismiss()
                        self.uc.errorSuccessAler("Error", (self.paymentMethodOptionsResult?.myAppResult?.Message)!, self)
                        
                    }
                }
            }
        }
    
    
    func GetAlowPaymentMethod() {
        
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(.gradient)
            SVProgressHUD.show(withStatus: "Loading")
        }
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"CountryIso3Code":self.ReceivingCountryList.AceCountryList![self.countryindex].Iso3Code!]as [String : Any]
        
        uc.webServicePosthttp22(urlString: ApiUrls.AllowPaymentMethod, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetAlowPaymentMethod()
                return
            }
            
           self.PaymentMethodResult = PaymentMethodresult(JSONString:result)
            
            if self.PaymentMethodResult?.myAppResult?.Code == 0 {
                
                if(self.PaymentMethodResult != nil && self.PaymentMethodResult.PaymentMethodList != nil)
                {
                    
                    self.PaymentMethodList = [String]()
                    self.PaymentMethodName.removeAll()
                    self.DeliveryOption.removeAll()
                    
                    for i in 0 ..< (self.PaymentMethodResult?.PaymentMethodList!.count)!{
                        
                        
                        if(self.PaymentMethodResult!.PaymentMethodList![i].PaymentMethodID != 106)
                        { self.PaymentMethodList.append(self.PaymentMethodResult.PaymentMethodList![i].PaymentMethodCode!)
                            self.PaymentMethodName.append(self.PaymentMethodResult.PaymentMethodList![i].PaymentMethodName!)
                            self.DeliveryOption.append(self.PaymentMethodResult.PaymentMethodList![i].PaymentMethodName!)
                            
                        }
                    }
                    
                    self.DeliveryDropDown.dataSource = self.DeliveryOption
                    self.DeliveryDropDown.reloadAllComponents()
                    
                    PayoutMethodobj = self.PaymentMethodList[0]
                    Initial_Payment_Method = self.PaymentMethodList[0]
                    
                    SVProgressHUD.dismiss()
                   
                }
                else
                {
                    SVProgressHUD.dismiss()
                    self.PaymentMethodList = [String]()
                    self.PaymentMethodName = [String]()
                    PayoutMethodobj = ""
                    Initial_Payment_Method = ""
   
                    self.uc.errorSuccessAler("Alert", "Payout method not found for this Country", self)
                }
                
            }else if self.PaymentMethodResult?.myAppResult?.Code == 101 {
                
                
                self.uc.logout(self)
                
                
            }else if self.PaymentMethodResult?.myAppResult?.Code == 102 {
                
                self.uc.errorSuccessAler("Error", "Payment Method Not Found for the Country", self)
                
                
            }else{
                
                if(self.PaymentMethodResult?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.PaymentMethodResult?.myAppResult?.Message)!, self)
                }
            }
            
        }
      
    }
    
    //button Delivery function
    func buttonCashAndBankDeselect(){
        
        //cash pick up
        lblCashPickUpM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
        imgCashPickUpM.image = UIImage(named: "check-none")
        
        //bank
        lblDepositToBankM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
        imgDepositToBankM.image = UIImage(named: "check-none")
        
    }
        
        func buttonCashPickUpAndDepositToBankPressed(senderTag: Int){

            for index in PaymentMethodName {

                switch senderTag {
                case 1:
                    print("Cash")
                    lblCashPickUpM.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
                    imgCashPickUpM.image = UIImage(named: "check-enable.png")
                    lblDepositToBankM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
    //                lblDepositBankAccountM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
                    imgDepositToBankM.image = UIImage(named: "check-none")
    //                imgbtnDepositBankAccountM.image = UIImage(named: "check-none")

                    Initial_Payment_Method = "Cash"
    //                PayoutMethodobj = index
    //                Payment_Method = "Cash"

    //                self.txtPickUpLocation.text = ""
    //                self.txtPickupLocation.text = ""
//                    self.TxtDeliveryMethod.text = "Cash"

//                    DispatchQueue.main.async {
//                        SVProgressHUD.setDefaultMaskType(.gradient)
//                        SVProgressHUD.show(withStatus: "Loading")
//                    }
//                    self.getCountryPayerList("Cash")
                case 2:
                    print("Bank")
                    lblCashPickUpM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
                    imgCashPickUpM.image = UIImage(named: "check-none")
                    lblDepositToBankM.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
    //                lblDepositBankAccountM.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
                    imgDepositToBankM.image = UIImage(named: "check-enable.png")
    //                imgbtnDepositBankAccountM.image = UIImage(named: "check-enable.png")

                    Initial_Payment_Method = "Bank"
    //                PayoutMethodobj = index
    //                Payment_Method = "Bank"
    //                self.txtPickUpLocation.text = ""
    //                self.txtPickupLocation.text = ""
//                    self.TxtDeliveryMethod.text = "Bank"

//                    DispatchQueue.main.async {
//                        SVProgressHUD.setDefaultMaskType(.gradient)
//                        SVProgressHUD.show(withStatus: "Loading")
//                    }
//                    self.getCountryPayerList("Bank")
                default:
                    print("default case in CreateTransViewController")
                }


            }

        }
    
}
