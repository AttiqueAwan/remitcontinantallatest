//
//  AddRecipientViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import DropDown

class AddRecipientViewController: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtFirstName: JVFloatLabeledTextField!
    @IBOutlet weak var txtLastName: JVFloatLabeledTextField!
    @IBOutlet weak var txtReceiverAddress: JVFloatLabeledTextField!
    
    //DeliveryOptions
    @IBOutlet weak var txtDeliveryOption: JVFloatLabeledTextField! //not use
    //    @IBOutlet weak var BtnDeliveryOption: UIButton!
    @IBOutlet weak var btnDeliveryOption1: UIButton!
    @IBOutlet weak var DeliveryOptionDropDown: UIImageView!
    @IBOutlet weak var txtDeliveryOption1: UITextField!
    
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var txtCountry: JVFloatLabeledTextField!
    @IBOutlet weak var btnCountry: UIButton!
    
    @IBOutlet weak var txtCountryCode: JVFloatLabeledTextField!
    @IBOutlet weak var txtPhoneNo: JVFloatLabeledTextField!
    
    @IBOutlet weak var BankDropDownIcon: UIImageView!
    @IBOutlet weak var txtBankName: JVFloatLabeledTextField!
    @IBOutlet weak var btnBankName: UIButton!
    
    @IBOutlet weak var txtBranchName: JVFloatLabeledTextField!
    @IBOutlet weak var txtIFSECode: JVFloatLabeledTextField!
    @IBOutlet weak var txtAccountNumber: JVFloatLabeledTextField!
    @IBOutlet weak var txtRelationShip: JVFloatLabeledTextField!
    @IBOutlet weak var bankView: UIView!
    @IBOutlet weak var bankViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnRelationShip: UIButton!
    @IBOutlet weak var btnAddRecipient: UIButton!
    
    //Start Moddify 17 feb 2020 Delivery Method
    
    @IBOutlet weak var viewCashPickUpM: UIView!
    @IBOutlet weak var btnCashPickUpM: UIButton!
    @IBOutlet weak var imgCashPickUpM: UIImageView!
    @IBOutlet weak var lblCashPickUpM: UILabel!
    
    @IBOutlet weak var viewDepositToBankM: UIView!
    @IBOutlet weak var btnDepositToBankM: UIButton!
    @IBOutlet weak var imgDepositToBankM: UIImageView!
    @IBOutlet weak var lblDepositToBankM: UILabel!
    
    //End Moddify  april 2020 Delivery Method
    
    @IBOutlet weak var viewSelectCompany: UIView!
    
    @IBOutlet weak var btnSelectCompany: UIButton!
    @IBOutlet weak var txtSelectCompany: UITextField!
    
    @IBOutlet weak var txtSelectCardNumber: UITextField!
    @IBOutlet weak var viewCardNumber: UIView!
    
    @IBOutlet weak var viewChangedDeliveryMethod: UIView!
    
    //Start Moddify  april 2020
    var PaymentMethodResult         : PaymentMethodresult!
    var paymentMethodOptionsResult  : PaymentMethodOptionsResult!
    
    var PaymentMethodName = [String]()
    var PaymentMethodList = [String]()
    //End Moddify 17 feb 2020
    
    let txtFieldSetting = textFieldSetting()
    let round = RoundedCorner()
    let txtFireldSetting = textFieldSetting()
    let uc = UtilitySoftTechMedia()
    
    var Countries = [String]()
    var Bank = [String]()
    var DeliveryOption = [String]() //["Cash Pick-up","Bank Transfer"]
    var RelationShip = [String]()
    var CompanyName = [String]()
    var paymentMethod = 0
    let CountryDropDown = DropDown()
    let DeliveryDropDown = DropDown()
    let RelationDropDown = DropDown()
    let CompanyDropDown = DropDown()
    
    var recipientResponce : AddRecipientResponce!
    var ReceivingCountryList   :  CountryListResult!
    var BankResponce : BankList!
    var RelationTypeList : ListingTypesresult!
    var UpdateRecipient : RecipientListResult!
    
    var sendingMethod = "9"
    var countryindex = 0
    var BankIndex = 0
    var DeliveryIndex = 0
    var paymentMethodCode = String()
    var mobilePaymentTypeId = String()
    var mobileCompanyName = String()
    var prepaidCardNumber = String()
    var mobileAccountNumber = String()
    
    var isFromDashboard = false
    var isFromAmountDetail = false
    
    
    
    var parmsDiction:[String:Any]!
    
    // For Card number only
     let z = 4, intervalString = "-"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        //UITextField.delegate = self
        self.txtFirstName.delegate = self
        
        if UpdateRecipient != nil {
            self.btnDeliveryOption1.isEnabled = false
        }else if UpdateRecipient == nil{
            self.btnDeliveryOption1.isEnabled = true
        }
        
        self.getCountryList()
        
        
        //setting User Type dropdown
        CountryDropDown.anchorView = self.btnCountry
        CountryDropDown.dataSource = Countries
        
        CountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtDeliveryOption1.text = ""
            
            self.txtCountry.text = item
            self.txtCountryCode.text = "+\(self.ReceivingCountryList.AceCountryList![index].DialingCode!)"
            
            self.getBankList(bankCode: self.ReceivingCountryList.AceCountryList![index].Iso3Code!)
            self.countryindex = index
            
            //            self.getAllowedPaymentMethodOptions()
            self.GetAlowPaymentMethod()
            if self.Countries.count > 0 {
                self.viewChangedDeliveryMethod.isHidden = false
            }else {
                self.viewChangedDeliveryMethod.isHidden = true
            }
        }
        
        //setting Delivery Type dropdown
        DeliveryDropDown.anchorView = self.btnDeliveryOption1  //self.BtnDeliveryOption
        DeliveryDropDown.dataSource = DeliveryOption //DeliveryOption
        
        DeliveryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSelectCompany.text = ""
            
            self.txtDeliveryOption1.text = item
            self.DeliveryIndex = index
            
            print(self.PaymentMethodResult.PaymentMethodList!.count)
            
            self.paymentMethodCode = self.PaymentMethodResult.PaymentMethodList![index].PaymentMethodCode!
            
            if self.paymentMethodCode == "Cash"{
                self.sendingMethod = "9"
                self.paymentMethod = 0
                self.viewSelectCompany.isHidden = true
                self.viewCardNumber.isHidden = true
                self.bankView.isHidden = true
                
            }else if self.paymentMethodCode == "Bank" {
                
                self.paymentMethod = 1 // set only for bank is 1
                self.sendingMethod = "10"
                self.viewSelectCompany.isHidden = true
                self.viewCardNumber.isHidden = true
                self.bankView.isHidden = false
                
            }else if self.paymentMethodCode == "Wallet" {
                self.paymentMethod = 0
                self.bankView.isHidden = true
                self.viewSelectCompany.isHidden = false
                self.viewCardNumber.isHidden = true
                self.sendingMethod = "751"
                self.getAllowedPaymentMethodOptions()
                
            }else if self.paymentMethodCode == "BillPayment" {
                self.paymentMethod = 0
                self.sendingMethod = "752"
                self.viewSelectCompany.isHidden = false
                self.viewCardNumber.isHidden = true
                self.bankView.isHidden = true
                //self.getBankList(bankCode: self.UpdateRecipient.BeneCountryIsoCode!)
                self.getAllowedPaymentMethodOptions()
                
                
            }else if self.paymentMethodCode == "PrepaidCard" {
                self.paymentMethod = 0
                self.sendingMethod = "753"
                self.viewSelectCompany.isHidden = false
                self.viewCardNumber.isHidden = true
                self.bankView.isHidden = true
                self.getAllowedPaymentMethodOptions()
            }
            
            if(self.UpdateRecipient != nil && index == 1)
            {
                //self.getBankList(bankCode: self.UpdateRecipient.BeneCountryIsoCode!)
            }
        }
        
        //Company Dropdown
        CompanyDropDown.anchorView = self.btnSelectCompany
        CompanyDropDown.dataSource = CompanyName
        CompanyDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSelectCardNumber.text = ""
            self.mobilePaymentTypeId = self.paymentMethodOptionsResult.PaymentMethodOptionsList![index].PaymentTypeId!
            self.mobileCompanyName = self.paymentMethodOptionsResult.PaymentMethodOptionsList![index].PaymentCompanyName!
            
            //BillPayment
            if self.sendingMethod == "752" {
                //self.mobileAccountNumber = self.paymentMethodOptionsResult.PaymentMethodOptionsList![index].
            }
            
            self.txtSelectCompany.text = item
            if self.txtDeliveryOption1.text == "PrepaidCard" {
                self.viewCardNumber.isHidden = false
            }
            
            
        }
        
        //setting User Type dropdown
        RelationDropDown.anchorView = self.btnRelationShip
        RelationDropDown.dataSource = RelationShip
        RelationDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtRelationShip.text = item
        }
        
        if self.UpdateRecipient != nil {
            
            self.FillRecipientValues()
            
        }else {
            
            //            self.bankViewHeight.constant = 0
            //            self.bankView.isHidden = true
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePickUpLocations(_:)), name: NSNotification.Name(rawValue: ApiUrls.NotificationPayerList), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(RelationShip.count < 1)
        {
            self.getRelationType()
        }
        
    }
    


    
    @objc  func ChangePickUpLocations(_ notification:NSNotification) {
        
        let userinfo =  notification.userInfo as? [String:Any]
        let item =  userinfo!["Name"] as? String
        let index = userinfo!["index"] as? Int
        self.txtBankName.text = item
        self.BankIndex = index!
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(AddDocuentViewController.doneButtonActionReceive))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtAccountNumber.inputAccessoryView = doneToolbar
        self.txtPhoneNo.inputAccessoryView = doneToolbar
        self.txtCountryCode.inputAccessoryView = doneToolbar
        self.txtSelectCardNumber.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionReceive(){
        
        self.txtAccountNumber.resignFirstResponder()
        self.txtPhoneNo.resignFirstResponder()
        self.txtCountryCode.resignFirstResponder()
        self.txtSelectCardNumber.resignFirstResponder()
    }
    
    func FillRecipientValues(){
        
        self.lblTitle.text = "Update Recipient"
        self.btnAddRecipient.setTitle("UPDATE", for: .normal)
        
        self.txtFirstName.text = self.UpdateRecipient.BeneFirstName
        self.txtLastName.text = self.UpdateRecipient.BeneLastName
        
        if let deliveryOption = self.UpdateRecipient.BenePayMethod{
            
            
            let benePayMethod = self.UpdateRecipient.BenePayMethod
            
            var benePayCode = ""
            if benePayMethod == 9 {
                
                self.paymentMethod = 0
                self.sendingMethod = "9"
                benePayCode = "Cash"
                
            }else if benePayMethod == 10 {
                benePayCode = "Deposit To Bank"
                self.paymentMethod = 1
                self.bankView.isHidden = false
            }else if benePayMethod == 751 {
                benePayCode = "Mobile Wallet"
                self.viewSelectCompany.isHidden = false
                //self.txtSelectCompany.text = self.UpdateRecipient.ben
            }else if benePayMethod == 752 {
                benePayCode = "Bill Payment"
                self.viewSelectCompany.isHidden = false
                self.viewCardNumber.isHidden = false
            }else if benePayMethod == 753 {
                benePayCode = "Prepaid Card"
                self.viewSelectCompany.isHidden = false
                self.viewCardNumber.isHidden = false
            }
            
            self.txtDeliveryOption1.text = benePayCode
            self.txtSelectCompany.text = self.UpdateRecipient.MobileCompanyName
            self.txtSelectCardNumber.text = "\(String(describing: self.UpdateRecipient.PrepaidCardNumber!))"
            
            self.sendingMethod = "\(String(describing: self.UpdateRecipient.BenePayMethod!))"
            
            
            
        }
        
        if let Country = self.UpdateRecipient.BeneCountryIsoCode {
            
            let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
            
            let AllCountries = CountryListResult(JSONString: result)
            
            
            for i in 0..<(AllCountries?.AceCountryList?.count)! {
                
                if(AllCountries?.AceCountryList![i].Iso3Code == Country){
                    
                    if let Country = AllCountries?.AceCountryList![i].CountryName {
                        
                        
                        self.txtCountry.text = Country
                        // self.countryindex = i
                        self.txtCountryCode.text = "+\((AllCountries?.AceCountryList![i].DialingCode!)!)"
                        if let deliveryOption = self.UpdateRecipient.BenePayMethod{
                            if(deliveryOption != 9)
                            {
                                self.getBankList(bankCode: (AllCountries?.AceCountryList![i].Iso3Code!)!)
                            }
                        }
                    }
                }
            }
            
            
        }
        
        
        if let PhoneNo = self.UpdateRecipient.BenePhone {
            
            self.txtPhoneNo.text = String(PhoneNo.dropFirst((self.txtCountryCode.text?.count)!))
        }
        
        if let BankName = self.UpdateRecipient.BeneBankName {
            
            self.txtBankName.text = BankName.uppercased()
            
        }
        
        if let BeneAddress = self.UpdateRecipient.BeneAddress {
            
            self.txtReceiverAddress.text = BeneAddress.uppercased()
            
        }
        
        if let BrnachName = self.UpdateRecipient.BeneBranchName {
            
            self.txtBranchName.text = BrnachName.uppercased()
            
        }
        
        if let IFSECode = self.UpdateRecipient.BeneBankBranchCode {
            
            self.txtIFSECode.text = IFSECode.uppercased()
            
        }
        
        if let AccountNumber = self.UpdateRecipient.BeneAccountNumber {
            
            self.txtAccountNumber.text = AccountNumber.uppercased()
            
        }
        
        if let RelationShip = self.UpdateRecipient.BeneRelationWithSender {
            
            self.txtRelationShip.text = RelationShip
            
        }
        
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        //NotificationCenter.default.addObserver(self, selector: #selector(showDashboar(_:)), name: ApiUrls.ShowDashboardNotification, object: nil)
        
        if isFromDashboard {
             NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: ["viewtag" : 953])
            self.navigationController?.popViewController(animated: true)
           
        }else {
            self.navigationController?.popViewController(animated: true)
            
//            if let viewControllers = self.navigationController?.viewControllers {
//                for vc in viewControllers {
//                    if vc.isKind(of: DashboardViewController.self) {
//                        print("isKind find VC")
//                        navigationController?.popToViewController(vc, animated: false)
//                        return
//                    }
//                }
//            }
        }
        
        
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCountryClick(_ sender: Any) {
        
        if(self.UpdateRecipient == nil){
            
            self.CountryDropDown.show()
        }
        
    }
    
    @IBAction func BtnDeliveryOptionClick(_ sender: UIButton) {
        for item in self.DeliveryOption {
            print(item)
        }
        self.DeliveryDropDown.show()
        //        self.buttonCashPickUpAndDepositToBankPressed(senderTag: sender.tag)
        
    }
    
    
    @IBAction func btnBankNameClick(_ sender: Any) {
        if(txtCountry.text!.isEmpty)
        {
            self.uc.errorSuccessAler("", "Please select receiving country name.", self)
        }
        else if(self.Bank.count > 0)
        {
            self.performSegue(withIdentifier: "BankListSegue", sender: nil)
        }
        else
        {
            self.BankDropDownIcon.isHidden = true
            self.btnBankName.isHidden = true
        }
    }
    
    @IBAction func btnSelectCompanyName(_ sender: UIButton) {
        self.CompanyDropDown.show()
    }
    
    @IBAction func btnRelationShipClick(_ sender: Any) {
        
        self.RelationDropDown.show()
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
        
//        if !(self.txtDeliveryOption1.text!.isEmpty) && (self.txtDeliveryOption1.text == "Cash Pickup" || self.txtDeliveryOption1.text == "Deposit to Bank Account") {
//
//
//        }else{
//
//            if (txtSelectCompany.text!.isEmpty) {
//                uc.errorSuccessAler("", "Select Company Option", self)
//            }else if (txtSelectCardNumber.text!.isEmpty) {
//                 uc.errorSuccessAler("", "Enter Card Number", self)
//            }
//
//        }
        
        if self.txtFirstName.text!.count >= 2 && self.txtFirstName.text!.count <= 25 {
           
        }else {
            uc.errorSuccessAler("Alert", "First name should be between 2 to 25 character", self)
            return
        }
        if self.txtLastName.text!.count >= 2 && self.txtLastName.text!.count <= 25 {
           
        }else {
            uc.errorSuccessAler("Alert", "Last name should be between 2 to 25 character", self)
            return
        }
        
        if !(self.txtDeliveryOption1.text!.isEmpty) && (self.txtDeliveryOption1.text == "Cash Pickup" || self.txtDeliveryOption1.text?.lowercased() == "deposit to bank account") {
                   
                   
        }else if self.txtDeliveryOption1.text?.lowercased() == "cash"{
            
        }else if self.txtDeliveryOption1.text?.lowercased() == "deposit to bank"{
            
        }
        else if self.txtDeliveryOption1.text == "Mobile Wallet" || self.txtDeliveryOption1.text == "Bill Payment"{
            
        }
        else{
            
            if (txtSelectCompany.text!.isEmpty) {
                uc.errorSuccessAler("", "Select Company Option", self)
            }else if (txtSelectCardNumber.text!.isEmpty) {
                uc.errorSuccessAler("", "Enter Card Number", self)
            }
            
        }
        
        
        if (self.txtFirstName.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Enter first name", self)
            
            
        }else if (self.txtLastName.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Enter last name", self)
            
        }else if (self.txtCountry.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Select country", self)
            
        }else if (self.txtCountryCode.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Enter country code name", self)
            
        }else if  (self.txtPhoneNo.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Enter mobile number", self)
            
        }else if(self.txtRelationShip.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Select relationship", self)
            
        }else if  (self.paymentMethod == 0){
            
            self.AddRecipient()
            
        }else{
            
            if (self.txtBankName.text?.isEmpty)!{
                
                uc.errorSuccessAler("", "Enter bank name", self)
                
            }
//            else if (self.txtBranchName.text?.isEmpty)!{
//
//                uc.errorSuccessAler("", "Enter branch name", self)
//
//            }
//            else if (self.txtIFSECode.text?.isEmpty)!{
//                
//                uc.errorSuccessAler("", "Enter branch code", self)
//
//
//            }
            else if (self.txtAccountNumber.text?.isEmpty)!{
                
                uc.errorSuccessAler("", "Enter account no", self)
                
            }else if ((self.txtAccountNumber.text?.count)! < 5){
                
                uc.errorSuccessAler("", "Invalid account no", self)
                
            }else if (self.txtCountry.text == "India"){
                
                if((self.txtIFSECode.text?.count)! < 11){
                    
                    uc.errorSuccessAler("", "Invalid branch/IFSE code", self)
                    
                }else{
                    
                    self.AddRecipient()
                }
            }
//            else if (self.txtCountry.text != "India"){
//                
//                if((self.txtIFSECode.text?.count)! < 3){
//                    
//                    uc.errorSuccessAler("", "Invalid branch/IFSE code", self)
//                    
//                }else{
//                    
//                    self.AddRecipient()
//                }
//                
//            }
            else{
                
                self.AddRecipient()
            }
        }
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "BankListSegue"){
            
            let destination = segue.destination as? BankListViewController
            if(self.BankResponce.AcePayerBranchList != nil)
            {
                if let list = self.BankResponce.AcePayerBranchList
                {
                    destination?.CountryPayerList =  list
                }
            }
            else
            {
                destination?.CountryPayerList = []
            }
        }
        
        
        
    }
    
    //Log out
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
    
    
}

extension AddRecipientViewController:UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        
        if self.txtSelectCardNumber == textField {
            
            let nsText = textField.text! as NSString

            if range.location == 19 { return false }

            if range.length == 0 && canInsert(atLocation: range.location) {
                textField.text! = textField.text! + intervalString + string
                return false
            }

            if range.length == 1 && canRemove(atLocation: range.location) {
                textField.text! = nsText.replacingCharacters(in: NSMakeRange(range.location-1, 2), with: "")
                return false
            }

            return true
            
        }
        
        if(self.txtAccountNumber == textField){
            
            let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
            let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            var filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
            
            
            if(range.location >= 24){
                
                return range.location < 24
            }
            
            filtered = filtered.uppercased()
            
            
            return (string == filtered)
            
            
        }
        
        
        if(self.txtCountry.text == "Pakistan" || self.txtCountry.text == "Bangladesh"){
            
            return range.location < 11
            
        }else if(self.txtCountry.text == "India"){
            
            return range.location < 11
            
        }
        
        return range.location < 11
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
   

    func canInsert(atLocation y:Int) -> Bool { return ((1 + y)%(z + 1) == 0) ? true : false }

    func canRemove(atLocation y:Int) -> Bool { return (y != 0) ? (y%(z + 1) == 0) : false }
    
}




