//
//  PayoutBranchTableViewCell.swift
//  AyanExpress
//
//  Created by Softech Media on 20/03/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class PayoutBranchTableViewCell: UITableViewCell {
    @IBOutlet weak var BranchName: UILabel!
    @IBOutlet weak var BranchDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
