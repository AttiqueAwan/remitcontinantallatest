//
//  CreateTransViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown
import JVFloatLabeledTextField
import SVProgressHUD

var Country = String()
var CountryIso3Code = String()
var CurrencyIsoCode = String()
var Sending_Currency = String()
var Payment_Method = String()
var Payout_branchCode = String()
var Service_Charges = String()
var RecievingCurrency = String()
var Exchange_Rate = Double()
var PayoutMethodobj = String()
var PayoutLocation = String()
var PayoutLocationAddress = String()
var MaxAmount = Int()
var MinAmount = Int()
var PayerID = String()
var Initial_Payment_Method = String()


class CreateTransViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
   
   
    var UserInfo:RecipientListResult!
   
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnReceivingCountry: UIButton!
    
    
    @IBOutlet weak var btnPickUpLocation: UIButton!
    @IBOutlet weak var txtPickUpLocation: JVFloatLabeledTextField!
   
    //below text field is for pickup branches
    @IBOutlet weak var txtPickupLocation: JVFloatLabeledTextField!
    
    @IBOutlet weak var btnContinue: UIButton!   //yes
    
    @IBOutlet weak var PayoutBranchTblVIew: UITableView!
    @IBOutlet weak var BRANCHTABLEVIEW: UIView!
    
    
    @IBOutlet weak var txtReceivingCountry: JVFloatLabeledTextField!
    @IBOutlet weak var btnPayoutOutBranches: UIButton!

    
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var PaymentView: UIView!
    @IBOutlet weak var PickupView: UIView!

    @IBOutlet weak var BtnDeliveryMethod: UIButton!
    @IBOutlet weak var TxtDeliveryMethod: UITextFieldX!
    
    //Start Moddify 13 feb 2020 Delivery Method
    
    @IBOutlet weak var viewCashPickUpM: UIView!
    @IBOutlet weak var btnCashPickUpM: UIButton!
    @IBOutlet weak var imgCashPickUpM: UIImageView!
    @IBOutlet weak var lblCashPickUpM: UILabel!
    
    @IBOutlet weak var viewDepositToBankM: UIView!
    @IBOutlet weak var btnDepositToBankM: UIButton!
    @IBOutlet weak var imgDepositToBankM: UIImageView!
    @IBOutlet weak var lblDepositToBankM: UILabel!
    
    @IBOutlet weak var viewPayoutBranch: UIView!
    // End Moddify 13 feb 2020 Delivery Method

    let round = RoundedCorner()
    
    let uc = UtilitySoftTechMedia()
    
    var ReceivingCountryList   :  CountryListResult!
    var ExchangeRateResponce     : ExchangeRate!         //yes
    var CountryPayerListResponce : CountryPayerList!      //yes
    var BankResponce : BankList!
    var BranchListResponce : BankList!
    var PaymentMethodResult     : PaymentMethodresult!         //yes
    
    let receivingCountryDropDown = DropDown()
    let paymentMethodDropDown = DropDown()
    let PickUpLocationDropDown = DropDown()
    let PickUpBranchesDropDown = DropDown()

    var Countries = [String]()
    var PaymentMethodList = [String]()
    var PaymentMethodName = [String]()
    var PickUpLocationList = [String]()
    var PickUpBranchesList = [String]()
    var PickUpBranchesAddressList = [String]()
    var selectCompany = [String]()
    var PickUpBranchesCode = [Int]()
    var PayerId = 0                      //yes
    var countryindex = 0                    //yes
    var PaymentMethodindex = 0               //yes
    var PickUpLocationindex = 0              //yes
    var PickUpBranchesindex = 0              //yes
    
    var MaximumAmount = 0.0                //yes
    var MinimumAmount = 0.0                //yes
    
    var ExchangeRates = Double()
    //yes
    var Payments = "Cash"                //yes
    var ReceivingCountry = ""            //yes
    var BranchCode = ""
    
    override func viewDidLoad() {
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        super.viewDidLoad()
        Dashboard = false
        BRANCHTABLEVIEW.isHidden = true
        _ = uc.getUserInfo()
        
        if UserInfo != nil {
            self.btnReceivingCountry.isEnabled = false
            self.BtnDeliveryMethod.isEnabled = false

        }else{
            print("No")
        }
        
     
        DispatchQueue.main.async {
            self.GetReceivingCountryList()
        }
        
        //setting   dropdown
        receivingCountryDropDown.anchorView = self.btnReceivingCountry
        receivingCountryDropDown.dataSource = Countries
        
        
        receivingCountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            
            self.txtReceivingCountry.text = item
            self.countryindex = index

            recivingCountryM = item
            
            if let myImage = UIImage(named: "\(self.ReceivingCountryList.AceCountryList![index].CountryName!).png"){
                self.txtReceivingCountry.withImage(direction: .Left, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
            }
           
            self.txtPickUpLocation.text = ""
            self.txtPickupLocation.text = ""
            self.PickUpLocationindex = 0
            self.PayerId = 0
            self.view.setNeedsLayout()
            self.GetAlowPaymentMethod()
            
        }
        
           paymentMethodDropDown.anchorView = self.BtnDeliveryMethod
           paymentMethodDropDown.dataSource = self.PaymentMethodName
           paymentMethodDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
           print("Selected item: \(item) at index: \(index)")
               
            let paymentCode = self.PaymentMethodResult?.PaymentMethodList![index].PaymentMethodID
            
            Initial_Payment_Method = self.PaymentMethodList[index]
            
            PayoutMethodobj = String(describing: paymentCode!)//self.PaymentMethodName[index]
            
                Payment_Method = self.PaymentMethodName[index]
               self.TxtDeliveryMethod.text = self.PaymentMethodName[index]
               self.txtPickUpLocation.text = ""
               self.txtPickupLocation.text = ""
            
               DispatchQueue.main.async {
                   SVProgressHUD.setDefaultMaskType(.gradient)
                   SVProgressHUD.show(withStatus: "Loading")
               }
                
               self.getCountryPayerList(self.PaymentMethodList[index])
           }
           
      
        
        PickUpLocationDropDown.anchorView = self.btnPickUpLocation
        PickUpLocationDropDown.dataSource = PickUpLocationList

        PickUpLocationDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtPickUpLocation.text = item
            self.txtPickupLocation.text = ""
            self.PickUpLocationindex = index
            self.PayerId = index
            RecievingCurrency = self.CountryPayerListResponce.AcePayerList![index].CurrencyIsoCode!
            self.ExchangeRates = Double(self.CountryPayerListResponce.AcePayerList![index].ExchangeRate!)
            self.MaximumAmount = self.CountryPayerListResponce.AcePayerList![index].MaximumAmount!
            self.MinimumAmount = self.CountryPayerListResponce.AcePayerList![index].MinimumAmount!
            
            
            DispatchQueue.main.async {
                SVProgressHUD.setDefaultMaskType(.gradient)
                SVProgressHUD.show(withStatus: "Loading")
            }
            
            self.GetPayerBranchList()
            
        }
        
   
        
        PayoutBranchTblVIew.rowHeight = UITableView.automaticDimension
        PayoutBranchTblVIew.estimatedRowHeight = 200

        PayoutBranchTblVIew.dataSource = self
        PayoutBranchTblVIew.delegate = self

    }
    
    
    @IBAction func SelectDeliveryMethod(_ sender: UIButton) {
        
        self.paymentMethodDropDown.show()
//        self.buttonCashPickUpAndDepositToBankPressed(senderTag: sender.tag)
        print(sender.tag)
    }
    
    
    
    @IBAction func btnContinueClick(_ sender: Any) {
        
        
        if (self.txtReceivingCountry.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please select country", self)
            
        }else if(self.TxtDeliveryMethod.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Choose payment method.", self)
            
        }else if(self.txtPickUpLocation.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Choose pick up locations?", self)
            
        }else if(self.BranchCode == ""){
            
            uc.errorSuccessAler("", "Please wait....", self)
//            uc.errorSuccessAler("", "Please choose pick-up location", self)
        
        }else{
             self.performSegue(withIdentifier: "AmountDetails", sender: nil)
        }
        
    }
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnReceivingCountryClick(_ sender: Any) {
       
        self.receivingCountryDropDown.show()
        
    }
    
    @IBAction func btnSelectCompany(_ sender: UIButton) {
    }
    
    @IBAction func btnPickUpLocationClick(_ sender: Any) {
        
        self.PickUpLocationDropDown.show()
        
    }
    
    @IBAction func btnPayoutBranchesClick(_ sender: Any) {
        
         BRANCHTABLEVIEW.isHidden = false
        
    }
    
    @IBAction func HideBranchView(_ sender: Any) {
        BRANCHTABLEVIEW.isHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (self.BranchListResponce) != nil else {
            return 0
        }
        
        guard let count = self.BranchListResponce.AcePayerBranchList?.count else {
            return 0
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PayoutBranchTableViewCell = PayoutBranchTblVIew.dequeueReusableCell(withIdentifier: "cell") as! PayoutBranchTableViewCell
        
        cell.BranchName.text = PickUpBranchesList[indexPath.row]
        cell.BranchDetails.text = PickUpBranchesAddressList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.txtPickupLocation.placeholder = "Payout Branch"
        self.txtPickupLocation.text = PickUpBranchesList[indexPath.row]
        PayoutLocation = PickUpBranchesList[indexPath.row]
        PayoutLocationAddress = PickUpBranchesAddressList[indexPath.row]
        self.BranchCode = "\(self.PickUpBranchesCode[indexPath.row])"
        BRANCHTABLEVIEW.isHidden = true
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        
        if(segue.identifier == "AmountDetails"){
            
            let vc = segue.destination as? AmountDetailsViewController
            vc?.UserInfo = self.UserInfo
            
            let userinfo = uc.getUserInfo()
            
            Country = self.txtReceivingCountry.text!
            CountryIso3Code = (userinfo!.UserInfo?.CountryIsoCode)!
            Sending_Currency = (userinfo!.UserInfo?.CurrencyIsoCode)!
            Payout_branchCode = self.BranchCode
            print("Payout_branchCode = \(Payout_branchCode)")
            Exchange_Rate = Double(self.ExchangeRates)
            
        }
        
    }
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
    

}
