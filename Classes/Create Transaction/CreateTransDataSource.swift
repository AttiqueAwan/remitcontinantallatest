//
//  CreateTransDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

extension CreateTransViewController{
    
    
    func GetReceivingCountryList(){
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(.gradient)
            SVProgressHUD.show(withStatus: "Loading")
        }
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"2"]as [String : Any]
        
        print(parms)
        
        uc.webServicePosthttp22(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetReceivingCountryList()
                return
            }
            
            let CountryListingResponce = CountryListResult(JSONString:result)
            
            if CountryListingResponce?.myAppResult?.Code == 0 {
                
                self.ReceivingCountryList =  CountryListResult(JSONString:result)
            
                for i in 0 ..< (self.ReceivingCountryList?.AceCountryList!.count)!{
                    self.Countries.append((self.ReceivingCountryList?.AceCountryList![i].CountryName)!)
                    
                    if(self.PayerId != 0){
                        if self.ReceivingCountry == (self.ReceivingCountryList?.AceCountryList![i].CurrencyIsoCode)!{
                            
                            self.txtReceivingCountry.text = self.ReceivingCountryList.AceCountryList![i].CountryName
                            
                                self.ReceivingCountry = self.txtReceivingCountry.text!
                            recivingCountryM = self.txtReceivingCountry.text!
                           
                            
                            self.countryindex = i
                            
                            self.GetAlowPaymentMethod()
                        }
                    }
                }
                
                self.txtReceivingCountry.text = self.ReceivingCountryList.AceCountryList![0].CountryName

//                if let myImage = UIImage(named: "\(self.ReceivingCountryList.AceCountryList![0].CountryName!).png"){
//                    self.txtReceivingCountry.withImage(direction: .Left, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
//                }
                
                self.view.setNeedsLayout()
                self.countryindex = 0
  
                self.receivingCountryDropDown.dataSource = self.Countries
                self.receivingCountryDropDown.reloadAllComponents()
                self.GetAlowPaymentMethod()
                
                if(self.ReceivingCountryList != nil){
                    
                    if((self.ReceivingCountryList.AceCountryList?.count)! > 0){
                        
                        
                        
                    }
                }
                
            }else if CountryListingResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else if CountryListingResponce?.myAppResult?.Code == 102 {
                
                self.uc.errorSuccessAler("Error", "Receiving Country Not Found", self)
                
            }else{
                
                if(CountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (CountryListingResponce?.myAppResult?.Message)!, self)
                }
            }
            
        }
    }
    
    
    func GetAlowPaymentMethod() {
        
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(.gradient)
            SVProgressHUD.show(withStatus: "Loading")
        }
    
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"CountryIso3Code":self.ReceivingCountryList.AceCountryList![self.countryindex].Iso3Code!]as [String : Any]
        
        
        uc.webServicePosthttp22(urlString: ApiUrls.AllowPaymentMethod, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetAlowPaymentMethod()
                return
            }
            
           self.PaymentMethodResult = PaymentMethodresult(JSONString:result)
            
            if self.PaymentMethodResult?.myAppResult?.Code == 0 {
                
                if(self.PaymentMethodResult != nil && self.PaymentMethodResult.PaymentMethodList != nil)
                {
                    
                    self.PaymentMethodList = [String]()
                    self.PaymentMethodName.removeAll()
                    
                    for i in 0 ..< (self.PaymentMethodResult?.PaymentMethodList!.count)!{
                        
                        
                        if(self.PaymentMethodResult!.PaymentMethodList![i].PaymentMethodID != 106)
                        { self.PaymentMethodList.append(self.PaymentMethodResult.PaymentMethodList![i].PaymentMethodCode!)
                            self.PaymentMethodName.append(self.PaymentMethodResult.PaymentMethodList![i].PaymentMethodName!)
                        }
                        
                        //1 check from receipient list screen
                        if self.UserInfo != nil {
                            //2 check payment array is not empty
                            var pMethod = ""
                            if !self.PaymentMethodName.isEmpty {
                                if self.UserInfo.BenePayMethod == 9 {
                                    pMethod = "Cash Pick-up"
                                   
                                }else if self.UserInfo.BenePayMethod == 10 {
                                    pMethod = "Deposit to Bank Account"
                                }else if self.UserInfo.BenePayMethod == 751 {
                                    pMethod = "Mobile Wallet"
                                }else if self.UserInfo.BenePayMethod == 752 {
                                    pMethod = "Prepaid Card"
                                }else if self.UserInfo.BenePayMethod == 753 {
                                    pMethod = "Bill Payment"
                                }
                                
                                if pMethod == self.PaymentMethodName[i]{
                                    self.TxtDeliveryMethod.text = pMethod
                                    let paymentCode = self.PaymentMethodResult?.PaymentMethodList![i].PaymentMethodID
                                    
                                    
                                    Initial_Payment_Method = self.PaymentMethodList[i]
                                    PayoutMethodobj = String(describing: paymentCode!)
                                    DispatchQueue.main.async {
                                        SVProgressHUD.setDefaultMaskType(.gradient)
                                        SVProgressHUD.show(withStatus: "Loading")
                                    }
                                    //RecipientSelected = false
                                    self.getCountryPayerList(self.PaymentMethodList[i])
                                }else {
//                                    self.uc.makeToast(message: "Delivery method does not exist")
                                }
                            }
                            
                        }else {
                            PayoutMethodobj = self.PaymentMethodList[0]
                            Initial_Payment_Method = self.PaymentMethodList[0]
                        }
                        
                    }

//                    PayoutMethodobj = self.PaymentMethodList[0]
//                    Initial_Payment_Method = self.PaymentMethodList[0]
                    
                   
//                    var myTag = 0
                    if(self.PaymentMethodList.count == 1)
                    {
                        
                    }else if (self.PaymentMethodList.count > 1) {
                        //show all view and deselect all button and textFild
                        SVProgressHUD.dismiss()
//                        self.viewDepositToBankM.isHidden = false
//                        self.viewCashPickUpM.isHidden = false
//                        self.buttonCashAndBankDeselect()
//                        self.TxtDeliveryMethod.text = ""
                        Initial_Payment_Method = ""
                    }
                    // self.TxtDeliveryMethod.text = self.PaymentMethodName[0]
                    self.paymentMethodDropDown.dataSource = self.PaymentMethodName
                    self.paymentMethodDropDown.reloadAllComponents()
//                    self.getCountryPayerList(self.PaymentMethodList[0])
                }
                else
                {
                    SVProgressHUD.dismiss()
                    self.PaymentMethodList = [String]()
                    self.PaymentMethodName = [String]()
                    PayoutMethodobj = ""
                    Initial_Payment_Method = ""
                    
                    self.PickUpLocationList = [String]()
                    self.PickUpLocationDropDown.dataSource = self.PickUpLocationList
                    self.PickUpLocationDropDown.reloadAllComponents()
                    self.uc.errorSuccessAler("Alert", "Payout method not found for this Country", self)
                }
                
            }else if self.PaymentMethodResult?.myAppResult?.Code == 101 {
                
                
                self.uc.logout(self)
                
                
            }else if self.PaymentMethodResult?.myAppResult?.Code == 102 {
                
                self.uc.errorSuccessAler("Error", "Payment Method Not Found for the Country", self)
                
                
            }else{
                
                if(self.PaymentMethodResult?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.PaymentMethodResult?.myAppResult?.Message)!, self)
                }
            }
            
        }
      
    }
    
    
    //Payout Branch List
    func getCountryPayerList(_ PaymentType:String){
        
//            DispatchQueue.main.async {
//                SVProgressHUD.setDefaultMaskType(.gradient)
//                SVProgressHUD.show(withStatus: "Loading")
//            }
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ReceivingCountryIso3Code":self.ReceivingCountryList.AceCountryList![self.countryindex].Iso3Code!,"PaymentMethod":PaymentType]as [String : Any]
        self.Payments = PaymentType
        
        uc.webServicePosthttp22(urlString: ApiUrls.GetCountryPayerList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getCountryPayerList(PaymentType)
                return
            }
            
            self.CountryPayerListResponce = CountryPayerList(JSONString:result)
            
            if self.CountryPayerListResponce?.myAppResult?.Code == 0 {
             
                if(self.CountryPayerListResponce != nil && self.CountryPayerListResponce.AcePayerList != nil)
                {
                     self.PickUpLocationList = [String]()
                    self.PayerId = 0
                    RecievingCurrency = self.CountryPayerListResponce.AcePayerList![0].CurrencyIsoCode!
                    self.ExchangeRates = (self.CountryPayerListResponce.AcePayerList![0].ExchangeRate!)
                    self.MaximumAmount = self.CountryPayerListResponce.AcePayerList![0].MaximumAmount!
                    self.MinimumAmount = self.CountryPayerListResponce.AcePayerList![0].MinimumAmount!
                    MaxAmount = Int(self.CountryPayerListResponce.AcePayerList![0].MaximumAmount!)
                    MinAmount = Int(self.CountryPayerListResponce.AcePayerList![0].MinimumAmount!)
                   
                    for i in 0 ..< (self.CountryPayerListResponce?.AcePayerList!.count)!{
                        self.PickUpLocationList.append("\((self.CountryPayerListResponce?.AcePayerList![i].PayerName)!) (\((self.CountryPayerListResponce.AcePayerList![i].ExchangeRate!)))")
                    }
                    
                    let payerName = (self.CountryPayerListResponce?.AcePayerList![0].PayerName)!
                    
                
                    if (self.PickUpLocationList.count == 1)  {
//                        self.viewPayoutBranch.isHidden = true
                        self.PickupView.isHidden = true
                        if self.ExchangeRates == 0{
                            self.uc.makeToast(message: "No exchange rates available")
                        }
                        self.txtPickUpLocation.text = "\(payerName) (\(self.ExchangeRates))"
                        
//                        self.txtPickUpLocation.placeholder = "Select Payout"
//                        payoutValidtionMessage = "Please Wait..."
                    }else if (self.PickUpLocationList.count > 1){
                        self.PickupView.isHidden = false
//                        self.viewPayoutBranch.isHidden = false
                        self.txtPickUpLocation.text = ""
                    }else if (self.PickUpLocationList.count == 0){
                        self.uc.makeToast(message: "No payer available")
//                        SVProgressHUD.dismiss()
                        self.txtPickUpLocation.text = ""
                    }
                    
                    self.PickUpLocationDropDown.dataSource = self.PickUpLocationList
                    self.PickUpLocationDropDown.reloadAllComponents()
                    
                    self.GetPayerBranchList()
                    
                }
                else
                {
                    SVProgressHUD.dismiss()
                    self.PickUpLocationList = [String]()
                    
                    self.uc.errorSuccessAler("Alert", "Pickup Location Not Found for this Country", self)
                    
                    self.btnPickUpLocation.isEnabled = false
                    
                }
                
            }else if self.CountryPayerListResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
                
            }else if self.CountryPayerListResponce?.myAppResult?.Code == 102 {
                
                self.txtPickUpLocation.text = ""
                self.PickUpLocationList = [String]()
                self.PickUpLocationDropDown.dataSource = self.PickUpLocationList
                self.PickUpLocationDropDown.reloadAllComponents()
                self.ExchangeRates = 0.0
                self.CountryPayerListResponce = nil
                self.uc.errorSuccessAler("Alert", "Payout location not found for this Country", self)
                SVProgressHUD.dismiss()
                
            }else{
                
                if(self.CountryPayerListResponce.myAppResult?.Message == nil){
                    SVProgressHUD.dismiss()
                    self.uc.errorSuccessAler("Alert", result, self)
                    
                    
                }else{
                    SVProgressHUD.dismiss()
                    self.uc.errorSuccessAler("Alert", (self.CountryPayerListResponce.myAppResult?.Message)!, self)
                }
            }
            
        }
    }
    
    //yeah
    func GetPayerBranchList() {
        
//        DispatchQueue.main.async {
//            SVProgressHUD.setDefaultMaskType(.gradient)
//            SVProgressHUD.show(withStatus: "Loading")
//        }
        
        PickUpBranchesCode.removeAll()
        PickUpBranchesList.removeAll()
        PickUpBranchesAddressList.removeAll()
        let payerid = self.CountryPayerListResponce.AcePayerList![PayerId].PayerId!
        PayerID = "\(payerid)"
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"PayerID":payerid]as [String : Any]
        
        
        uc.webServicePosthttp22(urlString: ApiUrls.getPayerBranchList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetPayerBranchList()
                return
            }
            
            let SERVICECHARGES = BankList(JSONString:result)
            
            if SERVICECHARGES?.myAppResult?.Code == 0 {
                if(SERVICECHARGES != nil && SERVICECHARGES!.AcePayerBranchList != nil)
                {
                    
                    self.BranchListResponce = SERVICECHARGES
                    self.PickUpBranchesAddressList.removeAll()
                    self.PickUpBranchesList = [String]()
                    for i in 0 ..< (self.BranchListResponce?.AcePayerBranchList!.count)!{
                        let branchname = (self.BranchListResponce?.AcePayerBranchList![i].BranchName)!
                        let branchadres = (self.BranchListResponce?.AcePayerBranchList![i].BranchAddress)!
                        let branchCode = (self.BranchListResponce?.AcePayerBranchList![i].BranchCode)!
                        self.PickUpBranchesCode.append(branchCode)
                        self.PickUpBranchesList.append(branchname)
                        self.PickUpBranchesAddressList.append(branchadres)
                    }
                    
                    if (self.PickUpBranchesAddressList.count == 1){
                        self.viewPayoutBranch.isHidden = true
                         self.PayoutBranchTblVIew.reloadData()
                         self.txtPickupLocation.text = self.PickUpBranchesAddressList[0] //(SERVICECHARGES?.AcePayerBranchList![0].BranchName)!
                    }else if (self.PickUpBranchesAddressList.count > 1){
                        self.PayoutBranchTblVIew.reloadData()
                         self.viewPayoutBranch.isHidden = false
                        self.txtPickupLocation.text = ""
                    }
                    
//                    self.PayoutBranchTblVIew.reloadData()
                    self.BranchCode = "\(self.PickUpBranchesCode[0])"
                    PayoutLocation = self.PickUpBranchesList[0]
                    PayoutLocationAddress = self.PickUpBranchesAddressList[0]
                }
                else
                {
                    
                    self.PickUpBranchesList = [String]()
                    self.uc.errorSuccessAler("Alert", "Pickup Location Branches Not Found", self)
                }
                SVProgressHUD.dismiss()
            }else if SERVICECHARGES?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else if SERVICECHARGES?.myAppResult?.Code == 102 {
                 SVProgressHUD.dismiss()
                self.BranchCode = ""
                self.PickUpBranchesList = [String]()
                self.uc.errorSuccessAler("Alert", "Pickup Location Branches Not Found", self)
                
                
            }else{
                SVProgressHUD.dismiss()
                if(SERVICECHARGES?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Alert", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Alert", (SERVICECHARGES?.myAppResult?.Message)!, self)
                }
            }
            
        }
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
        
    }
    
  
     //modify start feb 06
        //button Delivery function
        func buttonCashAndBankDeselect(){
            
    //        Initial_Payment_Method = ""
    //        PayoutMethodobj = ""
    //        Payment_Method = ""
            //cash pick up
            lblCashPickUpM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
            imgCashPickUpM.image = UIImage(named: "check-none")
            
            //bank
            lblDepositToBankM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
            imgDepositToBankM.image = UIImage(named: "check-none")
            
            self.txtPickUpLocation.text = ""
            self.txtPickupLocation.text = ""
            
        }
    
    func buttonCashPickUpAndDepositToBankPressed(senderTag: Int){

        for _ in PaymentMethodName {

            switch senderTag {
            case 1:
                print("Cash")
                lblCashPickUpM.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
                imgCashPickUpM.image = UIImage(named: "check-enable.png")
                lblDepositToBankM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
//                lblDepositBankAccountM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
                imgDepositToBankM.image = UIImage(named: "check-none")
//                imgbtnDepositBankAccountM.image = UIImage(named: "check-none")

                Initial_Payment_Method = "Cash"
//                PayoutMethodobj = index
//                Payment_Method = "Cash"

//                self.txtPickUpLocation.text = ""
//                self.txtPickupLocation.text = ""
                self.TxtDeliveryMethod.text = "Cash"

                DispatchQueue.main.async {
                    SVProgressHUD.setDefaultMaskType(.gradient)
                    SVProgressHUD.show(withStatus: "Loading")
                }
                self.getCountryPayerList("Cash")
            case 2:
                print("Bank")
                lblCashPickUpM.textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
                imgCashPickUpM.image = UIImage(named: "check-none")
                lblDepositToBankM.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
                imgDepositToBankM.image = UIImage(named: "check-enable.png")

                Initial_Payment_Method = "Bank"
//                PayoutMethodobj = index
//                Payment_Method = "Bank"
//                self.txtPickUpLocation.text = ""
//                self.txtPickupLocation.text = ""
                self.TxtDeliveryMethod.text = "Bank"

                DispatchQueue.main.async {
                    SVProgressHUD.setDefaultMaskType(.gradient)
                    SVProgressHUD.show(withStatus: "Loading")
                }
                self.getCountryPayerList("Bank")
            default:
                print("default case in CreateTransViewController")
            }


        }

    }
 
}

//Yeah



extension UITextField {
    
    enum Direction {
        case Left
        case Right
    }
    
    // add image to textfield
    func withImage(direction: Direction, image: UIImage, colorSeparator: UIColor,colorBorder: UIColor){
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        mainView.layer.cornerRadius = 5
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.layer.cornerRadius = 5
        view.layer.borderWidth = CGFloat(0.5)
        view.layer.borderColor = colorBorder.cgColor
        mainView.addSubview(view)
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 12.0, y: 8.0, width: 24.0, height: 24.0)
        view.addSubview(imageView)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = colorSeparator
        mainView.addSubview(seperatorView)
        
        if(Direction.Left == direction){ // image left
            seperatorView.frame = CGRect(x: 45, y: 0, width: 5, height: 40)
            self.leftViewMode = .always
            self.leftView = mainView
        } else { // image right
            seperatorView.frame = CGRect(x: 0, y: 0, width: 5, height: 40)
            self.rightViewMode = .always
            self.rightView = mainView
        }
    
    }
    
}
