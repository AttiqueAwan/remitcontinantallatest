//
//  BankListViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 18/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import MaterialControls

class BankListViewController: UIViewController {
    var Toast = MDToast()
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtPickUpLocations: UITextField!
    @IBOutlet weak var tblPickUpLocations: UITableView!
    
    var CountryPayerList            :     [BankResult]?
    var myCountryPayerList          :     [BankResult]?
    
    let uc = UtilitySoftTechMedia()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        
        self.myCountryPayerList = self.CountryPayerList
        if(self.myCountryPayerList!.count == 0)
        {
            Toast.text = "Don't have any bank for now, Please go back and select cash option."
            Toast.duration = 3
            Toast.show()
        }
    }
    

    @IBAction func btnBackClick(_ sender: Any) {
        
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }

}
