//
//  BankListDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 18/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension BankListViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var i = 0
        
        for strCountry in self.CountryPayerList! {
            
            if(strCountry.BankName == self.myCountryPayerList![indexPath.row].BankName!){
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: ApiUrls.NotificationPayerList), object: nil, userInfo: ["Name":self.CountryPayerList![i].BankName!,"index":i])
                break
            }
            i = i + 1
        }
        
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.myCountryPayerList != nil){
            
            return (self.myCountryPayerList?.count)!
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblPickUpLocations.dequeueReusableCell(withIdentifier: "BankListCell") as? BankListCell
        
        if(self.myCountryPayerList != nil){
            
            let PayerDetails = self.myCountryPayerList![indexPath.row]
            
            cell?.imgCountryFlag.image = #imageLiteral(resourceName: "ico_about_grey")
            
            cell?.lblCountryName.text = PayerDetails.BankName
            
            return cell!
            
        }
        let PayerDetails = self.myCountryPayerList![indexPath.row]
        
        cell?.imgCountryFlag.image = #imageLiteral(resourceName: "ico_about_grey")
        
        cell?.lblCountryName.text = PayerDetails.BankName
        
        return cell!
    }
    
    
    
    
}

extension BankListViewController:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(self.CountryPayerList != nil){
            
            var chooseCountryList : [BankResult]?
            
            var mystring = "\(textField.text ?? "")\(string)"
            if(textField.text?.count == 1 && string == ""){
                
                mystring = ""
            }
            
            for strCountry in self.CountryPayerList! {
                let name = strCountry.BankName
                let range = name?.lowercased().range(of: mystring, options: .caseInsensitive, range: nil,   locale: nil)
                
                if range != nil {
                    if(chooseCountryList == nil){
                        
                        chooseCountryList = [strCountry]
                        
                    }else{
                        
                        chooseCountryList?.append(strCountry)
                    }
                    
                }
            }
            
            self.myCountryPayerList = chooseCountryList
            
            self.myCountryPayerList =  self.myCountryPayerList?.sorted(by: {($0.BankName?.trimmingCharacters(in: .whitespacesAndNewlines).first!)! < ($1.BankName?.trimmingCharacters(in: .whitespacesAndNewlines).first!)!})
            
            if(string == "" && (textField.text?.count == 1)){
                
                self.myCountryPayerList = self.CountryPayerList
                
            }
            
            
            self.tblPickUpLocations.reloadData()
        }
        return true
    }
}

