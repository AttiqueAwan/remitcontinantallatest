//
//  BankListCell.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 18/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class BankListCell: UITableViewCell {

    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var imgSelected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
