//
//  PostCodeFinderCell.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class PostCodeFinderCell: UITableViewCell {

    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblPostalCode:UILabel!
    @IBOutlet weak var lblCity:UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
