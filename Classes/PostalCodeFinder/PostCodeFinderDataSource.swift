//
//  PostCodeFinderDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension PostCodeFinderViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let PostCodeDetail = self.postCodeFinderresult.PostCodeDetail![indexPath.row]
        
        NotificationCenter.default.post(name: ApiUrls.PostCodeNotification, object: nil, userInfo: ["PostalCode":PostCodeDetail.PostCode!,"City":PostCodeDetail.City!,"StreetAddress":PostCodeDetail.Address!,"HouseNo":PostCodeDetail.HouseNo!])
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: PostCodeFinderViewController.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 135.0
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135.0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.postCodeFinderresult != nil){
            
            return (self.postCodeFinderresult.PostCodeDetail?.count)!
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblPostCode.dequeueReusableCell(withIdentifier: "PostCodeFinderCell") as? PostCodeFinderCell
        
        let PostCodeDetail = self.postCodeFinderresult.PostCodeDetail![indexPath.row]
        
        
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: "Address:  \((PostCodeDetail.HouseNo)!) \((PostCodeDetail.Address)!)")
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.1098039216, alpha: 1), range: NSRange(location:0,length:8))
        
        
        
        cell?.lblAddress.attributedText = myMutableString
        
        myMutableString = NSMutableAttributedString(string: "City:          \((PostCodeDetail.City)!)")
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.1098039216, alpha: 1), range: NSRange(location:0,length:6))
        
        
        cell?.lblCity.attributedText = myMutableString
        
        myMutableString = NSMutableAttributedString(string: "PostCode: \((PostCodeDetail.PostCode)!)")
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.1098039216, alpha: 1), range: NSRange(location:0,length:9))
        
        
        cell?.lblPostalCode.attributedText = myMutableString
        
        
        return cell!
    }
    
    
    
    
}

extension PostCodeFinderViewController:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        textField.resignFirstResponder()
        if(self.txtPostCode.text?.isEmpty)!{
            
            uc.errorSuccessAler("Alert", "Please Enter Valild Post Code", self)
            
        }else if((self.txtPostCode.text?.count)! < 4){
            
            uc.errorSuccessAler("Alert", "Please Enter Valild Post Code", self)
            
        }else{
            
            self.GetPostalCode()
            
        }
        
        return true
        
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return range.location < 9
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if(self.postCodeFinderresult != nil){


            var choosePostalCode : [PostCodeFinderDetail]?

            var mystring = "\(textField.text ?? "")\(string)"
            if(textField.text?.count == 1 && string == ""){

                mystring = ""
            }

            for strPostCode in self.postCodeFinderresult.PostCodeDetail! {
                let name = strPostCode.Address
                let range = name?.lowercased().range(of: mystring, options: .caseInsensitive, range: nil,   locale: nil)

                if range != nil {
                    if(choosePostalCode == nil){

                        choosePostalCode = [strPostCode]

                    }else{

                        choosePostalCode?.append(strPostCode)
                    }

                }
            }

            self.mypostCodeFinderresult = postCodeFinderresult.PostCodeDetail

            if(string == "" && (textField.text?.count == 1)){

                self.mypostCodeFinderresult = self.postCodeFinderresult.PostCodeDetail

            }


            self.tblPostCode.reloadData()
        }
        return true
    }
}

