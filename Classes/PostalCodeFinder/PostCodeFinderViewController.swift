//
//  PostCodeFinderViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class PostCodeFinderViewController: UIViewController {

    
    let uc = UtilitySoftTechMedia()
    var postCodeFinderresult  : PostCodeFinderresult!
    var mypostCodeFinderresult  : [PostCodeFinderDetail]!
    
    @IBOutlet weak var noPostCodeView: UIView!
    
    @IBOutlet weak var btnBack:UIButton!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var tblPostCode:UITableView!
    
    @IBOutlet weak var txtPostCode: UITextField!
    
    var postCode = ""
    let round = RoundedCorner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        
        
        self.txtPostCode.text = postCode
        if(postCode != ""){
            
            self.GetPostalCode()
        }
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnBackClick(_ sender:Any){
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: PostCodeFinderViewController.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }

    @IBAction func btnSearchClick(_ sender:Any){
        
        if(self.txtPostCode.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please enter valild post code", self)
            
        }else if((self.txtPostCode.text?.count)! < 4){
            
            uc.errorSuccessAler("", "Please enter valild post code", self)
        
        }else{
            
            self.txtPostCode.resignFirstResponder()
            self.GetPostalCode()
            
        }
        
       
    }
    
    
    func GetPostalCode(){
        
        
        var postalCode = self.txtPostCode.text?.replacingOccurrences(of: " ", with: "")
        
        let codefirst = postalCode?.dropLast(3)
        let codelast = postalCode?.dropFirst((codefirst?.count)!)
        postalCode = "\((codefirst)!) \((codelast)!)"
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,
                      "PostCode":(postalCode)!]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.PostCodeFinder, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetPostalCode()
                return
            }
            
            self.postCodeFinderresult = PostCodeFinderresult(JSONString:result)
            
            if self.postCodeFinderresult?.myAppResult?.Code == 0 {
                
                self.noPostCodeView.isHidden = true
                self.tblPostCode.reloadData()
                
            }else if self.postCodeFinderresult?.myAppResult?.Code == 102 {
                
                self.noPostCodeView.isHidden = false
                
            }else if self.postCodeFinderresult?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.postCodeFinderresult?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.postCodeFinderresult?.myAppResult?.Message)!, self)
                }
            }
        }
        
    }
    @IBAction func btnAddManuallyClick(_ sender: Any) {
        
        if((self.txtPostCode.text?.isEmpty)!){
            
            
        }else{
            
            
            NotificationCenter.default.post(name: ApiUrls.PostCodeNotification, object: nil, userInfo: ["PostalCode":(self.txtPostCode.text)!])
        }
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: PostCodeFinderViewController.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     
    */

    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
}
