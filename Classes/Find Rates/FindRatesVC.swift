//
//  FindRatesVC.swift
//  FGCMoney
//
//  Created by Mac on 07/08/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import DropDown



class FindRatesVC: UIViewController {

    @IBOutlet weak var LblCountryName: UILabel!
    @IBOutlet weak var BtnSendingCountry: UIButton!
    @IBOutlet weak var LblReceivingCountryName: UILabel!
    @IBOutlet weak var BtnReceivingCountry: UIButton!
    @IBOutlet weak var txtAmount: UITextFieldX!
    @IBOutlet weak var CalculationView: ViewStyle!
    @IBOutlet weak var LblRateApplied: UILabel!
    @IBOutlet weak var LblReceivingAmount: UILabel!
    @IBOutlet weak var LblSendingAmount: UILabel!
    @IBOutlet weak var CalculateBtn: UIButtonStyle!
    @IBOutlet weak var AmountCaption: UILabel!
    @IBOutlet weak var CalculationViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTransferFee: UILabel!
    @IBOutlet weak var amountView: ViewStyle!
    
    @IBOutlet weak var lblShowCurrency: UILabel!
    
    let uc = UtilitySoftTechMedia()
    var ExchangeRateResponce:ExchangeRate!
    var CountryListingResponce : CountryListResult!
    var ReceivingCountryListingResponce : CountryListResult!
    var Country = ""
    var selectValue2 = 0
    var selectValue = 0
    let CountryDropDown = DropDown()
    let ReceivingCOuntryDropDown = DropDown()
    //saving api response
    var countryList = [String]()
    var ReceivingcountryList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        txtAmount.addTarget(self, action: #selector(FindRatesVC.textFieldDidChange(_:)),
        for: .editingChanged)
        self.amountView.isHidden = true
        ReceivingcountryList = [String]()
        //setting User Type dropdown
        ReceivingCOuntryDropDown.anchorView = self.BtnReceivingCountry
        ReceivingCOuntryDropDown.dataSource = ReceivingcountryList
        
        
        ReceivingCOuntryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
             self.CalculateBtn.isHidden = false
            self.CalculationView.isHidden = true
            self.LblReceivingCountryName.text = item
            self.selectValue2 = index
            self.amountView.isHidden = false
            self.GetExchangeRate()
           
        }
        
        CountryDropDown.anchorView = self.BtnSendingCountry
        CountryDropDown.dataSource = countryList
        
        
        CountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.ReceivingcountryList = [String]()
            self.ReceivingCOuntryDropDown.reloadAllComponents()
            self.LblCountryName.text = item
            self.selectValue = index
            self.Country = (self.CountryListingResponce?.AceCountryList![index].Iso3Code)!
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
     
        self.getSendingCountryList()
        self.CalculateBtn.isHidden = true
        self.CalculationView.isHidden = true
        self.CalculationViewHeightConstraint.constant = 0
        self.txtAmount.isHidden = true
        self.AmountCaption.isHidden = true
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SendingDropDown(_ sender: Any) {
        CountryDropDown.show()
    }
    
    @IBAction func ReceivingDropDown(_ sender: Any) {
        if(self.LblCountryName.text == "Country Name")
        {
            self.uc.errorSuccessAler("", " ~ Select Sending Country", self)
        }
        ReceivingCOuntryDropDown.show()
    }
    
    @IBAction func EditingChanged(_ sender: UITextField) {
        self.CalculationView.isHidden = true
        CalculationViewHeightConstraint.constant = 0
    }
    
    @IBAction func CalculateRates(_ sender: Any) {
      self.txtAmount.resignFirstResponder()
      if(txtAmount.text!.isEmpty)
      {
         self.uc.errorSuccessAler("Error", " ~ Enter Amount", self)
      }
      else if(self.ExchangeRateResponce == nil)
      {
         return
      }
      else if(self.ReceivingCountryListingResponce == nil)
      {
         return
      }
      else if(self.CountryListingResponce == nil)
      {
            return
      }
      else
      {
        GetServiceCharges()
        self.CalculationView.isHidden = false
        CalculationViewHeightConstraint.constant = 185
        let rate = self.ExchangeRateResponce.AceExchangeRateResponce![0].ExchangeRate!
        self.LblRateApplied.text = String(format: "%0.2f \(String(describing: self.ReceivingCountryListingResponce.AceCountryList![selectValue2].CurrencyIsoCode!))", Double(rate))
        let amount = Double(txtAmount.text!)
        let Total = Double(amount! * rate)
        self.LblReceivingAmount.text = String(format: "%0.2f \(String(describing: self.ReceivingCountryListingResponce.AceCountryList![selectValue2].CurrencyIsoCode!))", Double(Total))
        self.LblSendingAmount.text = String(format: "%0.2f \(String(describing: self.CountryListingResponce.AceCountryList![selectValue].CurrencyIsoCode!))", Double(amount!))
      }
    }
}


extension FindRatesVC
{
    
    func GetServiceCharges() {
       
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Amount":self.txtAmount.text!,"PayerID":self.ExchangeRateResponce.AceExchangeRateResponce![0].PayerID!,"SendingCountryIso3Code":self.Country,"ReceivingCountryIso3Code":(self.ReceivingCountryListingResponce.AceCountryList![selectValue2].Iso3Code)! ]as [String : Any]
        
        print(parms)
        uc.webServicePosthttp(urlString: ApiUrls.GetPublicServiceCharges, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetServiceCharges()
                return
            }
            
            let SERVICECHARGES = BankList(JSONString:result)
            
            if SERVICECHARGES?.myAppResult?.Code == 0 {
                
                self.lblTransferFee.text = String(format: "%0.2f \(self.Country)", (SERVICECHARGES?.AceChargesResponce?.Charges!)!)
                
            }else if SERVICECHARGES?.myAppResult?.Code == 101 {
                
                
                self.uc.logout(self)
                
                
            }else if SERVICECHARGES?.myAppResult?.Code == 102 {
                self.lblTransferFee.text = "0"
                self.uc.errorSuccessAler("", "Service Charges Not Found", self)
            }else{
                
                if(SERVICECHARGES?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (SERVICECHARGES?.myAppResult?.Message)!, self)
                }
            }
            
        }
    }
    
    func GetExchangeRate(){
        
      
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Payers":"1","SendingCountryIso3Code":self.Country,"ReceivingCountryIso3Code":(self.ReceivingCountryListingResponce.AceCountryList![selectValue2].Iso3Code)!] as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetPublicExchangeRate, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetExchangeRate()
                return
            }
            
            self.ExchangeRateResponce = ExchangeRate(JSONString:result)
            
            
            if self.ExchangeRateResponce?.myAppResult?.Code == 0 {
                
                if(self.ExchangeRateResponce.AceExchangeRateResponce != nil){
                    
                    
                    let rate = self.ExchangeRateResponce.AceExchangeRateResponce![0].ExchangeRate!
                    
                    self.LblRateApplied.text = String(format: "%0.2f \(String(describing: self.ReceivingCountryListingResponce.AceCountryList![self.selectValue2].CurrencyIsoCode!))", Double(rate))
                   
                    self.lblShowCurrency.text = self.ExchangeRateResponce.AceExchangeRateResponce![0].SendingCurrencyISOCode
                    
                   
                    self.txtAmount.isHidden = false
                    self.AmountCaption.isHidden = false
                       
                }else{
                    
                    self.uc.errorSuccessAler("", "Exchange rate not availabe", self)
                }
                
                
                
            }else if self.ExchangeRateResponce?.myAppResult?.Code == 101 {
                
                
                self.uc.logout(self)
                
                
            }else{
                
                if(self.ExchangeRateResponce.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", "Some thing went wrong", self)
                    
                }else{
                    self.uc.errorSuccessAler("", (self.ExchangeRateResponce.myAppResult?.Message)!, self)
                }
            }
            
        }
    }

    func getCountryList(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"2"]as [String : Any]
        
        self.ReceivingcountryList.removeAll()
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getCountryList()
                return
            }
            
            self.ReceivingCountryListingResponce = CountryListResult(JSONString:result)
            
            if self.ReceivingCountryListingResponce?.myAppResult?.Code == 0 {
                self.ReceivingcountryList = [String]()
                
                for i in 0 ..< (self.ReceivingCountryListingResponce?.AceCountryList!.count)! {
                    
                    self.ReceivingcountryList.append((self.ReceivingCountryListingResponce?.AceCountryList![i].CountryName)!)
                    
                }
                self.ReceivingCOuntryDropDown.dataSource = self.ReceivingcountryList
                self.ReceivingCOuntryDropDown.reloadAllComponents()
                self.selectValue2 = 0
//                self.BtnSendingCountry.isEnabled = false
                //self.LblReceivingCountryName.text = self.ReceivingcountryList[0]
                
                print(self.ReceivingcountryList.count)
                
                DispatchQueue.main.async {
                    self.GetExchangeRate()
                }
                
                
                
                
            }else if self.ReceivingCountryListingResponce?.myAppResult?.Code == 101 {
                
                
                
            }else{
                
                 self.ReceivingcountryList = [String]()
                 self.ReceivingCOuntryDropDown.reloadAllComponents()
                
                if(self.ReceivingCountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Alert", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Alert", (self.ReceivingCountryListingResponce?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    func getSendingCountryList(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"1"]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getSendingCountryList()
                return
            }
            
            self.CountryListingResponce = CountryListResult(JSONString:result)
            
            if self.CountryListingResponce?.myAppResult?.Code == 0 {
                
                
                for i in 0 ..< (self.CountryListingResponce?.AceCountryList!.count)! {
                    
                    self.countryList.append((self.CountryListingResponce?.AceCountryList![i].CountryName)!)
                
                    if((self.CountryListingResponce?.AceCountryList![i].CountryName)! == "United Kingdom"){
                        
                        self.selectValue = i
                        self.Country = (self.CountryListingResponce?.AceCountryList![i].Iso3Code)!
                        
                        self.LblCountryName.text = (self.CountryListingResponce?.AceCountryList![i].CountryName!)
                        self.BtnSendingCountry.isEnabled = false
                       
                    }
                    print (i) //i will increment up one with each iteration of the for loop
//                    self.getCountryList()
                }
                self.CountryDropDown.dataSource = self.countryList
                self.CountryDropDown.reloadAllComponents()
                self.getCountryList()
                
                //call country reciving
               
                
            }else if self.CountryListingResponce?.myAppResult?.Code == 101 {
                
                
                
            }else{
                
                if(self.CountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.CountryListingResponce?.myAppResult?.Message)!, self)
                }
            }
        }
    }
}



extension FindRatesVC {
    
   @objc func textFieldDidChange(_ textField: UITextField) {
    
    if textField.text == "" {
        self.CalculateBtn.isHidden = true
    }else {
        self.CalculateBtn.isHidden = false
    }

    }
}
