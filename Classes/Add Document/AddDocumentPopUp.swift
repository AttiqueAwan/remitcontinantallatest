//
//  AddDocumentPopUp.swift
//  BanglaRemitt
//
//  Created by Softtech Media on 22/11/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import MaterialControls
import MobileCoreServices

class AddDocumentPopUp: UIViewController,UIDocumentPickerDelegate {

    @IBOutlet weak var btnGallery: UIButton!
    
    @IBOutlet weak var btnCamera: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    let imagePicker = UIImagePickerController()
    let documentInteractionController = UIDocumentInteractionController()
    
    var checkimage = ""
    var img1 = UIImageView()
    var image1URL = ""
    var imgData : Data!
    var isPdfButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        self.btnCancel.layer.cornerRadius = 8.0
        self.btnCancel.layer.masksToBounds = true
        self.preferredContentSize = CGSize(width: 280, height: 250)
        documentInteractionController.delegate = self
        // Do any additional setup after loading the view.
    }

    
    
    @IBAction func btnCameraClick(_ sender: Any) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnGalleryClick(_ sender: Any) {
        //pdf file button clicked
        if let btnSender = sender as? UIButton {
            if btnSender.tag == 3 {
                isPdfButton = true
            }
        }
        
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func btnPDFClick(_ sender: Any) {
        attachDocument()
        
    }
    
    private func attachDocument() {
        
        let types = [kUTTypePDF]
        let importMenu = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
        
        if #available(iOS 11.0, *) {
            importMenu.allowsMultipleSelection = true
        }
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        
        self.present(importMenu, animated: true)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }

}


extension AddDocumentPopUp:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
            
            let image = info[.originalImage] as? UIImage
            
            if(picker.sourceType == .camera){
                
                let imageURL = self.saveImage(imageName: "\(arc4random()).png", image!)
             self.image1URL = imageURL
                
            }else{
                
                if #available(iOS 11.0, *) {
                    let imageURL = info[UIImagePickerController.InfoKey.imageURL] as! URL
                    self.image1URL =  imageURL.absoluteString
                } else {
                    // Fallback on earlier versions
                }
            }

            self.img1.image = image
            let  imageData = image!.pngData()
            self.imgData = imageData
            
        
        NotificationCenter.default.post(name: ApiUrls.kNotification, object: nil, userInfo: ["image":self.img1.image!,"checkimage":self.checkimage,"imageData":imgData,"imgURL":self.image1URL])
        
        dismiss(animated:false, completion: {
        
            self.dismiss(animated:true, completion: nil)
//            self.navigationController?.popViewController(animated: true)

        })
    }
    
    func saveImage(imageName: String,_ imageselected:UIImage)-> String{
        //create an instance of the FileManager
        let fileManager = FileManager.default
        //get the image path
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        //get the image we took with camera
        let image = imageselected
        //get the PNG data for this image
        let data = image.pngData()
        //store it in the document directory
        fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
        return imagePath
    }
}


extension AddDocumentPopUp: UIDocumentInteractionControllerDelegate {
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("Url is : \(urls)")
        do
        {
            let filedata = try Data.init(contentsOf: urls[0])
            //FileBase64Str = filedata.base64EncodedString(options: .init(rawValue: 0))
            
            //self.UpdateCheckListInfo(DocType: self.CurrentType, FileName: FileBase64Str)
            print("file bytes are : \(filedata)")
            NotificationCenter.default.post(name: ApiUrls.pdfNotification, object: nil, userInfo: ["fileData":filedata,"fileURL":urls[0],"checkimage":self.checkimage])
        }
        catch(let err)
        {
            print(err)
        }
        
        self.dismiss(animated:true, completion: nil)
        
    }
}
