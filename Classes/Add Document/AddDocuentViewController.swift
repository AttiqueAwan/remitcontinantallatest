//
//  AddDocuentViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 21/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import DropDown
import Jelly
import SDWebImage
import MaterialControls
import MobileCoreServices


class AddDocuentViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate {

    
    @IBOutlet weak var ScrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblBackSide: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDocumentType: JVFloatLabeledTextField!
    @IBOutlet weak var txtDocumentNumber: JVFloatLabeledTextField!
    @IBOutlet weak var txtIssueDate: JVFloatLabeledTextField!
    @IBOutlet weak var txtExpiryDate: JVFloatLabeledTextField!
    
    @IBOutlet weak var btnDocumentTYpe: UIButton!
    @IBOutlet weak var btnIssueDate: UIButton!
    @IBOutlet weak var btnExpiryDate: UIButton!
    @IBOutlet weak var btnImage1: UIButton!
    @IBOutlet weak var btnImage2: UIButton!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img1View: UIView!
    @IBOutlet weak var img2View: UIView!
    @IBOutlet weak var btnAddDocument: UIButton!
    @IBOutlet weak var lblimg1: UILabel!
    @IBOutlet weak var lblimg2: UILabel!
    
    @IBOutlet weak var img1Lbl: UILabel!
    @IBOutlet weak var img2Lbl: UILabel!
    @IBOutlet weak var txtDocumentDescription: JVFloatLabeledTextField!
    @IBOutlet weak var DescriptionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var DocumentNumberTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var DocumentDescriptionView: UIView!
    
    @IBOutlet weak var TxtissuerCountry: JVFloatLabeledTextField!
    @IBOutlet weak var IssuerCountryBtn: UIButton!
    @IBOutlet weak var IssuerCountryViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var IssuerCountryViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var IssuerCountryView: UIView!
    
    var jellyAnimator: Animator?
    
    
    var Toast = MDToast()
    
    let round = RoundedCorner()
    let txtFieldSetting = textFieldSetting()
    
    var documentResponce : DocumentList!
    
    var DocumentUpdate : DocumentListResult!
    
    var DocumentTypeList : ListingTypesresult!
    
    //Date Picker
    var IssuedatePicker = MDDatePickerDialog()
    var ExpirydatePicker = MDDatePickerDialog()
    
    var DocumentType = [String]()
    var DocumentTypeName = [String]()
    var Docindex = Int()
    
    let uc = UtilitySoftTechMedia()
    let DocumentTypeDropDown = DropDown()
    
    
    var Nationality = [String]()
    var NationalityISOCode = [String]()
    let NationalityDropDown = DropDown()
    var nationalityIndex = 0
    
    
     var checkpicker = ""
    var checkimage = ""
    var image1URL = ""
    var Image2URL = ""
    var doc1FileDataType = "image"
    var doc2FileDataType = "image"
    var file1Data = Data();
    var file2Data = Data();
    //show jelly popup
    
    var nextVc = UIViewController()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        self.AllCountryList()
        self.getDocumnetTypeList()
        
//        self.txtDocumentDescription.isHidden = true
//        self.DocumentNumberTopConstraint.constant = 0
//        self.DescriptionViewHeightConstraint.constant = 0
//        self.IssuerCountryViewHeightConstraint.constant = 0
//        self.IssuerCountryViewTopConstraint.constant = 0
//        self.TxtissuerCountry.isHidden = true
//        self.IssuerCountryView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(SelectImage(_:)), name: ApiUrls.kNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SelectFile(_:)), name: ApiUrls.pdfNotification, object: nil)
        
//        self.lblimg1 = round.RoundLabel(self.lblimg1)
//        self.lblimg2 = round.RoundLabel(self.lblimg2)
        
        
        //setting User Type dropdown
        DocumentTypeDropDown.anchorView = self.btnDocumentTYpe
        DocumentTypeDropDown.dataSource = DocumentTypeName
        
        
        DocumentTypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.ScrollViewHeight.constant = 700
            self.txtDocumentType.text = item
            let itemId = self.DocumentType[index]
            
            self.Docindex = index
            if(itemId == Constants.PASSPORT_ID) {
                
                self.img2View.isHidden = true
                self.IssuerCountryView.isHidden = false
                self.lblBackSide.isHidden = true
                self.lblBackSide.isHidden = true
                
            }else if (itemId == Constants.NATIONAL_CARD_ID){
                
                self.img2View.isHidden = false
                self.lblBackSide.isHidden = false
                self.IssuerCountryView.isHidden = false
                self.ScrollViewHeight.constant = 740
                
            }else if(itemId == Constants.RESIDENT_CARD_ID){
                self.IssuerCountryView.isHidden = false
                self.img2View.isHidden = false
                self.ScrollViewHeight.constant = 740
            }else if (itemId == Constants.DRIVING_LIC_ID){
                self.IssuerCountryView.isHidden = false
                self.img2View.isHidden = false
                self.lblBackSide.isHidden = false
                self.ScrollViewHeight.constant = 740
                
            }else  if(itemId == Constants.OTHERS_ID)
            {
                self.txtDocumentDescription.isHidden = false
                self.IssuerCountryView.isHidden = true
                self.img2View.isHidden = true
                self.lblBackSide.isHidden = true
                self.ScrollViewHeight.constant = 550
            }else if(itemId == Constants.BANK_STATEMENT_ID){
                self.IssuerCountryView.isHidden = true
                self.img2View.isHidden = true
                self.lblBackSide.isHidden = true
                self.ScrollViewHeight.constant = 550
            }else if (itemId == Constants.UTILITY_BILLS_ID){
                self.IssuerCountryView.isHidden = true
                self.img2View.isHidden = true
                self.ScrollViewHeight.constant = 550
                self.lblBackSide.isHidden = true
            }else if (itemId == Constants.CREDIT_CARD_ID){
                self.IssuerCountryView.isHidden = true
                self.img2View.isHidden = true
                self.lblBackSide.isHidden = true
                self.ScrollViewHeight.constant = 550
            }
        }
        
        //setting User Type dropdown
        NationalityDropDown.anchorView = self.IssuerCountryBtn
        NationalityDropDown.dataSource = Nationality
        
        NationalityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.nationalityIndex = index
            self.TxtissuerCountry.text = item
        }
        
        
        IssuedatePicker.delegate = self
        ExpirydatePicker.delegate = self
        ExpirydatePicker.minimumDate = Date()
       
        self.addDoneButtonOnKeyboard()
     
        
        if(self.DocumentUpdate != nil){
            fillValues()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(self.DocumentType.count < 1){
            
            if(self.DocumentUpdate != nil){
                self.btnAddDocument.setTitle("Update", for: .normal)
//                self.lblTitle.text = "Update Document"
//                self.lblimg1.text = "Front side doc"
//                self.lblimg2.text = "Back side doc"
                
            }else{
                
                self.btnAddDocument.setTitle("Add", for: .normal)
                self.lblTitle.text = "Add Document"
            }
            
        }
    }
    
    
    func AllCountryList(){
        
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":""]as [String : Any]
        self.Nationality.removeAll()
        self.NationalityISOCode.removeAll()
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.AllCountryList()
                return
            }
            
            let AllCountries = CountryListResult(JSONString: result)
            
            if AllCountries?.myAppResult?.Code == 0 {
                
                let AllCountries = CountryListResult(JSONString: result)
                
                let count = AllCountries!.AceCountryList!.count
                for i in 0..<count {
                    self.Nationality.append((AllCountries?.AceCountryList![i].CountryName) ?? "")
                    self.NationalityISOCode.append((AllCountries?.AceCountryList![i].Iso3Code) ?? "")
                }
                self.NationalityDropDown.dataSource = self.Nationality
                self.NationalityDropDown.reloadAllComponents()
                
                UserDefaults.standard.set(result, forKey: "AllCountriesList")
                
                
            }else if AllCountries?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(AllCountries?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (AllCountries?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    
    
    func fillValues(){
        
        if let DocumentTypeName = self.DocumentUpdate.DocTypeName{
            self.img1Lbl.isHidden = true
            self.img2Lbl.isHidden = true
            self.txtDocumentType.text = DocumentTypeName
            
            if(DocumentTypeName == "Others")
            {
                
//              self.txtDocumentDescription.isHidden = false
//              self.DocumentNumberTopConstraint.constant = 15
//              self.DescriptionViewHeightConstraint.constant = 60
                self.lblBackSide.isHidden = true
                self.img2View.isHidden = true
                self.img1Lbl.isHidden = true
                self.ScrollViewHeight.constant = 550
            }
             if let description = self.DocumentUpdate.DocTypeOther
             {
                self.txtDocumentDescription.text = description
             }
            if(DocumentTypeName == "Licence" || DocumentTypeName == "ID-Card" ){
                self.img2View.isHidden = false
                self.ScrollViewHeight.constant = 740
//                self.IssuerCountryViewHeightConstraint.constant = 60
//                self.IssuerCountryViewTopConstraint.constant = 15
//                self.TxtissuerCountry.isHidden = false
                self.IssuerCountryView.isHidden = false
                
                if let IssuerCountryIsoCode = self.DocumentUpdate.IssuerCountryIsoCode{
                    let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                    
                    let AllCountries = CountryListResult(JSONString: result)
                    
                    
                    for i in 0..<(AllCountries?.AceCountryList?.count)! {
                        
                        if(AllCountries?.AceCountryList![i].Iso3Code == IssuerCountryIsoCode){
                            
                            
                            if let Country = AllCountries?.AceCountryList![i].CountryName {
                                self.TxtissuerCountry.text = Country
                                nationalityIndex = i
                            }
                            break
                        }
                    }
                    
                }
                
            }else if(DocumentTypeName == "Passport"){
                    self.img2View.isHidden = true
                    self.img1Lbl.isHidden = true
                    self.lblBackSide.isHidden = true
//                    self.IssuerCountryViewHeightConstraint.constant = 60
//                    self.IssuerCountryViewTopConstraint.constant = 15
//                    self.TxtissuerCountry.isHidden = false
//                    self.IssuerCountryView.isHidden = false
                self.IssuerCountryView.isHidden = false
                    
                    if let IssuerCountryIsoCode = self.DocumentUpdate.IssuerCountryIsoCode{
                        let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
                        
                        let AllCountries = CountryListResult(JSONString: result)
                        
                        
                        for i in 0..<(AllCountries?.AceCountryList?.count)! {
                            
                            if(AllCountries?.AceCountryList![i].Iso3Code == IssuerCountryIsoCode){
                                
                                
                                if let Country = AllCountries?.AceCountryList![i].CountryName {
                                    self.TxtissuerCountry.text = Country
                                    nationalityIndex = i
                                }
                                break
                            }
                        }
                        
                    }
                    
            }else if (DocumentTypeName == "ID-Card" ){
                  self.IssuerCountryView.isHidden = true
                  self.img2View.isHidden = true
                  self.ScrollViewHeight.constant = 740
            }
            else
            {
//                self.IssuerCountryViewHeightConstraint.constant = 0
//                self.IssuerCountryViewTopConstraint.constant = 0
//                self.TxtissuerCountry.isHidden = true
//                self.img2View.isHidden = true
//                self.IssuerCountryView.isHidden = true
                
            }
            
        }
        
        if let DocumentNumber = self.DocumentUpdate.DocNumber{
            
            self.txtDocumentNumber.text = DocumentNumber
        }
        
        if let DocIssueDate = self.DocumentUpdate.DocIssueDate{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            var Todaydate = dateFormatter.date(from: "\((DocIssueDate.components(separatedBy: "T")[0]))")
            
            if(Todaydate == nil){
                
                dateFormatter.dateFormat = "dd-MM-yyyy"
                Todaydate = dateFormatter.date(from: "\((DocIssueDate.components(separatedBy: "T")[0]))")
            
                self.txtIssueDate.text = "\((Todaydate)!)"
               
                
            }else{
                
                let seperatedIssue = DocIssueDate.components(separatedBy: "T")[0]
                
                let issue = seperatedIssue.components(separatedBy: "-")
                
                self.txtIssueDate.text = "\((issue[2]))-\((issue[1]))-\((issue[0]))"
                
            }
        }
        
        if let DocExpiryDate = self.DocumentUpdate.DocExpireDate{
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            var Todaydate = dateFormatter.date(from: "\((DocExpiryDate.components(separatedBy: "T")[0]))")
            
            if(Todaydate == nil){
                
                dateFormatter.dateFormat = "dd-MM-yyyy"
                Todaydate = dateFormatter.date(from: "\((DocExpiryDate.components(separatedBy: "T")[0]))")
                
                self.txtExpiryDate.text = "\((Todaydate)!)"
                
                
            }else{
                
                let seperatedIssue = DocExpiryDate.components(separatedBy: "T")[0]
                
                
                let issue = seperatedIssue.components(separatedBy: "-")
                
                self.txtExpiryDate.text = "\((issue[2]))-\((issue[1]))-\((issue[0]))"
                
            }
            
        }
        
        if let Docurl = self.DocumentUpdate.DocFileUrl{

               self.btnImage1.setImage(UIImage(named: ""), for: .normal)
                self.img1.sd_setImage(with: URL(string: Docurl), placeholderImage: UIImage(named: "add-new doc.png"))
                self.image1URL = Docurl
          
        }
       
        if let DocBackUrl = self.DocumentUpdate.DocFileBackUrl{
            self.btnImage2.setImage(UIImage(named: ""), for: .normal)
            if(DocBackUrl == "")
             {
//                lblimg2.isHidden = true
//                img2.isHidden = true
//                btnImage2.isHidden = true
            }
             else
              {
                self.img2.sd_setImage(with: URL(string: DocBackUrl), placeholderImage: UIImage(named: "add-new doc.png"))
                self.Image2URL = DocBackUrl
             }
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDocumentTypeClick(_ sender: Any) {
        if(self.DocumentUpdate == nil){
          DocumentTypeDropDown.show()
        }
    }
    
    @IBAction func BtnIssuerCountryClick(_ sender: UIButton) {
        if(self.DocumentUpdate == nil){
          self.NationalityDropDown.show()
        }
    }
    
    @IBAction func btnIssueDateClick(_ sender: Any) {
        
        self.view.endEditing(true)
        checkpicker = "Issue"
        IssuedatePicker.show()
        
    }
    
    @IBAction func btnExpiryDateClick(_ sender: Any) {
        
        self.view.endEditing(true)
        checkpicker = "Expiry"
        ExpirydatePicker.show()
        
    }
    
    
    @IBAction func btnImage1Click(_ sender: Any) {
        
        
        
        if(self.DocumentUpdate != nil){
            
            self.performSegue(withIdentifier: "ShowImage", sender: self.image1URL)
            
        }
        else{
        var customBlurFadeInPresentation = FadePresentation()
                  customBlurFadeInPresentation.presentationUIConfiguration.backgroundStyle = .blurred(effectStyle: .dark)
                  customBlurFadeInPresentation.presentationSize.width = .custom(value: 320)
                  customBlurFadeInPresentation.presentationSize.height = .custom(value: 280)
              customBlurFadeInPresentation.presentationUIConfiguration.cornerRadius = 15.0
                  let viewcontroller =  self.storyboard?.instantiateViewController(withIdentifier: "AddDocumentPopUp") as? AddDocumentPopUp
                  
                  viewcontroller?.checkimage = "img1"
                  self.jellyAnimator = Animator(presentation:customBlurFadeInPresentation)
                  self.jellyAnimator?.prepare(presentedViewController: viewcontroller!)
                  self.present(viewcontroller!, animated: true, completion: nil)
                  
                  checkimage = "img1"
        }
            
//        else{
//
//            let alert = UIAlertController(title: "Choose front document image", message: nil, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//                self.openCamera()
//            }))
//
//            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
//                self.openGallery()
//            }))
//
//            alert.addAction(UIAlertAction(title: "Pdf File", style: .default, handler: { _ in
//
//                self.pdfFile_Func()
//
//            }))
//
//            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//            if let popoverController = alert.popoverPresentationController {
//                popoverController.sourceView = self.view
//                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//                popoverController.permittedArrowDirections = []
//            }
//            checkimage = "img1"
//            self.present(alert, animated: true, completion: nil)
//
////            checkimage = "img1"
//        }
       
    }
    
    @IBAction func btnImage2Click(_ sender: Any) {
        
        if(self.DocumentUpdate != nil){
            
            self.performSegue(withIdentifier: "ShowImage", sender: self.Image2URL)
            
        }
        else {
            var customBlurFadeInPresentation = FadePresentation()
            customBlurFadeInPresentation.presentationUIConfiguration.backgroundStyle = .blurred(effectStyle: .dark)
            customBlurFadeInPresentation.presentationSize.width = .custom(value: 320)
            customBlurFadeInPresentation.presentationSize.height = .custom(value: 280)
            
            
            customBlurFadeInPresentation.presentationUIConfiguration.cornerRadius = 15.0
            let viewcontroller =   self.storyboard?.instantiateViewController(withIdentifier: "AddDocumentPopUp") as? AddDocumentPopUp
            
            viewcontroller?.checkimage = "img2"
            self.jellyAnimator = Animator(presentation:customBlurFadeInPresentation)
            self.jellyAnimator?.prepare(presentedViewController: viewcontroller!)
            self.present(viewcontroller!, animated: true, completion: nil)
            
            checkimage = "img2"
        }

//        else{
//            let alert = UIAlertController(title: "Choose back document image", message: nil, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//                self.openCamera()
//            }))
//
//            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
//                self.openGallery()
//            }))
//
//            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//
//            if let popoverController = alert.popoverPresentationController {
//                popoverController.sourceView = self.view
//                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//                popoverController.permittedArrowDirections = []
//            }
//
//            checkimage = "img2"
//            self.present(alert, animated: true, completion: nil)
//
//
////            checkimage = "img2"
//        }

    }
    
    
    
    func openCamera()
         {
             if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                 let imagePicker = UIImagePickerController()
                 imagePicker.delegate = self
                 imagePicker.sourceType = UIImagePickerController.SourceType.camera
                 imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .fullScreen
                 self.present(imagePicker, animated: true, completion: nil)
             }
             else
             {
                 let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                 alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
             }
         }
         
         func openGallery()
         {
             if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                 let imagePicker = UIImagePickerController()
                 imagePicker.delegate = self
                 imagePicker.allowsEditing = true
                 imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.modalPresentationStyle = .fullScreen
                 self.present(imagePicker, animated: true, completion: nil)
             }
             else
             {
                 let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
                 alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
             }
         }
         
         func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          if (info[.originalImage] as? UIImage) != nil {
                  let image = info[.originalImage] as? UIImage
                      
                      if(picker.sourceType == .camera){
                          
                          let imageURL = self.saveImage(imageName: "\(arc4random()).png", image!)
                         if(self.checkimage == "img1"){
                                    
                                   self.image1URL = imageURL
                            
                         }else if(self.checkimage == "img2"){
                                    
                                   self.Image2URL = imageURL
                         }
                          
                      }else{
                          
                          if #available(iOS 11.0, *) {
                              let imageURL = info[UIImagePickerController.InfoKey.imageURL] as! URL
                              if(self.checkimage == "img1"){
                                                               
                                 self.image1URL = imageURL.absoluteString
                                                       
                            }else if(self.checkimage == "img2"){
                                                               
                                self.Image2URL = imageURL.absoluteString

                            }
                              
                          } else {
                              // Fallback on earlier versions
                          }
                      }
                      
                      
                    if(self.checkimage == "img1"){
                               
                               self.img1.image = image
                              
                               
                    }else if(self.checkimage == "img2"){
                               
                               self.img2.image = image
                              
                    }
                     
            self.btnImage1.setImage(UIImage(named: ""), for: .normal)
            self.btnImage2.setImage(UIImage(named: ""), for: .normal)
             }
             picker.dismiss(animated: true, completion: nil)
         }
         
         func convertToBase64(image: UIImage) -> String {
             return image.pngData()!.base64EncodedString()
         }
      
      func saveImage(imageName: String,_ imageselected:UIImage)-> String{
             //create an instance of the FileManager
             let fileManager = FileManager.default
             //get the image path
             let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
             //get the image we took with camera
             let image = imageselected
             //get the PNG data for this image
             let data = image.pngData()
             //store it in the document directory
             fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
             return imagePath
             
         }
    
    @objc  func SelectImage(_ notification:NSNotification) {
        
        let userinfo =  notification.userInfo as? [String:Any]
        let image =  userinfo!["image"] as? UIImage
        checkimage = (userinfo!["checkimage"] as? String)!
        //let imageData = userinfo!["imageData"] as? NSData
        let imageURL = userinfo!["imgURL"] as? String
        
        if(self.checkimage == "img1"){
            
            self.img1.image = image
            self.image1URL =  imageURL!
            self.img1Lbl.isHidden = true
            self.btnImage1.setImage(nil, for: .normal)
            
        }else if(self.checkimage == "img2"){
            
            self.img2.image = image
            self.Image2URL =  imageURL!
            self.img2Lbl.isHidden = true
            self.btnImage2.setImage(nil, for: .normal)
        }
        
//        self.nextVc.view.removeFromSuperview()
//        self.nextVc.removeFromParent()
    }
    
    @objc  func SelectFile(_ notification:NSNotification) {
           
           let userinfo =  notification.userInfo as? [String:Any]
           
           let fileURL =  userinfo!["fileURL"] as! URL
           
           checkimage = (userinfo!["checkimage"] as? String)!
           //let imageData = userinfo!["imageData"] as? NSData
     
           
           if(self.checkimage == "img1"){
               
               self.doc1FileDataType = "pdf"
               self.img1.image = UIImage(named: "pdf.png")
               self.image1URL = fileURL.absoluteString
               self.file1Data =  userinfo!["fileData"] as! Data
               
           }else if(self.checkimage == "img2"){
               
               self.doc2FileDataType = "pdf"
               self.img2.image = UIImage(named: "pdf.png")
               self.Image2URL = fileURL.absoluteString;
               self.file2Data =  userinfo!["fileData"] as! Data
           }
       }
    
    @IBAction func btnAddDocumentClick(_ sender: Any) {
        
        if(self.txtDocumentType.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please select document type", self)
            
        }else if(self.txtDocumentNumber.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please enter document number", self)
            
        }else if(self.txtIssueDate.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please select issue date", self)
            
        }else if(self.txtExpiryDate.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please select expiry date", self)
            
        }else if(self.img1.image == nil){
            uc.errorSuccessAler("", "Upload front side document", self)
        }else if( self.txtDocumentType.text == "Driving Licence" || self.txtDocumentType.text == "NATIONAL-ID-Card"){
           if(self.img2.image == nil && self.DocumentUpdate == nil)
           {
                uc.errorSuccessAler("", "Upload back side document", self)
            }
            else
           {
               self.addDocument(ApiUrls.AddDocumentList,0)
            }
        }
        else{
            
            if(self.DocumentUpdate != nil){
                
                self.addDocument(ApiUrls.UpdateDocument, self.DocumentUpdate.DocID!)
                
            }else{
                
                self.addDocument(ApiUrls.AddDocumentList,0)
                
            }
            
        }
        
    }
    

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "ShowImage"){
            
            let destination = segue.destination as? DocumentImageViewController
            destination?.DocumentURL = (sender as? String)!
            
        }
    }
    

    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(AddDocuentViewController.doneButtonActionReceive))
        done.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtDocumentNumber.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonActionReceive(){
        
        self.txtDocumentNumber.resignFirstResponder()
    }
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }

}
