//
//  AddDocumentDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 27/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import MaterialControls

// setting up date picker delegate method
extension AddDocuentViewController:MDDatePickerDialogDelegate{
    
    //open datepicker dialog and set up value to label on done
    func datePickerDialogDidSelect(_ date: Date) {
        
        let formater = DateFormatter()
        formater.dateFormat = "dd-MM-yyyy"
        
        let newDate = formater.string(from: date)
        
        if(self.checkpicker == "Issue"){
           
            if (date.timeIntervalSinceNow.sign == .minus) {
                // date is in past
                self.txtIssueDate.text = newDate
            }
            else{
                self.uc.errorSuccessAler("", "Please select some past Date from current date for document. ", self)
                self.txtIssueDate.text = ""
            }
            
        }
        else if(self.checkpicker == "Expiry")
        {
            if (date.timeIntervalSinceNow.sign == .plus) {
                // date is in future
                 self.txtExpiryDate.text = newDate
            }
            else{
                self.uc.errorSuccessAler("", "Please select some future Date from current date for document. ", self)
                self.txtExpiryDate.text = ""
            }
            
        }
    }
}

extension AddDocuentViewController
{
    
    func getDocumnetTypeList(){
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ListType":"DOCUMENTTYPES"]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.GetLists, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getDocumnetTypeList()
                return
            }
            
            self.DocumentTypeList = ListingTypesresult(JSONString:result)
            
            if  self.DocumentTypeList?.myAppResult?.Code == 0 {
                
                for i in 0..<(self.DocumentTypeList.ListType?.count)!{
//                    if(ApiUrls.TotalDoc == 0)
//                    {
//                        let txt = self.DocumentTypeList.ListType![i].Text!
//                        if(txt == "Passport" || txt == "Licence" || txt == "ID-Card" || txt == "Resident Card")
//                        {
//                            self.DocumentTypeName.append(self.DocumentTypeList.ListType![i].Text!)
//                            self.DocumentType.append(self.DocumentTypeList.ListType![i].ID!)
//                        }
//                    }
//                    else
//                    {
                        self.DocumentTypeName.append(self.DocumentTypeList.ListType![i].Text!)
                        self.DocumentType.append(self.DocumentTypeList.ListType![i].ID!)
//                    }
                    
                    
                    if(self.DocumentUpdate != nil){
                        
                        if(self.DocumentUpdate.DocType == "\((self.DocumentTypeList.ListType![i].ID)!)"){
                            self.Docindex = i
                        }
                    }
                }
                
                self.DocumentTypeDropDown.dataSource = self.DocumentTypeName
                self.DocumentTypeDropDown.reloadAllComponents()
                
                
            }else if  self.DocumentTypeList?.myAppResult?.Code == 102{
                
                
                
            }else{
                
                if( self.DocumentTypeList?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", ( self.DocumentTypeList?.myAppResult?.Message)!, self)
                }
                
            }
            
        }
        
        
    }
    
    
   
    
    func addDocument(_ url : String, _ DocID : Int){
        
       
        var parms : [String:Any]
        if(self.DocumentUpdate != nil){
            let issueDate = self.txtIssueDate.text?.components(separatedBy: "-")
            let expiryDate = self.txtExpiryDate.text?.components(separatedBy: "-")
            
            
            parms =  ["Token":ApiUrls.AuthToken,
                      "AppID":"\(ApiUrls.AppID)",
                "ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                "DocBody":self.image1URL,
                "DocType":self.DocumentType[Docindex],
                "DocTypeName":self.DocumentTypeName[Docindex],
                "DocIssueDate":"\((issueDate?[2])!)-\((issueDate?[1])!)-\((issueDate?[0])!)",
                "DocExpireDate":"\((expiryDate?[2])!)-\((expiryDate?[1])!)-\((expiryDate?[0])!)",
                "DocNumber":self.txtDocumentNumber.text!,
                "DocTypeOther":self.txtDocumentDescription.text!
                ] as [String:Any]
            
            if( self.txtDocumentType.text == "Licence" || self.txtDocumentType.text == "ID-Card" || self.txtDocumentType.text == "Passport"){
                
                parms["IssuerCountryIsoCode"] = self.NationalityISOCode[nationalityIndex]
                
            }
            
            
            parms["DocID"] = "\((self.DocumentUpdate.DocID)!)"
            
            uc.webServicePosthttp(urlString: url, params:parms , message: "Loading...", currentController: self)
                   {result in
                       
                       
                       if(result == "fail")
                       {
                           self.addDocument(url, DocID)
                           return
                       }
                       
                       self.documentResponce = DocumentList(JSONString:result)
                       
                       if self.documentResponce?.myAppResult?.Code == 0 {
                           
//                           let alertCOntroller = UIAlertController(title: "", message: "You have successful updated Document", preferredStyle: UIAlertController.Style.alert)
//                           let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
//
//                               ApiUrls.TotalDoc = 1
//                               self.navigationController?.popViewController(animated: true)
//
//                           })
//                           alertCOntroller.addAction(action)
//                        self.present(alertCOntroller,animated: true,completion: {
//                            self.navigationController?.popViewController(animated: true)
//                            //self.dismiss(animated: true, completion: nil)
//                        })
//
                          self.dismiss(animated: true, completion: nil)
                       }else if self.documentResponce?.myAppResult?.Code == 101 {
                           
                           self.uc.logout(self)
                           
                       }else{
                           
                           if(self.documentResponce?.myAppResult?.Message == nil){
                               
                               self.uc.errorSuccessAler("Error", result, self)
                               
                           }else{
                               self.uc.errorSuccessAler("Error", (self.documentResponce?.myAppResult?.Message)!, self)
                           }
                       }
                   }
            
        }
        else
        {
            Toast.text = "Uploading Document"
            Toast.show()
            Toast.duration = 2
            var image1Data = (self.img1.image?.jpegData(compressionQuality: 0.7))!
            if(self.doc1FileDataType == "pdf")
            {
                image1Data = self.file1Data
            }
            
            let issueDate = self.txtIssueDate.text?.components(separatedBy: "-")
              let expiryDate = self.txtExpiryDate.text?.components(separatedBy: "-")
            Toast.text = "Uploading Document"
            Toast.show()
            Toast.duration = 2
              
              parms =  ["Token":ApiUrls.AuthToken,
                        "AppID":"\(ApiUrls.AppID)",
                  "ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                  "DocBody":self.image1URL,
                  "DocType":self.DocumentType[Docindex],
                  "DocTypeName":self.DocumentTypeName[Docindex],
                  "DocIssueDate":"\((issueDate?[2])!)-\((issueDate?[1])!)-\((issueDate?[0])!)",
                  "DocExpireDate":"\((expiryDate?[2])!)-\((expiryDate?[1])!)-\((expiryDate?[0])!)",
                  "DocNumber":self.txtDocumentNumber.text!,
                  "DocTypeOther":self.txtDocumentDescription.text!,
                  "ImageData":image1Data] as [String:Any]
            
            parms["Doc1FileDataType"] = self.doc1FileDataType
            parms["Doc2FileDataType"] = self.doc2FileDataType
              
              if( self.txtDocumentType.text == "Resident Card" || self.txtDocumentType.text == "National ID Card") || self.txtDocumentType.text == "Driving License"{
                
                if(self.doc2FileDataType == "pdf")
                {
                    parms["DocBodyBack"] = self.Image2URL
                    parms["ImageData2"] = self.file2Data
                }else{
                    parms["DocBodyBack"] = self.Image2URL
                    
                    //crash when one pic trying to uploading..
                    if self.img2.image != nil{
                        parms["ImageData2"] = self.img2.image!.jpegData(compressionQuality: 0.7)
                    }
                    
                }

//                  parms["DocBodyBack"] = self.Image2URL
//                  parms["ImageData2"] = self.img2.image?.jpegData(compressionQuality: 0.7) ?? ""
                  parms["IssuerCountryIsoCode"] = self.NationalityISOCode[nationalityIndex]
                  
              }
              if(self.txtDocumentType.text == "Passport"){
                  
                  parms["IssuerCountryIsoCode"] = self.NationalityISOCode[nationalityIndex]
                  
              }
            uc.webServicePosthttpUpload(urlString: url, params:parms , message: "Loading...", currentController: self)
                   {result in
                       
                       
                       if(result == "fail")
                       {
                           self.addDocument(url, DocID)
                           return
                       }
                       
                       self.documentResponce = DocumentList(JSONString:result)
                       
                       if self.documentResponce?.myAppResult?.Code == 0 {
                           
//                           let alertCOntroller = UIAlertController(title: "", message: "You have successful Added Document", preferredStyle: UIAlertController.Style.alert)
//                           let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
//
//                               ApiUrls.TotalDoc = 1
//                               self.navigationController?.popViewController(animated: true)
//
//                           })
//                           alertCOntroller.addAction(action)
//                           self.present(alertCOntroller,animated: true,completion: nil)
                        self.dismiss(animated: true, completion: nil)
                           
                       }else if self.documentResponce?.myAppResult?.Code == 101 {
                           
                           self.uc.logout(self)
                           
                       }else{
                           
                           if(self.documentResponce?.myAppResult?.Message == nil){
                               
                               self.uc.errorSuccessAler("Error", result, self)
                               
                           }else{
                               self.uc.errorSuccessAler("Error", (self.documentResponce?.myAppResult?.Message)!, self)
                           }
                       }
                   }
        }
        
        
    }
}

