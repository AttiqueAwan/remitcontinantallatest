//
//  AfterSIgnupDoLogin.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class AfterSIgnupDoLogin: UIViewController{

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtEmail: JVFloatLabeledTextField!
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    @IBOutlet weak var txtRetypePassword: JVFloatLabeledTextField!
    
    let txtFieldSetting = textFieldSetting()
    let round = RoundedCorner()
    
    //Manage api calling and alert
    let uc = UtilitySoftTechMedia()
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

    //getDashboard
      func GettingToken(){
          
         
          let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
          let parms =  ["AppID":ApiUrls.AppID,"Token":token,"DeviceType":"I","DeviceToken":firebaseToken,"Password":Password,"Email":Email]as [String : Any]
          
          
          uc.webServicePosthttp(urlString: ApiUrls.LoginUser, params:parms , message: "Loading...", currentController: self){result in
              
              if(result == "fail")
              {
                  self.GettingToken()
                  return
              }
              
              let loginResponse = AppUser(JSONString:result)
              
              if loginResponse != nil && loginResponse?.AuthToken != nil{
                  let NewKey = "\(loginResponse!.AuthToken!.auth_key!)\(loginResponse!.AuthToken!.user_id!)"
                  ApiUrls.AuthToken = NewKey.hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
                  
                  UserDefaults.standard.set(Email, forKey: "Email")
                  UserDefaults.standard.set(Password, forKey: "Password")
                  
                 self.uc.saveAuthToken(result: result)
                 self.performSegue(withIdentifier: "CreatedSucessfully", sender: nil)
                  
                  
                  
              }else{
                  
                  if(loginResponse == nil){
                      
                      self.uc.errorSuccessAler("Error", result, self)
                      
                  }else{
                      
                      self.uc.errorSuccessAler("Error", (loginResponse!.myAppResult?.Message)!, self)
                  }
              }
              
          }
          
      }
    
}
