//
//  step2ViewController.swift
//  TopConnect
//
//  Created by Softtech Media on 30/07/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class step2ViewController: UIViewController {
    
    var parms = [String:Any]()
    let uc = UtilitySoftTechMedia()
    
    @IBOutlet weak var txtemail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    @IBOutlet weak var txtReenterPassword: JVFloatLabeledTextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var reenterpassView: UIView!
    @IBOutlet weak var emailView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.04705882353, blue: 0.1490196078, alpha: 1)
        self.emailView.transform = CGAffineTransform(scaleX: 0,y: 0)
        self.passwordView.transform = CGAffineTransform(scaleX: 0,y: 0)
        self.reenterpassView.transform = CGAffineTransform(scaleX: 0,y: 0)
        animation()
    }
    func animation(){
             UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: .calculationModeLinear, animations: {
                self.emailView.transform = CGAffineTransform(scaleX: 1,y: 1)
                self.passwordView.transform = CGAffineTransform(scaleX: 1,y: 1)
                self.reenterpassView.transform = CGAffineTransform(scaleX: 1,y: 1)
                 
             }, completion:{finished in
                 
                 self.view.layer.removeAllAnimations()
             })
         }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func btnLogin(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "view") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if (self.txtemail!.text?.isEmpty)!{
            uc.errorSuccessAler("", "Please enter a valid Email", self)
        }else if (self.txtPassword!.text!.isEmpty){
            
            uc.errorSuccessAler("", "Password must have a length between 6 and 25", self)
        
        }else if (self.txtReenterPassword!.text!.isEmpty){
            
            uc.errorSuccessAler("", "Confirm password must have a length between 6 and 25", self)
        
        }
        else if (self.txtReenterPassword!.text! != self.txtPassword!.text!){
            
            uc.errorSuccessAler("", "Password and Confirm password must equal", self)
        
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "step3") as! SignUp3ViewController
            vc.parms = [
                "LastName" : self.parms["LastName"] as! String ,
                "FirstName" : self.parms["FirstName"] as! String,
               "Password"  : self.txtPassword.text!,
               "Email"    : self.txtemail.text!,
            ]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
       
    }
    
   
}
