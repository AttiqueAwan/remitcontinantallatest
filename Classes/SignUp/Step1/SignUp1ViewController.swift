//
//  SignUp1ViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import MaterialControls

var Name = ""
var LastName = ""
var Email = ""
var Password = ""
var PhoneNumber = ""

class SignUp1ViewController: UIViewController ,UITextFieldDelegate{
    
    var parms = [String:Any]()
    var Toast = MDToast()
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtfirstname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtlastname: SkyFloatingLabelTextField!
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    
    
    let txtFieldSetting = textFieldSetting()
    let round = RoundedCorner()
    
    let uc = UtilitySoftTechMedia()
    
    //Sending Values to Next View
    var SendValue = [[String: String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.04705882353, blue: 0.1490196078, alpha: 1)
        self.firstNameView.transform = CGAffineTransform(scaleX: 0,y: 0)
        self.lastNameView.transform = CGAffineTransform(scaleX: 0,y: 0)
        txtfirstname.delegate = self
        txtlastname.delegate = self
        DispatchQueue.main.async {
            
           
        }
        animation()
        // Do any additional setup after loading the view.
    }
    func animation(){
           UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: .calculationModeLinear, animations: {
               self.firstNameView.transform = CGAffineTransform(scaleX: 1,y: 1)
               self.lastNameView.transform = CGAffineTransform(scaleX: 1,y: 1)
               
           }, completion:{finished in
               
               self.view.layer.removeAllAnimations()
           })
       }
    @IBAction func btnNextClick(_ sender: Any) {
        
        //Checking textfield values not Null
        if (self.txtfirstname.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "first name required!", self)
            
        }else if ((self.txtfirstname?.text!.count)!<3){
            
            uc.errorSuccessAler("", "first name must have length between 3 and 100", self)
        
        }
        else if (self.txtlastname?.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "last name required!", self)
        
        }else if ((self.txtlastname?.text!.count)!<3){
            
            uc.errorSuccessAler("", "last name must have length between 3 and 100", self)
        
        }
        else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "step2") as! step2ViewController
            vc.parms = [
                "LastName" : self.txtlastname!.text!,
                "FirstName" : self.txtfirstname!.text!
            ]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: .letters) != nil || string == " " || string == ""
        {
            return true
        }else {
            Toast.text = "This character is not allowed"
            Toast.show()
            Toast.duration = 1
            return false
        }
    }
    


    
}
