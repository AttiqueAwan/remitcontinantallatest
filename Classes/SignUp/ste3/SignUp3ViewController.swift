//
//  SignUp3ViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import FRHyperLabel
import DropDown

class SignUp3ViewController: UIViewController,UITextFieldDelegate {
    var parms = [String:Any]()
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtCountry: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCode: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNo: SkyFloatingLabelTextField!
    @IBOutlet weak var countryView: ViewStyle!
    @IBOutlet weak var phoneNoView: UIView!
    
    
    let txtFieldSetting = textFieldSetting()
    let round = RoundedCorner()
    
    //Sending Values to Next View
    var SendApiValues = [[String: String]]()
    
    var CountryListingResponce : CountryListResult!
    
    //Manage api calling and alert
    let uc = UtilitySoftTechMedia()
    
    let CountryDropDown = DropDown()
    
    //saving api response
    var countryList = [String]()
    var DailingCode = [Int]()
    
    var selectValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.04705882353, blue: 0.1490196078, alpha: 1)
        self.countryView.transform = CGAffineTransform(scaleX: 0,y: 0)
        self.phoneNoView.transform = CGAffineTransform(scaleX: 0,y: 0)
        animation()
        //self.btnTermsAndCondition.numberOfLines = 0
        //Step 1: Define a normal attributed string for non-link text
        
        var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "By creating an account you agree with the Privacy Policy and Terms & Conditions of FacePace.")
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:43,length:14))
         
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:62,length:18))
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1) ,range: NSRange(location:83,length:9)) //length 11
        
        //self.btnTermsAndCondition.attributedText = myMutableString
        
        
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            if(substring == "Privacy Policy"){
                
                guard let url = URL(string: "https://www.remitcontinental.com/page.php?slug=privacy-policy") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            else if(substring == "Terms & Conditions"){
                
                guard let url = URL(string: "https://www.remitcontinental.com/page.php?slug=terms-and-conditions") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            
        }
        
        //Step 3: Add link substrings
        //self.btnTermsAndCondition.setLinksForSubstrings(["Terms & Conditions","Privacy Policy"], withLinkHandler: handler)
        
        DispatchQueue.main.async {
            
            self.txtPhoneNo.delegate = self
            self.getCountryList()
        }
        
        
        
        //setting User Type dropdown
        CountryDropDown.anchorView = self.btnCountry
        CountryDropDown.dataSource = countryList
        
        
        CountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCountry.text = item
            self.txtCode.text = "+\(self.DailingCode[index])"
            self.selectValue = index
        }
        animation()
        // Do any additional setup after loading the view.
    }
    
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
   func animation(){
             UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: .calculationModeLinear, animations: {
                 self.countryView.transform = CGAffineTransform(scaleX: 1,y: 1)
                 self.phoneNoView.transform = CGAffineTransform(scaleX: 1,y: 1)
                 
             }, completion:{finished in
                 
                 self.view.layer.removeAllAnimations()
             })
         }
    
    
    @IBAction func btnNextClick(_ sender: Any) {
        
        
        if(self.txtCountry.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please select country", self)
            
        }else if(self.txtPhoneNo.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Mobile number required!", self)
            
        }else if !(uc.validatePhone(value: "\((self.txtCode.text)!)\((self.txtPhoneNo.text)!)")){
            
            uc.errorSuccessAler("", "Invalid mobile number", self)
            
        }else{
            
            self.view.endEditing(true);
            self.txtPhoneNo.endEditing(true)
        
            self.SendApiValues.append(["Country":self.txtCountry.text!.trimmingCharacters(in: .whitespacesAndNewlines)])
            self.SendApiValues.append(["CountryCode":self.txtCode.text!.trimmingCharacters(in: .whitespacesAndNewlines)])
            self.SendApiValues.append(["Phone":self.txtPhoneNo.text!.trimmingCharacters(in: .whitespacesAndNewlines)])
            
            self.RegisterUser()
            
            
        }
        
    }
    
    
    @IBAction func btnCountryClick(_ sender: Any) {
        
        self.CountryDropDown.show()
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
        
    
    func getCountryList(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"1"]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getCountryList()
                return
            }
            
            self.CountryListingResponce = CountryListResult(JSONString:result)
            
            if self.CountryListingResponce?.myAppResult?.Code == 0 {
                
                
                for i in 0 ..< (self.CountryListingResponce?.AceCountryList!.count)! {
                    
                    self.countryList.append((self.CountryListingResponce?.AceCountryList![i].CountryName)!)
                    self.DailingCode.append((self.CountryListingResponce.AceCountryList![i].DialingCode)!)
                    if((self.CountryListingResponce?.AceCountryList![i].CountryName)! == "United Kingdom"){
                        
                        self.selectValue = i
                        self.txtCountry.text = self.CountryListingResponce?.AceCountryList![i].CountryName
                        self.txtCode.text = "+\((self.CountryListingResponce?.AceCountryList![i].DialingCode)!)"
                        
                    }
                    print (i) //i will increment up one with each iteration of the for loop
                    
                }
                self.CountryDropDown.dataSource = self.countryList
                self.CountryDropDown.reloadAllComponents()
                
                
            }else if self.CountryListingResponce?.myAppResult?.Code == 101 {
                
                
                
            }else{
                
                if(self.CountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.CountryListingResponce?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(self.txtPhoneNo == textField){
            
            let ACCEPTABLE_CHARACTERS = " +0123456789"
            let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            var filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
            
            
            if(range.location >= 11){
                
                return range.location < 12
            }
            
            filtered = filtered.uppercased()
            
            
            return (string == filtered)
            
        }
       return range.location < 12
    }

}
