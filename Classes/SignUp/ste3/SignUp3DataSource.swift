//
//  SignUp3DataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 24/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension SignUp3ViewController{
    
    //getDashboard
    func RegisterUser(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"DeviceType":"I","DeviceToken":firebaseToken,
                      "Password":self.parms["Password"] as! String ,
                      "FirstName":self.parms["FirstName"] as! String ,
                      "LastName":self.parms["LastName"] as! String ,
                      "MiddleName":"",
                      "Email":self.parms["Email"] as! String ,
                      "Phone":"\((self.txtCode.text)!)\((self.txtPhoneNo.text)!)",
            "CountryIsoCode":self.CountryListingResponce.AceCountryList![selectValue].Iso3Code!.trimmingCharacters(in: .whitespacesAndNewlines)] as [String : Any]
        print(parms)
        
        
        uc.webServicePosthttp(urlString: ApiUrls.RegisterUser, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.RegisterUser()
                return
            }
            
            let RegisterResponse = AppUser(JSONString:result)
            
            if RegisterResponse?.myAppResult?.Code == 0 {
                
                PhoneNumber = "\((self.txtCode.text)!)\((self.txtPhoneNo.text)!)".trimmingCharacters(in: .whitespacesAndNewlines)
                UserDefaults.standard.set(Email, forKey: "Email")
                UserDefaults.standard.set(Password, forKey: "Password")
                
                UserDefaults.standard.set(result, forKey: "UserLogin")
                
              //  self.performSegue(withIdentifier: "OTP", sender: nil)
                self.GettingToken()
                
            }else if RegisterResponse?.myAppResult?.Code == 101 {
                
                
                
            }else{
                
                if(RegisterResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (RegisterResponse?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    
    
    //getDashboardDetail
       func GettingToken(){
           
          
           let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
           let parms =  ["AppID":ApiUrls.AppID,"Token":token,"DeviceType":"I","DeviceToken":firebaseToken,"Password":self.parms["Password"] as! String,"Email":self.parms["Email"] as! String]as [String : Any]
           
           
           uc.webServicePosthttp(urlString: ApiUrls.LoginUser, params:parms , message: "Loading...", currentController: self){result in
               
               if(result == "fail")
               {
                   self.GettingToken()
                   return
               }
               
               let loginResponse = AppUser(JSONString:result)
               
               if loginResponse != nil && loginResponse?.AuthToken != nil{
                   let NewKey = "\(loginResponse!.AuthToken!.auth_key!)\(loginResponse!.AuthToken!.user_id!)"
                   ApiUrls.AuthToken = NewKey.hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
                   
                   UserDefaults.standard.set(Email, forKey: "Email")
                   UserDefaults.standard.set(Password, forKey: "Password")
                   
                  self.uc.saveAuthToken(result: result)
                  self.performSegue(withIdentifier: "Confirm2", sender: nil)
                   
                   
                   
               }else{
                   
                   if(loginResponse == nil){
                       
                       self.uc.errorSuccessAler("Error", result, self)
                       
                   }else{
                       
                       self.uc.errorSuccessAler("Error", (loginResponse!.myAppResult?.Message)!, self)
                   }
               }
               
           }
           
       }
    
}
