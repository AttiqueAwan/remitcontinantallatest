//
//  VerifyOTP.swift
//  TopConnect
//
//  Created by Mac on 11/10/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class VerifyOTP: UIViewController {

    
    @IBOutlet weak var txtOTP: SkyFloatingLabelTextField!
    @IBOutlet weak var LblPhoneNumber: UILabel!
    
    //Manage api calling and alert
   
    let uc = UtilitySoftTechMedia()
    var OTPRes : OTPResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.LblPhoneNumber.text = Email
        SendOTP()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func BackBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ResendOTPCLicked(_ sender: UIButton) {
        SendOTP()
    }
    
    @IBAction func VerifyNowClicked(_ sender: UIButtonStyle) {
        if(self.txtOTP.text!.isEmpty)
        {
            self.uc.errorSuccessAler("Alert", "Please enter OTP which you have received at your email or phone number.", self)
        }
        else if(self.OTPRes!.OTPResp!.OTPToken! == self.txtOTP.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            self.VerifyOTP()
        }
        else
        {
            self.uc.errorSuccessAler("Alert", "You have entered wrong OTP.", self)
        }
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VerifyOTP
{
    func SendOTP(){
              
              let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
              let parms =  ["AppID":ApiUrls.AppID,"Token":token,"DeliveryType":"E","PhoneNumber":PhoneNumber,"OTPType":3,"Email":Email]as [String : Any]
              
              
              uc.webServicePosthttp(urlString: ApiUrls.SendOTP, params:parms , message: "Loading...", currentController: self){result in
                  
                  if(result == "fail")
                  {
                      self.SendOTP()
                      return
                  }
                  
                self.OTPRes = OTPResponse(JSONString:result)
                                
                if self.OTPRes != nil && self.OTPRes.myAppResult?.Code == 0{
                    
                    self.uc.errorSuccessAler("OTP Alert", "Check your email.\nOTP has been sent on your given email address.", self)
                      
                      
                  }else{
                      
                    if(self.OTPRes.myAppResult?.Message == nil){
                          
                          self.uc.errorSuccessAler("Error", result, self)
                          
                      }else{
                          
                          self.uc.errorSuccessAler("Error", (self.OTPRes!.myAppResult?.Message)!, self)
                      }
                  }
                  
              }
              
          }
    
         func VerifyOTP(){
              
             
             let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
            let parms =  ["AppID":ApiUrls.AppID,"Token":token,"PhoneNumber":PhoneNumber,"OTPType":3,"Email":Email,"OTP":self.txtOTP.text!.trimmingCharacters(in: .whitespacesAndNewlines)]as [String : Any]
              
              uc.webServicePosthttp(urlString: ApiUrls.VerifyOTP, params:parms , message: "Loading...", currentController: self){result in
                  
                  if(result == "fail")
                  {
                      self.VerifyOTP()
                      return
                  }
                  
                   let loginResponse = AppUser(JSONString:result)
                                  
                if loginResponse != nil && loginResponse?.myAppResult?.Code == 0{
                    
                    self.GettingToken()
                    
                  }else{
                      
                      if(loginResponse == nil){
                          
                          self.uc.errorSuccessAler("Error", result, self)
                          
                      }else{
                          
                          self.uc.errorSuccessAler("Error", (loginResponse!.myAppResult?.Message)!, self)
                      }
                  }
                  
              }
              
          }
    
       //getDashboardDetail
       func GettingToken(){
           
          
           let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
           let parms =  ["AppID":ApiUrls.AppID,"Token":token,"DeviceType":"I","DeviceToken":firebaseToken,"Password":Password,"Email":Email]as [String : Any]
           
           
           uc.webServicePosthttp(urlString: ApiUrls.LoginUser, params:parms , message: "Loading...", currentController: self){result in
               
               if(result == "fail")
               {
                   self.GettingToken()
                   return
               }
               
               let loginResponse = AppUser(JSONString:result)
               
               if loginResponse != nil && loginResponse?.AuthToken != nil{
                   let NewKey = "\(loginResponse!.AuthToken!.auth_key!)\(loginResponse!.AuthToken!.user_id!)"
                   ApiUrls.AuthToken = NewKey.hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
                   
                   UserDefaults.standard.set(Email, forKey: "Email")
                   UserDefaults.standard.set(Password, forKey: "Password")
                   
                  self.uc.saveAuthToken(result: result)
                  self.performSegue(withIdentifier: "Confirm", sender: nil)
                   
                   
                   
               }else{
                   
                   if(loginResponse == nil){
                       
                       self.uc.errorSuccessAler("Error", result, self)
                       
                   }else{
                       
                       self.uc.errorSuccessAler("Error", (loginResponse!.myAppResult?.Message)!, self)
                   }
               }
               
           }
           
       }
}
