//
//  SideMenuViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 21/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import MaterialControls


class SideMenuViewController: UIViewController  , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
   
    
    
    var ShowNewLbl = false
    let uc = UtilitySoftTechMedia()
    var image1URL = ""
    var documentResponce : DocumentList!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        
        animation()
        
        // Do any additional setup after loading the view.
         
    }
    func animation(){
        UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: .calculationModeLinear, animations: {
            //self.emailView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            //self.lblUserName.transform = CGAffineTransform(scaleX: 1,y: 1)
            //self.lblCustomerID.transform = CGAffineTransform(scaleX: 1,y: 1)
            //self.passwordView.shake(count: 50, for: 1.5, withTranslation: 5)
            //self.emailView.frame.size.width += 360
            self.lblEmail.center.x -= self.view.bounds.width
            //self.emailView.center.y -= self.view.bounds.height
            self.lblUserName.center.x = self.view.bounds.width
            //self.passwordView.center.y -= self.view.bounds.height
            
        }, completion:{finished in
            self.view.layer.removeAllAnimations()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ShowNewLbl = false
        NotificationCenter.default.addObserver(self, selector: #selector(Reload(_:)), name: ApiUrls.ComplaintNotification, object: nil)
        
        let userInfo = uc.getUserInfo()
        if(userInfo != nil)
        {
            
            var clientId = "RC"
            
            let userCountryISO3 = userInfo?.UserInfo?.CountryIsoCode
            
            if(userCountryISO3 != nil && userCountryISO3 != "")
            {
                let country = uc.getCountryByISO3(userCountryISO3!)
                
                if(country.Iso2Code != nil )
                {
                    if(country.Iso2Code == "GB")
                    {
                        clientId += "UK"
                    }else{
                        clientId += country.Iso2Code!
                    }
                }
            }
            var customerId = "\(userInfo?.UserInfo!.CustomerID! ?? 0)"
            
            
            if(customerId.count == 2 )
            {
                customerId = "00\(customerId)"
            }else if(customerId.count == 3 )
            {
                customerId = "0\(customerId)"
            }
            
            let ID = customerId
            
            
            self.lblUserName.text = userInfo?.UserInfo?.FullName
            self.lblEmail.text = "Client id : \(ID)"
            if let UserImage = userInfo?.UserInfo?.Photo{
                
                self.imgUser.sd_setImage(with: URL(string: UserImage), placeholderImage: UIImage(named: "my-profile.png"))
                self.imgUser.layer.cornerRadius = self.imgUser.layer.frame.size.height/2
                self.imgUser.layer.masksToBounds = true
                
            }
        }
        
        
        
    }
    
    @IBAction func btnPersonalinfo(_ sender: Any) {
            self.performSegue(withIdentifier: "Profile", sender: nil)
    }
    @IBAction func btnChangePass(_ sender: Any) {
           self.performSegue(withIdentifier: "ChangePassword", sender: nil)
    }
    @IBAction func btnPolicy(_ sender: Any) {
           guard let url = URL(string: "https://www.remitcontinental.com/page.php?slug=privacy-policy") else {
                          return //be safe
           }
                      
          if #available(iOS 10.0, *) {
              UIApplication.shared.open(url, options: [:], completionHandler: nil)
          } else {
              UIApplication.shared.openURL(url)
          }
    }
    @IBAction func btnAboutUs(_ sender: Any) {
            guard let url = URL(string: "https://www.remitcontinental.com/index.php") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
    }
    @IBAction func btnContactUs(_ sender: Any) {
            guard let url = URL(string: "https://www.remitcontinental.com/contact.php") else {
                          return //be safe
                      }

                      if #available(iOS 10.0, *) {
                          UIApplication.shared.open(url, options: [:], completionHandler: nil)
                      } else {
                          UIApplication.shared.openURL(url)
                      }
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: nil)
    }
    
    @objc  func Reload(_ notification:NSNotification) {
        ShowNewLbl = true
       
    }
    
    @IBAction func LogoutBtn(_ sender: UIButtonStyle) {
         self.modalPresentationStyle = .fullScreen
         self.performSegue(withIdentifier: "Logout", sender: nil)
    }
    @IBAction func UploadProfileImage(_ sender: UIButton) {
        
        
     let alert = UIAlertController(title: "Choose profile picture", message: nil, preferredStyle: .actionSheet)
                         alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                             self.openCamera()
                         }))
                         
                         alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                             self.openGallery()
                         }))
                     
                         alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: nil)

          }
          
          func openCamera()
             {
                 if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                     let imagePicker = UIImagePickerController()
                     imagePicker.delegate = self
                     imagePicker.sourceType = UIImagePickerController.SourceType.camera
                     imagePicker.allowsEditing = false
                     self.present(imagePicker, animated: true, completion: nil)
                 }
                 else
                 {
                     let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                     alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                     self.present(alert, animated: true, completion: nil)
                 }
             }
             
             func openGallery()
             {
                 if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                     let imagePicker = UIImagePickerController()
                     imagePicker.delegate = self
                     imagePicker.allowsEditing = true
                     imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                     self.present(imagePicker, animated: true, completion: nil)
                 }
                 else
                 {
                     let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
                     alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                     self.present(alert, animated: true, completion: nil)
                 }
             }
             
             func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
              if (info[.originalImage] as? UIImage) != nil {
                      let image = info[.originalImage] as? UIImage
                          
                          if(picker.sourceType == .camera){
                              
                              let imageURL = self.saveImage(imageName: "\(arc4random()).png", image!)
                              self.image1URL = imageURL
                              
                          }else{
                              
                              if #available(iOS 11.0, *) {
                                  let imageURL = info[UIImagePickerController.InfoKey.imageURL] as! URL
                                  self.image1URL =  imageURL.absoluteString
                              } else {
                                  // Fallback on earlier versions
                              }
                              
                          }
                          
                  

                          self.imgUser.image = image
                          self.UpdateProfilePicture(ApiUrls.UpdateProfilePicture)
                         
                 }
                 picker.dismiss(animated: true, completion: nil)
             }
             
             func convertToBase64(image: UIImage) -> String {
                 return image.pngData()!.base64EncodedString()
             }
          
          func saveImage(imageName: String,_ imageselected:UIImage)-> String{
                 //create an instance of the FileManager
                 let fileManager = FileManager.default
                 //get the image path
                 let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
                 //get the image we took with camera
                 let image = imageselected
                 //get the PNG data for this image
                 let data = image.pngData()
                 //store it in the document directory
                 fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
                 return imagePath
                 
             }
    
    func UpdateProfilePicture(_ url : String){
      
          let parms =  ["Token":ApiUrls.AuthToken,"AppID":"\(ApiUrls.AppID)",
                        "ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                        "PhotoBody":self.image1URL,
                        "ImageData":(self.imgUser.image?.jpegData(compressionQuality: 0.7))!] as [String:Any]
          
          uc.webServicePosthttpUploadPhoto(urlString: url, params:parms , message: "Loading...", currentController: self){result in
              
              if(result == "fail")
              {
                self.UpdateProfilePicture(url)
                  return
              }
              
              self.documentResponce = DocumentList(JSONString:result)
              
              if self.documentResponce?.myAppResult?.Code == 0 {
                  
                  let toast = MDToast()
                toast.text = "Uploaded successfully"
                toast.duration = 2
                toast.show()
                  
              }else if self.documentResponce?.myAppResult?.Code == 101 {
                  
                  self.uc.logout(self)
                  
              }else{
                  
                  if(self.documentResponce?.myAppResult?.Message == nil){
                      
                      self.uc.errorSuccessAler("Error", result, self)
                      
                  }else{
                      self.uc.errorSuccessAler("Error", (self.documentResponce?.myAppResult?.Message)!, self)
                  }
              }
          }
          
      }
    
}

extension UIApplication {
    static var appVersion: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
}


