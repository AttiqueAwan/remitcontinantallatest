//
//  SideMenuCell.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 21/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var NewLbl: UILableX!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblMenuName: UILabel!
    
    @IBOutlet weak var imgVArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
