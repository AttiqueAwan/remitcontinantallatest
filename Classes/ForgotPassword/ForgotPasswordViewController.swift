//
//  ForgotPasswordViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ForgotPasswordViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var txtEmail: JVFloatLabeledTextField!
    @IBOutlet weak var txtOTP: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNewPswd: SkyFloatingLabelTextField!
    @IBOutlet weak var OTPView: UIView!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    let txtFieldSetting = textFieldSetting()
    let round = RoundedCorner()
    let uc = UtilitySoftTechMedia()
    var OTPRes : OTPResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.OTPView.isHidden = true
        DispatchQueue.main.async {
            UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
            self.btnForgotPassword = self.round.RoundCorner(self.btnForgotPassword)
            self.txtEmail = self.txtFieldSetting.textFieldBaseLine(self.txtEmail, #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
            
        }
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func btnForgotPasswordClick(_ sender: Any) {
        
        if(self.txtEmail.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please enter email", self)
            
        }else if !(uc.isValidEmail(testStr: self.txtEmail.text!)){
            
            uc.errorSuccessAler("", "Please enter valid emai", self)
        }else{
            Email = self.txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            self.SendOTP()
         
        }
        
    }
    @IBAction func Submit(_ sender: UIButton) {
        
        if(self.txtOTP.text?.isEmpty)!{
            
            uc.errorSuccessAler("", "Please enter OTP", self)
            
        }else if(self.txtNewPswd.text?.isEmpty)! {
            
            uc.errorSuccessAler("", "Please enter New Password", self)
        }else if(self.txtNewPswd.text!.count < 7) {
            
            uc.errorSuccessAler("", "Minimum 8 characters required for New Password", self)
        }else{
    
            self.VerifyOTP()
            
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func myHandler(alert: UIAlertAction){
        self.OTPView.isHidden = true
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func SendOTP(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"DeliveryType":"E","OTPType":4,"Email":Email]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.SendOTP, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.SendOTP()
                return
            }
            
            self.OTPRes = OTPResponse(JSONString:result)
            
            if self.OTPRes != nil && self.OTPRes.myAppResult?.Code == 0{
                
                self.uc.errorSuccessAler("OTP Alert", "Check your email.\nOTP has been sent on your \(Email) email address.", self)
                
                self.OTPView.isHidden = false
            }else{
                
                if(self.OTPRes.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (self.OTPRes!.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    
    func VerifyOTP(){
        
       
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token, "OTPType":4,"Email":Email,"OTP":self.txtOTP.text!.trimmingCharacters(in: .whitespacesAndNewlines),"NewPassword":self.txtNewPswd.text!.trimmingCharacters(in: .whitespacesAndNewlines)]as [String : Any]
        
        uc.webServicePosthttp(urlString: ApiUrls.VerifyOTP, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.VerifyOTP()
                return
            }
            
            let loginResponse = AppUser(JSONString:result)
            
            if loginResponse != nil && loginResponse?.myAppResult?.Code == 0{
                
                let alertController = UIAlertController(title: "", message: "Dear customer your password has been updated successfully.", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler: self.myHandler)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }else{
                
                if(loginResponse == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (loginResponse!.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
    
}
