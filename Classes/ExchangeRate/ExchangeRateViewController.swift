//
//  ExchangeRateViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 31/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown
import JVFloatLabeledTextField


class ExchangeRateViewController: UIViewController {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblExchangeRate: UITableView!
    @IBOutlet weak var lblLastUpdate: UILabel!
    
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var txtCountry: JVFloatLabeledTextField!
    @IBOutlet weak var imgdropDown: UIImageView!
    @IBOutlet weak var countryView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    let uc = UtilitySoftTechMedia()
    
    var ExchangeRateResponce:ExchangeRate!
    var CountryListingResponce : CountryListResult!
    var Country = ""
    var login = false
    var selectValue = 0
    
    let CountryDropDown = DropDown()
    let round = RoundedCorner()
    
    //saving api response
    var countryList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        
        imgdropDown.image = imgdropDown.image!.withRenderingMode(.alwaysTemplate)
        imgdropDown.tintColor = UIColor.darkGray
        
      //  self.countryView = round.RoundViews(self.countryView, #colorLiteral(red: 0, green: 0.3803921569, blue: 0.6431372549, alpha: 1))
        
        
        self.getCountryList()
        
        //setting User Type dropdown
        CountryDropDown.anchorView = self.btnCountry
        CountryDropDown.dataSource = countryList
        
        
        CountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCountry.text = item
            self.imgLogo.image = UIImage(named: "\(item).png")
           // self.changeCOuntryImage(name: item)
            self.selectValue = index
            self.GetExchangeRate()
            
        }
        
        
        
        if(login == true){
            
            self.btnBack.isHidden = false
            
            let userinfo = uc.getUserInfo()
            
            let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
            
            let AllCountries = CountryListResult(JSONString: result)
            
            
            for i in 0..<(AllCountries?.AceCountryList?.count)! {
                
                if(AllCountries?.AceCountryList![i].Iso3Code == userinfo?.UserInfo?.CountryIsoCode){
                    
                    
                    
                    if let Country = AllCountries?.AceCountryList![i].Iso3Code {
                        
                        self.Country = Country
                        //self.GetExchangeRate()
                        break
                        
                    }
                }
            }
            
        }else{
            
            let englishLocale : NSLocale = NSLocale.init(localeIdentifier :  "en_US")
            
            // get the current locale
            let currentLocale = NSLocale.current
            
            let theEnglishName : String? = englishLocale.displayName(forKey: NSLocale.Key.identifier, value: currentLocale.identifier)
            if let theEnglishName = theEnglishName
            {
                let index = theEnglishName.firstIndex(of: "(")!
                
                let newStr = String(theEnglishName[index...])
                var countryName = newStr.replacingOccurrences(of: "(", with: "")
                countryName = countryName.replacingOccurrences(of: ")", with: "")
                self.Country = countryName
                print("the localized country name is \(countryName)")
            }
            
            
            let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
            
            let AllCountries = CountryListResult(JSONString: result)
            
            
            for i in 0..<(AllCountries?.AceCountryList?.count)! {
                
                if((AllCountries?.AceCountryList![i].CountryName!.uppercased().contains(Country.uppercased()))! ){
                    
                    
                    
                    if let Country = AllCountries?.AceCountryList![i].Iso3Code {
                        
                        self.Country = Country
                        //self.GetExchangeRate()
                        break
                        
                    }
                }
            }
        }
    }
    
    func changeCOuntryImage(name : String )
    {
        txtCountry.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "\(name).png")
        imageView.image = image
        txtCountry.leftView = imageView
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
//        NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: nil)
    }
    
    @IBAction func btnRegisterClick(_ sender: Any) {
        
        self.performSegue(withIdentifier: "Register", sender: nil)
    }
    @IBAction func btnLoginClick(_ sender: Any) {
        
        self.performSegue(withIdentifier: "Login", sender: nil)
    }
    
    
    @IBAction func btnCountryClick(_ sender: Any) {
        
        self.CountryDropDown.show()
    }
    
    
    func GetExchangeRate(){
        
        if(self.Country == "GBR"){
            
        }else{
            
            self.Country = "GBR"
        }
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Payers":"1","SendingCountryIso3Code":"GBR","ReceivingCountryIso3Code":(self.CountryListingResponce.AceCountryList![selectValue].Iso3Code)!] as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetPublicExchangeRate, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetExchangeRate()
                return
            }
            
            self.ExchangeRateResponce = ExchangeRate(JSONString:result)
            
            
            if self.ExchangeRateResponce?.myAppResult?.Code == 0 {
                
                if(self.ExchangeRateResponce.AceExchangeRateResponce != nil){
                    
                    let creationdate = self.ExchangeRateResponce.AceExchangeRateResponce![0].CreationDate?.components(separatedBy: "-")
                    
                    if((creationdate?.count)! > 0){
                        
//                         self.lblLastUpdate.text = "Last Updated: \(creationdate![2])-\(creationdate![1])-\(creationdate![0])"
                    }
                    
//                    let date = Date()
//                    let format = DateFormatter()
////                    format.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                    format.dateFormat = "dd-MM-yyyy HH:mm"
//
//                    let formattedDate = format.string(from: date)
//                    print(formattedDate)
                    let date = Date()
                    let dateFormatter = DateFormatter()

                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                    dateFormatter.dateStyle = .medium
                    dateFormatter.timeStyle = .short
                    let dateString = dateFormatter.string(from: date)
                    self.lblLastUpdate.text = "Last Updated:\(dateString) "
                    self.tblExchangeRate.reloadData()
                    
                }else{
                    self.uc.errorSuccessAler("", "Exchange rate not availabe", self)
                }
                
            }else if self.ExchangeRateResponce?.myAppResult?.Code == 101 {
                
                
                self.uc.logout(self)
                
                
            }else{
                
                if(self.ExchangeRateResponce.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", "Some thing went wrong", self)
                    
                }else{
                    self.uc.errorSuccessAler("", (self.ExchangeRateResponce.myAppResult?.Message)!, self)
                }
            }
            
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
