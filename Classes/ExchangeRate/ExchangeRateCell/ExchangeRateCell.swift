//
//  ExchangeRateCell.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 31/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class ExchangeRateCell: UITableViewCell {

    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblExchangeRate: UILabel!
    @IBOutlet weak var imgCountry:UIImageView!
    
    @IBOutlet weak var exchangeview:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
