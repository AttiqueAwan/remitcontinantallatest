//
//  ExchangeRateDataSource.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 31/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation
import UIKit

extension ExchangeRateViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.ExchangeRateResponce == nil){
            
            return 0
        }
        if(self.ExchangeRateResponce.AceExchangeRateResponce?.count != 0){
            
            return (self.ExchangeRateResponce.AceExchangeRateResponce?.count)!
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = self.tblExchangeRate.dequeueReusableCell(withIdentifier: "ExchangeRate") as? ExchangeRateCell
        
        let details  = self.ExchangeRateResponce.AceExchangeRateResponce![indexPath.row]
        
        cell?.lblBankName.text = details.PayerName
        cell?.lblExchangeRate.text = "\((details.ExchangeRate)!) \((details.RecievingCurrencyyISOCode)!)"
        cell?.imgCountry.image = UIImage(named: "location_b.png")
        
      //  cell?.exchangeview = round.RoundViews((cell?.exchangeview)!, #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        cell?.exchangeview.layer.cornerRadius = 5
        cell?.exchangeview.layer.masksToBounds = true
        return cell!
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            
            let details  = self.ExchangeRateResponce.AceExchangeRateResponce![indexPath.row]
            
            let attributeString = NSMutableAttributedString(string: "\n\nExchange Rate is: \((details.ExchangeRate)!) \((details.RecievingCurrencyyISOCode)!)")
        
            let paragraphStyle = NSMutableParagraphStyle.init()
            paragraphStyle.alignment = .center
            
            attributeString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attributeString.length))
            
            
            let font = UIFont.systemFont(ofSize: 14)
            let attributes = [NSAttributedString.Key.font: font]
            let attributeString2 = NSMutableAttributedString(string: "\((details.PayerName)!)", attributes: attributes)
            
            attributeString2.append(attributeString)
            
            
            let alert = UIAlertController(title: "Exchange Rate", message: "Select course", preferredStyle: UIAlertController.Style.alert)
            
            alert.setValue(attributeString2, forKey: "attributedMessage")
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                
                            }))
            self.present(alert, animated: true, completion: nil)
            
           
        }
       
    }
    
    
    
    func getCountryList(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"2"]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getCountryList()
                return
            }
            
            self.CountryListingResponce = CountryListResult(JSONString:result)
            
            if self.CountryListingResponce?.myAppResult?.Code == 0 {
                
                
                for i in 0 ..< (self.CountryListingResponce?.AceCountryList!.count)! {
                    
                    self.countryList.append((self.CountryListingResponce?.AceCountryList![i].CountryName)!)

                    
                }
                self.CountryDropDown.dataSource = self.countryList
                self.CountryDropDown.reloadAllComponents()
                self.selectValue = 0
                self.txtCountry.text = self.CountryListingResponce.AceCountryList![self.selectValue].CountryName
                // self.changeCOuntryImage(name: self.txtCountry.text!)
                self.imgLogo.image = UIImage(named: "\(self.txtCountry.text!).png")
                DispatchQueue.main.async {
                    self.GetExchangeRate()
                }
               
             
                
                
            }else if self.CountryListingResponce?.myAppResult?.Code == 101 {
                
                
                
            }else{
                
                if(self.CountryListingResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.CountryListingResponce?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
        
    }
}

