//
//  SliderViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 24/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class SliderViewController: UIViewController {

    @IBOutlet weak var btnView: UIView!
    
    @IBOutlet weak var sliderview: UIView!
    
    @IBOutlet weak var btnExchangeRate:UIButton!
    
    // The UIPageViewController
    var pageContainer: UIPageViewController!
    
    // The pages it contains
    var pages = [UIViewController]()
    
    // Track the current index
    var currentIndex: Int?
    private var pendingIndex: Int?
    
    var uc = UtilitySoftTechMedia()
    
    let rounderCorner = RoundedCorner()
    
    var tTime: Timer!

    
    @IBOutlet weak var signIn: UIButton!
    @IBOutlet weak var signUp: UIButton!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.1960870624, green: 0.2391059399, blue: 0.4983942509, alpha: 1)
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
       // AllCountryList()
        setNeedsStatusBarAppearanceUpdate()
        if (UserDefaults.standard.value(forKey: "AllCountriesList") as? String) != nil {
            
        }else{
            
            self.AllCountryList()
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let page1: UIViewController! = storyboard.instantiateViewController(withIdentifier: "Page1")
        let page2: UIViewController! = storyboard.instantiateViewController(withIdentifier: "Page2")
        let page3: UIViewController! = storyboard.instantiateViewController(withIdentifier: "Page3")
       
        pages.append(page1)
        pages.append(page2)
        pages.append(page3)
       
        
        // Create the page container
        pageContainer = UIPageViewController()
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([page1], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        currentIndex = 0
        
        
        self.view.addSubview(pageContainer.view)
        self.view.bringSubviewToFront(self.btnView)
        
        tTime = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(changeSlide), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnExchangeRateClick(_ sender:Any){
        
        
        self.performSegue(withIdentifier: "ExchangeRate", sender: nil)
        
    }
    @objc func changeSlide() {
        
        if currentIndex == pages.count {
            currentIndex = 0
        }
        
        pageContainer.setViewControllers([pages[currentIndex!]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        currentIndex =  currentIndex! + 1
    }
    
    func viewController(at index: Int) -> UIViewController? {
        let childViewController = storyboard?.instantiateViewController(withIdentifier: "Page\(Int(UInt(index)) + 1)")
        //childViewController?.index = index
        if index == 0 {
            view.backgroundColor = UIColor(red: 0.000 / 255.000, green: 123.000 / 255.000, blue: 203.000 / 255.000, alpha: 1.0)
        } else if index == 1 {
            
        } else {
            view.backgroundColor = UIColor(red: 41.000 / 255.000, green: 61.000 / 255.000, blue: 80.000 / 255.000, alpha: 1.0)
        }
        return childViewController
    }
    @IBAction func btnSignInCick(_ sender: Any) {
        
        self.performSegue(withIdentifier: "SignInSegue", sender: nil)
    }
    
    @IBAction func btnSignUpClick(_ sender: Any) {
        
        self.performSegue(withIdentifier: "SignUpSegue", sender: nil)
    }
    
    
    
    func AllCountryList(){
    
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":""]as [String : Any]
        
        print(token)
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.AllCountryList()
                return
            }
            
            let AllCountiresResponse = AppUser(JSONString:result)
            print("country list start \n "+result+" end \n")
           // let AllCountiresResponse = CountryListResult(JSONString:result)
            
            if AllCountiresResponse?.myAppResult?.Code == 0 {
                
                
                UserDefaults.standard.set(result, forKey: "AllCountriesList")
                
                
            }else if AllCountiresResponse?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(AllCountiresResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", "Some Thing Went Wrong", self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (AllCountiresResponse?.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    
}

extension SliderViewController:UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    
    // MARK: - UIPageViewController delegates
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of:viewController )
        if currentIndex == 0 {
            return  pages[2]
            // return nil
        }
        let previousIndex = abs((currentIndex! - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of:viewController )
        if currentIndex == pages.count-1 {
            //return nil
            return pages[0]
        }
        let nextIndex = abs((currentIndex! + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.firstIndex(of:pendingViewControllers.first!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentIndex = pendingIndex
        }
    }
    
}


//extension UIApplication {
//    
//    var statusBarView: UIView? {
//        return value(forKey: "statusBar") as? UIView
//    }
//    
//}

extension UIApplication {
    var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 38482458385
            if let statusBar = self.keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = tag

                self.keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
}
