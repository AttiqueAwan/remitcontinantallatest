//
//  PaymentViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import SVProgressHUD
import WebKit
import MaterialControls

class PaymentViewController: UIViewController, WKNavigationDelegate {
    var Toast = MDToast()
    
    
    @IBOutlet weak var PaymentWebView: WKWebView!
    @IBOutlet weak var webView: UIView!
    @IBOutlet weak var CloseOrScreenshotbtn: UIButton!
    
    var weburl = ""
    var url = ""
    var webbView: WKWebView!
    let uc = UtilitySoftTechMedia()
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        
        CloseOrScreenshotbtn.isHidden = true
        PaymentWebView.navigationDelegate = self
     
        let urll = URL(string: self.weburl)
        url = "\(urll!)"
        PaymentWebView.load(URLRequest(url: urll!))
        PaymentWebView.allowsBackForwardNavigationGestures = true
  
    }
    
    //Delegate
     func webView(_ webView: WKWebView, didFinish navigation:
        WKNavigation!) {
//        showActivityIndicator(show: false)
        SVProgressHUD.dismiss()
    }

       func webView(_ webView: WKWebView, didStartProvisionalNavigation
       navigation: WKNavigation!) {
        SVProgressHUD.show()
//        showActivityIndicator(show: true)
    }

       func webView(_ webView: WKWebView, didFail navigation:
       WKNavigation!, withError error: Error) {
//        showActivityIndicator(show: false)
         SVProgressHUD.dismiss()
    }
   
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let urlStr = navigationResponse.response.url?.absoluteString{
            print(urlStr)
            if(urlStr.contains("app-success.php"))
            {
                CloseOrScreenshotbtn.isHidden = false
                self.url = urlStr
            }
            
        }
        decisionHandler(.allow)
    }
  
    @IBAction func btnBackClick(_ sender: Any) {
        
        if(self.url.contains("app-success.php")){
            
           self.performSegue(withIdentifier: "transactionList", sender: nil)
            
        }else{
            
            let alertCOntroller = UIAlertController(title: "", message: "Your transaction status is incomplete until you make payment", preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
                
                self.performSegue(withIdentifier: "transactionList", sender: nil)
            })
            alertCOntroller.addAction(action)
            self.present(alertCOntroller,animated: true,completion: nil)
            
        }
        //comment
        
    }
    
    
    func screenshot() -> UIImage {
        if #available(iOS 10.0, *) {
            return UIGraphicsImageRenderer(size: self.webView.bounds.size).image { _ in
                self.webView.drawHierarchy(in: CGRect(origin: .zero, size: self.webView.bounds.size), afterScreenUpdates: true)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(self.webView.bounds.size, false, UIScreen.main.scale)
            self.webView.drawHierarchy(in: self.webView.bounds, afterScreenUpdates: true)
            let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
            UIGraphicsEndImageContext()
            return image
        }
    }
    
    
    
    @IBAction func btnCloseClick(_ sender: Any) {
        
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(screenshot(), nil, nil, nil)
        Toast.text = "ScreenShot saved in image gallery"
        Toast.show()
        Toast.duration = 3
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
}

