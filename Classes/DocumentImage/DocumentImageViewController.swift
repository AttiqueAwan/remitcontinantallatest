//
//  DocumentImageViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 02/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class DocumentImageViewController: UIViewController {
    
    
    @IBOutlet weak var myWebView: UIWebView!
    
    @IBOutlet weak var DocumentImage: UIImageView!
    @IBOutlet weak var btnBack:UIButton!
    @IBOutlet weak var lbltitle:UILabel!
    @IBOutlet weak var btnScreenShot:UIButton!
    
     var DocumentURL = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        
        let l = DocumentURL.components(separatedBy:"/")
               //let file = l.last?.components(separatedBy: ".")[0]
               let ext = l.last?.components(separatedBy: ".")[1]
               if(ext == "pdf")
               {
                self.DocumentImage.isHidden = true
                   self.myWebView.isHidden = false
                   let urll = URL(string: self.DocumentURL)!
                   self.myWebView.loadRequest(URLRequest(url:urll))
                   
               }else{
                self.myWebView.isHidden = true
                self.DocumentImage.isHidden = false
                
                
                   self.DocumentImage.sd_setImage(with: URL(string: self.DocumentURL), placeholderImage: UIImage(named: "ico_camera.png"))
               }
    }
    
    @IBAction func btnBackClick(_ sender: Any){
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnTakeScreenShotClick(_ sender: Any){
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension DocumentImageViewController: WKNavigationDelegate {
    
      //Delegate
         func webView(_ webView: WKWebView, didFinish navigation:
            WKNavigation!) {
    //        showActivityIndicator(show: false)
            SVProgressHUD.dismiss()
        }

           func webView(_ webView: WKWebView, didStartProvisionalNavigation
           navigation: WKNavigation!) {
            SVProgressHUD.show()
    //        showActivityIndicator(show: true)
        }

           func webView(_ webView: WKWebView, didFail navigation:
           WKNavigation!, withError error: Error) {
    //        showActivityIndicator(show: false)
             SVProgressHUD.dismiss()
        }
       
}
