//
//  TransactionCell.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet weak var lblTransStatus: UILabel!
    @IBOutlet weak var lblRecipientName: UILabel!
    @IBOutlet weak var lblTransPIN: UILabel!
    @IBOutlet weak var lblTransDate: UILabel!
    @IBOutlet weak var lblAmountReceive: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var TransactionStatusImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
