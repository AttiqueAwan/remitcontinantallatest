//
//  TransactionViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController {
    @IBOutlet weak var noTransView: UIView!

    @IBOutlet weak var tblTransList: UITableView!
    
    let uc = UtilitySoftTechMedia()
    var transactionListResponse : TransactionList!
    
    let mypopover = Popover()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.tblTransList.tableFooterView = UIView()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        self.getUserTransactions()
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        
        NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: nil)
        
    }
    
    @IBAction func btnAddTransClick(_ sender: Any) {
        
        if !(uc.ProfileCompleteCheck()){
            
            let alertCOntroller = UIAlertController(title: "", message: "Please complete your profile to make a transaction", preferredStyle: UIAlertController.Style.alert)
            
            let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
                
                let  vc =  self.storyboard?.instantiateViewController(withIdentifier: "Profile") as? ProfileViewController
                
                self.present(vc!, animated: true, completion: nil)
                
            })
            alertCOntroller.addAction(action)
            self.present(alertCOntroller,animated: true,completion: nil)
            
        }else{
            
            self.performSegue(withIdentifier: "NewTransaction", sender: nil)
        }
        
    }
    @IBAction func btnAddFirstTransactionClick(_ sender: Any) {
        
        if !(uc.ProfileCompleteCheck()){
            
            let alertCOntroller = UIAlertController(title: "", message: "Please complete your profile to make a transaction", preferredStyle: UIAlertController.Style.alert)
            
            let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action: UIAlertAction) in
                
                let  vc =  self.storyboard?.instantiateViewController(withIdentifier: "Profile") as? ProfileViewController
                
                self.present(vc!, animated: true, completion: nil)
                
            })
            alertCOntroller.addAction(action)
            self.present(alertCOntroller,animated: true,completion: nil)
            
        }else{
            
            self.performSegue(withIdentifier: "NewTransaction", sender: nil)
        }
        
    }
    
    func getUserTransactions(){
        
        
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID, 
                      "Limit":"0",
                      "PaymentMethod":""]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetTransactionList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getUserTransactions()
                return
            }
            
            self.transactionListResponse = TransactionList(JSONString:result)
            
            if self.transactionListResponse?.myAppResult?.Code == 0 {
                
                if(self.transactionListResponse.AceTransList != nil && (self.transactionListResponse.AceTransList?.count)! > 0){
                    
                    
                    
                    self.tblTransList.isHidden = false
                    self.noTransView.isHidden = true
                    self.tblTransList.reloadData()
                    
                    
                }else{
                    
                   self.tblTransList.isHidden = true
                   self.noTransView.isHidden = false
                   
                    
                }
                
                
            }else if self.transactionListResponse?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }
            else if self.transactionListResponse?.myAppResult?.Code == 102 {
                
               self.tblTransList.isHidden = true
                self.noTransView.isHidden = false
                
            }
            else{
                self.tblTransList.isHidden = true
                self.noTransView.isHidden = false
                if(self.transactionListResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Alert", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Alert", (self.transactionListResponse?.myAppResult?.Message)!, self)
                }
            }
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "TransDetail"){
            
            let destination = segue.destination as? TransDetailViewController
            destination?.UpdateTrans = self.transactionListResponse.AceTransList![(sender as? Int)!]
       
        }
        else if(segue.identifier == "CancelTrans"){
            
            let destination = segue.destination as? NewComplaintVC
            destination?.UpdateTrans = self.transactionListResponse.AceTransList![(sender as? Int)!]
        }
        else if(segue.identifier == "PayAtLoc"){
            
            let destination = segue.destination as? PayAtLocationVC
            destination?.TransDetail = self.transactionListResponse.AceTransList![(sender as? Int)!]
            
        }else if(segue.identifier == "Complance"){
            
            let destination = segue.destination as? SupportListVC
            destination?.PaymentId = self.transactionListResponse.AceTransList![(sender as? Int)!].PaymentID!
            
        }
        if(segue.identifier == "PaymentSegue"){
            
            let destination = segue.destination as? PaymentViewController
            let PageURL = transactionListResponse.AceTransList![(sender as? Int)!].PaymentPageURL!
            let PaymentID = transactionListResponse.AceTransList![(sender as? Int)!].PaymentID!
            let url =  "\(String(describing: PageURL))?id=\(String(describing: PaymentID))"
            destination?.weburl = url
        }
        
    }
    

    @IBAction func btnPlus(_ sender: UIButton) {
        performSegue(withIdentifier: "NewTransaction", sender:  nil)
    }
    
    var section = ["Complet","Incomplete"]
    
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
}


extension TransactionViewController:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.transactionListResponse.AceTransList![indexPath.row].SendingPaymentMethod == "Cash")
        {
            self.performSegue(withIdentifier: "PayAtLoc", sender: indexPath.row)
        }
        else
        {
            self.performSegue(withIdentifier: "TransDetail", sender: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard (self.transactionListResponse) != nil else {
            return 0
        }

        guard let count = self.transactionListResponse.AceTransList?.count else {
            return 0
        }
        return count
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //animation 1
        cell.alpha = 0.5
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.05,
            animations: {
                cell.alpha = 1
                self.view.layoutIfNeeded()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblTransList.dequeueReusableCell(withIdentifier: "cell") as? TransactionCell
        
        cell?.lblRecipientName.text = self.transactionListResponse.AceTransList![indexPath.row].BeneName
        cell?.lblTransStatus.text = self.transactionListResponse.AceTransList![indexPath.row].PaymentStatus
        cell?.lblTransPIN.text = "Reference code: \((self.transactionListResponse.AceTransList![indexPath.row].PaymentNumber!))"
        
        let transDate = self.transactionListResponse.AceTransList![indexPath.row].PaymentDate?.components(separatedBy: "T")[0].components(separatedBy:"-")
        
        let month = uc.getMonth(month: Int((transDate?[1])!)!)
        
        cell?.lblTransDate.text = "\((month))-\((transDate?[2])!)-\((transDate?[0])!)"
        
        cell?.lblAmountReceive.text = "Recipient gets: \((self.transactionListResponse.AceTransList![indexPath.row].PayOutAmount)!) \((self.transactionListResponse.AceTransList![indexPath.row].ReceivingCurrency)!)"
        
        
        guard let paymentStatus = self.transactionListResponse.AceTransList![indexPath.row].PaymentStatus else {
            
            return cell!
        }
        
        
        cell!.btnEdit.addTarget(self, action: #selector(self.ShowPopUP(sender:)), for: .touchUpInside)
        cell!.btnEdit.tag = indexPath.row
        
        //Start checking status
        
        if(paymentStatus == "Canceled"){
            
            cell!.TransactionStatusImg.image = UIImage(named: "cancel")
            //IMG_comp-hold
            
        }else if(paymentStatus == "Canceling"){
            
           //IMG_comp-hold
            cell!.TransactionStatusImg.image = UIImage(named: "cancel")
            
        }else if(paymentStatus == "Completed"){
            
          //IMG_complete
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_complete")
            
        }else if(paymentStatus.lowercased() == "in-process" || paymentStatus.lowercased() == "inprocess"){
            
          //IMG_inprogess
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_inprogess")
            
        }else if(paymentStatus == "Incomplete"){
            
           //IMG_incomplete
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_incomplete")
            
        }else if(paymentStatus == "ok"){
            
          //IMG_ok
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_ok")
            
        }else if(paymentStatus == "Pending"){
            
           //IMG_comp-hold
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_comp-hold")
            
        }else if(paymentStatus == "Paid"){
          
            //IMG_complete
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_complete")
            
        }else if(paymentStatus == "Compliance Hold" || paymentStatus.contains("Compliance")){
          
            //IMG_comp-hold
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_comp-hold")
            
        }else{
            
            cell!.TransactionStatusImg.image = UIImage(named: "IMG_inprogess")
          
        }
        
        //End status checking
        
        return cell!
        
    }
    
    @objc func ShowPopUP(sender: UIButton){
        let paymentStatus = self.transactionListResponse.AceTransList![sender.tag].PaymentStatus
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblTransList)
        let indexPath = self.tblTransList.indexPathForRow(at:buttonPosition)
        let rectOfCellInTableView = tblTransList.rectForRow(at: indexPath!)
        let rectOfCellInSuperview = tblTransList.convert(rectOfCellInTableView, to: tblTransList.superview)
        // print("Y of Cell is: \(rectOfCellInSuperview.origin.y)")
        
        var height = CGFloat()
        var aView = UIView()
       if(paymentStatus == "Incomplete")
       {
        
        _ = sender.tag
        _ = CGPoint(x: sender.frame.origin.x+225, y: rectOfCellInSuperview.origin.y+260)
        aView = UIView(frame: CGRect(x: 10, y: 20, width: self.view.frame.width/2, height: 60))
        
        let View = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        View.setTitle("View Detail", for: .normal)
        View.setTitleColor(UIColor.black, for: .normal)
        View.frame = CGRect(x: aView.frame.origin.x+10, y:10, width:self.view.frame.width/2,height:40)
        View.tag=sender.tag
        View.addTarget(self, action: #selector(self.View(_:)), for: .touchUpInside)
        View.contentHorizontalAlignment = .left
        
        aView.addSubview(View)
        }
        else
       {
        
        _ = sender.tag
        _ = CGPoint(x: sender.frame.origin.x+225, y: rectOfCellInSuperview.origin.y+260)
        aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 220))
       
        let View = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        View.setTitle("View Detail", for: .normal)
        View.setTitleColor(UIColor.black, for: .normal)
        View.frame = CGRect(x: aView.frame.origin.x+10, y:10, width:self.view.frame.width/2,height:40)
        View.tag=sender.tag
        View.addTarget(self, action: #selector(self.View(_:)), for: .touchUpInside)
        View.contentHorizontalAlignment = .left
        height = 10 + 50
        
        let Complaint = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        Complaint.setTitle("Complaint", for: .normal)
        Complaint.setTitleColor(UIColor.black, for: .normal)
        Complaint.frame = CGRect(x: aView.frame.origin.x+10, y: 60, width:self.view.frame.width/2,height:40)
        Complaint.tag=sender.tag
        Complaint.addTarget(self, action: #selector(self.Complaint(_:)), for: .touchUpInside)
        Complaint.contentHorizontalAlignment = .left
        
        let Send = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        Send.setTitle("Send Again", for: .normal)
        Send.setTitleColor(UIColor.black, for: .normal)
        if( paymentStatus == "Canceled" || paymentStatus == "Complete" ){
            height = height + 50
            Send.frame = CGRect(x: aView.frame.origin.x+10, y:height, width:self.view.frame.width/2,height:40)
        }
        Send.tag=sender.tag
        Send.addTarget(self, action: #selector(self.Send(_:)), for: .touchUpInside)
        Send.contentHorizontalAlignment = .left
        
        let Cancel = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        Cancel.setTitle("Cancel Transaction", for: .normal)
        Cancel.setTitleColor(UIColor.black, for: .normal)
        if( paymentStatus == "In-process"  ){
            height = height + 50
            Cancel.frame = CGRect(x: aView.frame.origin.x+10, y:height, width:self.view.frame.width/2,height:40)
        }
        Cancel.tag=sender.tag
        Cancel.addTarget(self, action: #selector(self.Cancel(_:)), for: .touchUpInside)
        Cancel.contentHorizontalAlignment = .left
        
        
        
        var check = false
        
        if( paymentStatus == "Canceled" || paymentStatus == "Complete" ){
            
            aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 160))
            aView.addSubview(Send)
            check = true
        }else if( paymentStatus == "In-process"){
            
            aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 160))
            aView.addSubview(Cancel)
            
        }
        if(check == true)
        {
            if( paymentStatus == "In-process" ){
                
                aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 210))
                aView.addSubview(Cancel)
            }
        }
        
        if( paymentStatus != "Canceled" && paymentStatus != "Complete" && paymentStatus != "In-process" && paymentStatus != "Incomplete" )
        {
            aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 110))
        }
        
        aView.addSubview(Complaint)
        aView.addSubview(View)
        }
        
        mypopover.isHidden=false
        
        if((indexPath?.row)! < 2){
            
            mypopover.popoverType = .down
        }else{
            
            mypopover.popoverType = .up
        }
        
        
        mypopover.show(aView, fromView: sender)
    }
    
    
    //Press Delete Button in PopUp
    @objc func View(_ sender:UIButton){
        if(self.transactionListResponse.AceTransList![sender.tag].SendingPaymentMethod == "Cash")
        {
            self.performSegue(withIdentifier: "PayAtLoc", sender: sender.tag)
        }
        else
        {
            self.performSegue(withIdentifier: "TransDetail", sender: sender.tag)
        }
        mypopover.dismiss()
        
    }
    
    @objc func Send(_ sender:UIButton){
        //PaymentSegue
        self.performSegue(withIdentifier: "NewTransaction", sender: nil)
        mypopover.dismiss()
        
    }
    
    
    @objc func Cancel(_ sender:UIButton){
        self.performSegue(withIdentifier: "CancelTrans", sender: sender.tag)
        mypopover.dismiss()
    }
    
  
    @objc func Complaint(_ sender:UIButton){
        self.performSegue(withIdentifier: "Complance", sender: sender.tag)
        mypopover.dismiss()
    }
    
    
}
