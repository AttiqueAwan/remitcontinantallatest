//
//  BankDetailCell.swift
//  UKAsiaRemitt
//
//  Created by Mac on 02/08/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit

class BankDetailCell: UITableViewCell {

    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblAccountNum: UILabel!
    @IBOutlet weak var lblSortCode: UILabel!
    @IBOutlet weak var lblSwiftCode: UILabel!
    @IBOutlet weak var lblReferenceNum: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
