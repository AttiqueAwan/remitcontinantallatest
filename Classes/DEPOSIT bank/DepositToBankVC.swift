//
//  DepositToBankVC.swift
//  UKAsiaRemitt
//
//  Created by Mac on 02/08/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import MaterialControls


class DepositToBankVC: UIViewController , UITableViewDelegate , UITableViewDataSource{
   
    var Toast = MDToast()

    @IBOutlet weak var LblDescription: UILabel!
    @IBOutlet weak var BankListTblVIew: UITableView!
    
    let uc = UtilitySoftTechMedia()
    var BankDetailResp : BankDetailResponse!
    var UpdateTrans : TransactionListresult!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        let amount = Float(self.UpdateTrans.PayInAmount!)!
        let Charges = Float(self.UpdateTrans.ServiceCharges!)!
        let Sum = Float(amount + Charges)
        
        let charCount = self.UpdateTrans.SendingCurrency!.count + String(Sum).count + 1
        
        var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string:  "Please transfer the full amount of \(self.UpdateTrans.SendingCurrency!) \(String(Sum)) from your bank account to our bank account using the bank details and reference number shown below. You can do this by online/telephone banking or by visiting your bank in person.\n\nRemember that you need to send the money from a bank account in your name within the next 24 hours otherwise it will be cancelled automatically.")
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.red,range: NSRange(location:35,length:charCount))
        myMutableString.addAttribute(NSAttributedString.Key.font,value:UIFont.boldSystemFont(ofSize: 13),range: NSRange(location:35,length:charCount))
        
        self.LblDescription.attributedText = myMutableString
        self.GetBankDetailList()
    }

    func screenshot() -> UIImage {
           if #available(iOS 10.0, *) {
               return UIGraphicsImageRenderer(size: self.view.bounds.size).image { _ in
                   self.view.drawHierarchy(in: CGRect(origin: .zero, size: self.view.bounds.size), afterScreenUpdates: true)
               }
           } else {
               UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, UIScreen.main.scale)
               self.view.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
               let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
               UIGraphicsEndImageContext()
               return image
           }
       }
       
       
       func screenShotMethod() {
           //Save it to the camera roll
           UIImageWriteToSavedPhotosAlbum(screenshot(), nil, nil, nil)
           Toast.text = "ScreenShot saved in image gallery"
           Toast.show()
           Toast.duration = 3
       }
    
    @IBAction func TakeScreenshot(_ sender: UIButton) {
        
        screenShotMethod()
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.BankDetailResp != nil){
            
            return (self.BankDetailResp?.BankDetail?.count)!
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.BankListTblVIew.dequeueReusableCell(withIdentifier: "cell") as? BankDetailCell
        cell?.lblReferenceNum.text = self.UpdateTrans.PaymentNumber!
        cell?.lblBankName.text = (self.BankDetailResp?.BankDetail![indexPath.row].BankName)!
        cell?.lblAccountNumber.text = (self.BankDetailResp?.BankDetail![indexPath.row].AccountTitle)!
        cell?.lblAccountNum.text = (self.BankDetailResp?.BankDetail![indexPath.row].AccountNumber)!
        cell?.lblSortCode.text = (self.BankDetailResp?.BankDetail![indexPath.row].SortCode)!
        cell?.lblSwiftCode.text = (self.BankDetailResp?.BankDetail![indexPath.row].SwiftCode)!
        
        let amount = Float(self.UpdateTrans.PayInAmount!)!
        let Charges = Float(self.UpdateTrans.ServiceCharges!)!
        let Sum = Float(amount + Charges)
        
        cell?.lblAmount.text = "\(self.UpdateTrans.SendingCurrency!) \(String(Sum))"
        return cell!
    }
    
    func GetBankDetailList()
    {
        
        let userinfo = uc.getUserInfo()
        
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,
                      "AppID":ApiUrls.AppID,
                      "CountryIso3Code":(userinfo?.UserInfo?.CountryIsoCode)!]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.DepositBankList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.GetBankDetailList()
                return
            }
            
            self.BankDetailResp = BankDetailResponse(JSONString:result)
            
            if self.BankDetailResp?.myAppResult?.Code == 0 {
                
                if(self.BankDetailResp?.BankDetail != nil && (self.BankDetailResp?.BankDetail?.count)! > 0){
                    
                    self.BankListTblVIew.isHidden = false
                    self.BankListTblVIew.reloadData()
                    
                }
                else
                {
                
                    self.BankListTblVIew.isHidden = true
 
                }
                
                
            }else if self.BankDetailResp?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(self.BankDetailResp?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.BankDetailResp?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }

}
