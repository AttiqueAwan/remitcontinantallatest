//
//  ChangePasswordViewController.swift
//  AyanExpress
//
//  Created by Softech Media on 05/03/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ChangePasswordViewController: UIViewController ,UITextFieldDelegate {

    @IBOutlet weak var txtCurrentPassword: JVFloatLabeledTextField!
    @IBOutlet weak var txtNewPassword: JVFloatLabeledTextField!
    @IBOutlet weak var txtConfirmPassword: JVFloatLabeledTextField!
    
    let txtFieldSetting = textFieldSetting()
    let uc = UtilitySoftTechMedia()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        if let viewControllers = self.navigationController?.viewControllers {
            for vc in viewControllers {
                if vc.isKind(of: ChangePasswordViewController.classForCoder()) {
                    print("It is in stack")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func BtnResetPasswordFunc(_ sender: UIButton) {
        if(txtCurrentPassword.text == "")
        {
            self.uc.errorSuccessAler("", "Current password can't be null", self)
        }
        else if(txtNewPassword.text == "")
        {
            self.uc.errorSuccessAler("", "New password can't be null", self)
        }
        else if(txtConfirmPassword.text == "")
        {
            self.uc.errorSuccessAler("", "Confirm password can't be null", self)
        }
        else if(txtNewPassword.text != txtConfirmPassword.text)
        {
            self.uc.errorSuccessAler("", "New and confirm password not matched.", self)
        }
        else
        {
           ResetPassword()
        }
        
    }
    
    //getDashboard
    func ResetPassword(){
       
        let parms =  ["NewPassword":self.txtNewPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines),"OldPassword":self.txtCurrentPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines),"Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID,"ID":(uc.getAuthToken()?.AuthToken?.user_id)!]as [String : Any]

        uc.webServicePosthttp(urlString: ApiUrls.changepassword, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.ResetPassword()
                return
            }
            let resultsss = AppUser(JSONString: result)
            print(result)
            if(result.contains("Successfully done"))
            {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message: "\(resultsss!.Data!)", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else if(result.contains("length between 6 and 25"))
            {
                self.uc.errorSuccessAler("", "Password must have a length between 6 and 25", self)
            }
            else if(result.contains("Invalid old password."))
            {
                self.uc.errorSuccessAler("", "Invalid old password.", self)
            }
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtCurrentPassword){
            
            self.txtCurrentPassword = self.txtFieldSetting.textFieldBaseLine(self.txtCurrentPassword, #colorLiteral(red: 0.8901960784, green: 0.1176470588, blue: 0.1529411765, alpha: 1))
            
        }else if(textField == self.txtNewPassword){
            
            
            self.txtNewPassword = self.txtFieldSetting.textFieldBaseLine(self.txtNewPassword, #colorLiteral(red: 0.8901960784, green: 0.1176470588, blue: 0.1529411765, alpha: 1))
        }else{
            
            
            self.txtConfirmPassword = self.txtFieldSetting.textFieldBaseLine(self.txtConfirmPassword, #colorLiteral(red: 0.8901960784, green: 0.1176470588, blue: 0.1529411765, alpha: 1))
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField.text == ""){
            
            if(textField == self.txtCurrentPassword){
                
                self.txtCurrentPassword = self.txtFieldSetting.textFieldBaseLine(self.txtCurrentPassword, #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
                
            }else if(textField == self.txtNewPassword){
                
                
                self.txtNewPassword = self.txtFieldSetting.textFieldBaseLine(self.txtNewPassword, #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
            }else{
                
                
                self.txtConfirmPassword = self.txtFieldSetting.textFieldBaseLine(self.txtConfirmPassword, #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
            }
            
        }
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField.text == ""){
            if(textField == self.txtCurrentPassword){
                
                self.txtCurrentPassword = self.txtFieldSetting.textFieldBaseLine(self.txtCurrentPassword, #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
                
            }else if(textField == self.txtNewPassword){
                
                
                self.txtNewPassword = self.txtFieldSetting.textFieldBaseLine(self.txtNewPassword, #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
            }else{
                
                
                self.txtConfirmPassword = self.txtFieldSetting.textFieldBaseLine(self.txtConfirmPassword, #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
            }

            
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
    

}
