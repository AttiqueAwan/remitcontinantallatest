//
//  RecipientViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 20/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit

class RecipientViewController: UIViewController {

    @IBOutlet weak var tblRecipient: UITableView!
    @IBOutlet weak var noRecipientView: UIView!
    @IBOutlet weak var backBtnView: UIView!
    @IBOutlet weak var boxHeight: NSLayoutConstraint!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var addViewHeight: NSLayoutConstraint!
    
    
    let mypopover = Popover()
    
    let uc = UtilitySoftTechMedia()
    
    var recipientResponce : RecipientList!
    
    var UserInfo:RecipientListResult!
    
    var FilteredArray = [RecipientListResult?]()
    
    var parms:[String:Any]!
    
    fileprivate var isFromDashboard = false
    
    var isFromAmountDetail = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgView.isHidden = true
        boxHeight.constant = 0
        addViewHeight.constant = 0
        if(isFromAmountDetail == true){
            
            imgView.isHidden = false
            boxHeight.constant = 70
            addViewHeight.constant = 80
        }
        
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.03529411765, blue: 0.03921568627, alpha: 1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(isFromDashBoardNotication(_:)), name: ApiUrls.FromDashboardNotification, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @objc  func isFromDashBoardNotication(_ notification:NSNotification) {
        isFromDashboard = true;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(Dashboard == true)
        {
//            self.backBtnView.isHidden = true
        }
        else
        {
            //crash here when back from ReviewViewController
            self.backBtnView.isHidden = false
        }
        self.tblRecipient.tableFooterView = UIView()
        DispatchQueue.main.async {

            
            self.getUserRecipient()
        }
    }

    @IBAction func btnBackClick(_ sender: Any) {
        
//        NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: nil)
//        self.navigationController?.popViewController(animated: true)
       
        
               
        //NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: nil)
        
        if isFromDashboard {
            NotificationCenter.default.post(name: ApiUrls.ShowDashboardNotification, object: nil, userInfo: nil)
            if let viewControllers = self.navigationController?.viewControllers {
                for vc in viewControllers {
                    if vc.isKind(of: DashboardViewController.self) {
                        print("isKind find VC")
                        navigationController?.popToViewController(vc, animated: false)
                        return
                    }
                }
            }
           // self.navigationController?.popViewController(animated: true)
        }else {
            
            if isFromAmountDetail {
                self.navigationController?.popViewController(animated: true)
            }else {
                if let viewControllers = self.navigationController?.viewControllers {
                    for vc in viewControllers {
                        if vc.isKind(of: DashboardViewController.self) {
                            print("isKind find VC")
                            navigationController?.popToViewController(vc, animated: false)
                            return
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnAddRecipientClick(_ sender: Any) {
        self.UserInfo = nil
        self.performSegue(withIdentifier: "AddUpdateRecipient", sender: parms)
        
    }
  
    @IBAction func addBtn(_ sender: Any) {
        
        self.UserInfo = nil
        self.performSegue(withIdentifier: "AddUpdateRecipient", sender: parms)
        
    }
    @IBAction func btnAddFirstRecipientClick(_ sender: Any) {
        
        self.UserInfo = nil
        self.performSegue(withIdentifier: "AddUpdateRecipient", sender: parms)
        
        
    }
    
    func getUserRecipient(){
        
    
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,"AppID":ApiUrls.AppID]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetRecipientList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.getUserRecipient()
                return
            }
            self.FilteredArray.removeAll()
            self.recipientResponce = RecipientList(JSONString:result)
            
            if self.recipientResponce?.myAppResult?.Code == 0 {
                
                if(self.recipientResponce.AceBeneList != nil && (self.recipientResponce.AceBeneList?.count)! > 0){
                    
                    //Start kamran code
                    
                    if(self.parms != nil)
                    {
                        let Selectedcountry = self.parms["Country"] as! String
                        let count = (self.recipientResponce.AceBeneList?.count)!
                        let paymentMethodName = self.parms["Payment_Method"] as! String
                        var paymentMethod = 0

                                          
                        if (paymentMethodName == "9"){
                            paymentMethod = 9
                            
                        }else if (paymentMethodName == "10"){
                            paymentMethod = 10
                            
                        }else if (paymentMethodName == "751"){
                            paymentMethod = 751
                            
                        }else if (paymentMethodName == "752"){
                            paymentMethod = 752
                            
                        }else if (paymentMethodName == "753"){
                            paymentMethod = 753
                            
                        }
                        
                        for i in 0..<count
                        {
                            let country =
                                (self.recipientResponce!.AceBeneList![i].CountryName)
                            //(self.recipientResponce!.AceBeneList![i].CountryName)
                            if(Selectedcountry == country && paymentMethod == self.recipientResponce!.AceBeneList![i].BenePayMethod)
                            {
                            self.FilteredArray.append(self.recipientResponce!.AceBeneList![i])
                            }
                        }
                    }
                    else
                    {
                        
                        self.FilteredArray = self.recipientResponce!.AceBeneList!
                    }
                    
                    //end kamran code
                    
                    if ((self.FilteredArray.count) > 0) {
                        self.tblRecipient.reloadData()
                        self.tblRecipient.isHidden = false
                        self.noRecipientView.isHidden = true
                    }
                    
                }else{
                    
                    self.tblRecipient.isHidden = true
                    self.noRecipientView.isHidden = false
                    
                }
                
                
            }else if self.recipientResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else if self.recipientResponce?.myAppResult?.Code == 102 {
                
                self.tblRecipient.isHidden = true
                self.noRecipientView.isHidden = false
                
            }else{
                
                if(self.recipientResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("", (self.recipientResponce?.myAppResult?.Message)!, self)
                }
            }
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "Review"){
            
            let vc = segue.destination as? ReviewViewController
            vc?.parms = self.parms
            vc?.UserInfo = self.UserInfo
            
        }else if(segue.identifier == "AddUpdateRecipient"){
            
            
            let vc = segue.destination as? AddRecipientViewController
            vc?.isFromDashboard = isFromDashboard
            vc?.isFromAmountDetail = isFromAmountDetail
            vc?.parmsDiction = sender as? [String: Any]

            if(UserInfo != nil){
                 vc?.UpdateRecipient = self.UserInfo
            }
        }
        else if(segue.identifier == "RecipientSelected")
        {
            let vc = segue.destination as? CreateTransViewController
            vc?.UserInfo = self.UserInfo
        }
    }
    
    @IBAction func btnLogoutM(_ sender: UIButton) {
        uc.logout(self)
    }
}

extension RecipientViewController:UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        print(indexPath.row)
        UserInfo  = self.recipientResponce.AceBeneList![indexPath.row]
        
        if(self.parms == nil){
            
        }else{
            if(Initial_Payment_Method != "")
            {
            if(Initial_Payment_Method == "Bank"){
            
            if(self.recipientResponce.AceBeneList![indexPath.row].BeneBankName! == "" || self.recipientResponce.AceBeneList![indexPath.row].BeneBranchName! == "" || self.recipientResponce.AceBeneList![indexPath.row].BeneAccountNumber! == ""){
                
                let alertController = UIAlertController(title: "", message: "Please fill user bank info", preferredStyle: .alert)
                let alertaction = UIAlertAction(title: "OK", style: .default, handler:{action in
                    
                    self.UserInfo = nil
                    
                    self.UserInfo = self.recipientResponce.AceBeneList![indexPath.row]
                    self.performSegue(withIdentifier: "AddUpdateRecipient", sender: nil)
                    
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler:{action in
                    
                    self.UserInfo = nil
                    alertController.dismiss(animated: true, completion: nil)
                })
                alertController.addAction(cancel)
                alertController.addAction(alertaction)
                self.present(alertController, animated: true, completion: nil)
                
            }else{
                self.UserInfo = self.recipientResponce.AceBeneList![indexPath.row]
                self.performSegue(withIdentifier: "Review", sender: nil)
            }
            
                }
                
            }else{
            self.UserInfo = self.recipientResponce.AceBeneList![indexPath.row]
            self.performSegue(withIdentifier: "Review", sender: nil)
        }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        guard (self.recipientResponce) != nil else {
//            return 0
//        }
//
//        guard let count = self.recipientResponce.AceBeneList?.count else {
//            return 0
//        }
//        return count
        
        guard (self.recipientResponce) != nil else {
                   return 0
               }
               
        return self.FilteredArray.count
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //animation 1
        cell.alpha = 0.5
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.05,
            animations: {
                cell.alpha = 1
                self.view.layoutIfNeeded()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        
        let cell = self.tblRecipient.dequeueReusableCell(withIdentifier: "RecipientCell") as? RecipientCell
        
        cell?.lblName.text = self.FilteredArray[indexPath.row]!.BeneName
        cell?.LblBeneInitials.text = "\(self.FilteredArray[indexPath.row]!.BeneFirstName?.first ?? " ")\(self.FilteredArray[indexPath.row]!.BeneLastName?.first ?? " ")"
//        let namearray = self.FilteredArray[indexPath.row]!.BeneName?.split(separator: " ")

//        cell?.lblBeneNameInitials.text = "\(String(describing: namearray![0].first!))\(String(describing: namearray![1].first!))"
        
        cell?.btnEdit.tag = indexPath.row
        cell?.btnEdit.addTarget(self, action: #selector(self.ShowPopUP(sender:)), for: .touchUpInside)
        
        guard let paymentStatus = self.FilteredArray[indexPath.row]!.BenePayMethod else {
            return cell!
        }
        
        
        if(paymentStatus == 9){
            
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "Delivery Option:  Cash Pick-up")
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:17))
          
            cell?.lblReceivingMethod.attributedText = myMutableString
            
            myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "Receiver Phone:  \((self.FilteredArray[indexPath.row]!.BenePhone)!)")
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:17))
            
            cell?.lblPhone.attributedText =  myMutableString
            
            
        }else {
            
            let benePayMethod = self.FilteredArray[indexPath.row]!.BenePayMethod
            var benePayCode = ""
            
        
           var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: "Delivery Option: \(benePayCode)")
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:17))
            
            
            
             cell?.lblReceivingMethod.attributedText = myMutableString
            
            myMutableString = NSMutableAttributedString()
            
            if benePayMethod == 10 {
                benePayCode = "Deposit To Bank"
                myMutableString = NSMutableAttributedString(string: "Delivery Option: \(benePayCode)")
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:17))
                cell?.lblReceivingMethod.attributedText = myMutableString
                
                if(self.FilteredArray[indexPath.row]!.BeneAccountNumber! != "")
                {
                     myMutableString = NSMutableAttributedString(string: "A/C #:  \((self.FilteredArray[indexPath.row]!.BeneAccountNumber)!)")
                }
                else
                {
                     myMutableString = NSMutableAttributedString(string: "IBAN :  \((self.FilteredArray[indexPath.row]!.BeneIBAN)!)")
                }

//                myMutableString = NSMutableAttributedString(string: "A/C #:  \((self.FilteredArray[indexPath.row]!.BeneAccountNumber)!)")
                
            }else if benePayMethod == 751 {
                
                benePayCode = "Mobile Wallet"
                myMutableString = NSMutableAttributedString(string: "Delivery Option: \(benePayCode)")
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:17))
                cell?.lblReceivingMethod.attributedText = myMutableString
                myMutableString = NSMutableAttributedString(string: "Receiver Phone:  \((self.FilteredArray[indexPath.row]!.BenePhone)!)")
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:13))
                
            }else if benePayMethod == 752 {
                benePayCode = "Bill Payment"
                
                myMutableString = NSMutableAttributedString(string: "Delivery Option: \(benePayCode)")
                cell?.lblReceivingMethod.attributedText = myMutableString
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:17))
                myMutableString = NSMutableAttributedString(string: "Receiver Phone:  \((self.FilteredArray[indexPath.row]!.BenePhone)!)")
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:13))
                
            }else if benePayMethod == 753 {
                benePayCode = "Prepaid Card"
              
                myMutableString = NSMutableAttributedString(string: "Delivery Option: \(benePayCode)")
                cell?.lblReceivingMethod.attributedText = myMutableString
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:17))
                myMutableString = NSMutableAttributedString(string: "Card Number:  \((self.FilteredArray[indexPath.row]!.PrepaidCardNumber)!)")
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:13))
            }
           
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), range: NSRange(location:0,length:5))
            
            cell?.lblPhone.attributedText =  myMutableString
        }
        
        if let Country = self.FilteredArray[indexPath.row]!.BeneCountryIsoCode {
            
            let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
            
            let AllCountries = CountryListResult(JSONString: result)
            
            
            for i in 0..<(AllCountries?.AceCountryList?.count)! {
                
                if(AllCountries?.AceCountryList![i].Iso3Code == Country){
                    
                    if let Country = AllCountries?.AceCountryList![i].CountryName {
                        
                        cell?.imgFlag.image = UIImage(named: "\(Country).png")
                        return cell!
                    }
                }
            }
        }
        return cell!
    }
    
    @objc func ShowPopUP(sender: UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblRecipient)
        let indexPath = self.tblRecipient.indexPathForRow(at:buttonPosition)
        let rectOfCellInTableView = tblRecipient.rectForRow(at: indexPath!)
        let rectOfCellInSuperview = tblRecipient.convert(rectOfCellInTableView, to: tblRecipient.superview)
        // print("Y of Cell is: \(rectOfCellInSuperview.origin.y)")
        _ = sender.tag
        _ = CGPoint(x: sender.frame.origin.x+225, y: rectOfCellInSuperview.origin.y+260)
        let aView = UIView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width/2, height: 125))
        
        let View = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        View.setTitle("View/Edit", for: .normal)
        View.setTitleColor(UIColor.black, for: .normal)
        View.frame = CGRect(x:10, y:10, width:self.view.frame.width/2,height:40)
        View.tag=sender.tag
        View.addTarget(self, action: #selector(self.View(_:)), for: .touchUpInside)
        View.contentHorizontalAlignment = .left
        
        let Send = UIButton() // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        Send.setTitle("Send Money", for: .normal)
        Send.setTitleColor(UIColor.black, for: .normal)
        Send.frame = CGRect(x: 10, y:60, width:self.view.frame.width/2,height:40)
        Send.tag=sender.tag
        Send.addTarget(self, action: #selector(self.Send(_:)), for: .touchUpInside)
        Send.contentHorizontalAlignment = .left
        
        //delet button
        
//
        aView.addSubview(View)
        aView.addSubview(Send)
        mypopover.isHidden=false
        
        if((indexPath?.row)! < 2){
            
            mypopover.popoverType = .down
        }else{
            mypopover.popoverType = .up
        }
        mypopover.show(aView, fromView: sender)
    }
    
    //Delete functionility
    @objc func deleteFunc(_ sender:UIButton){
    
        showSimpleAlert(sender)
   
    }
    
    func showSimpleAlert(_ sender:UIButton) {
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete this Recipient",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
            
            print("DELETED")
            self.deleteRecipient(sender)
            self.mypopover.dismiss()
            
            
           }))
           alert.addAction(UIAlertAction(title: "Canel",
                                         style: UIAlertAction.Style.default,
                                         handler: {(_: UIAlertAction!) in
                                           //Sign out action
           }))
           self.present(alert, animated: true, completion: nil)
        
    }
    
    
    //Press Delete Button in PopUp
    @objc func View(_ sender:UIButton){
        self.UserInfo = self.FilteredArray[sender.tag]
        self.performSegue(withIdentifier: "AddUpdateRecipient", sender: nil)
        mypopover.dismiss()
        
    }
    
    @objc func Send(_ sender:UIButton){
        if(RecipientSelected == false)
        {
            //try
            
            setPaymentMethod(sender)

            if(Initial_Payment_Method != "")
            {
                if(Initial_Payment_Method == "Bank")
                {
                    UserInfo  = self.FilteredArray[sender.tag]
                    if(self.FilteredArray[sender.tag]!.BeneBankName! == "" ||  self.FilteredArray[sender.tag]!.BeneAccountNumber! == ""){
                
                        let alertController = UIAlertController(title: "", message: "Please fill user bank info", preferredStyle: .alert)
                        let alertaction = UIAlertAction(title: "OK", style: .default, handler:{action in
                    
                            self.UserInfo = nil
                    
                            self.UserInfo = self.recipientResponce.AceBeneList![sender.tag]
                            self.performSegue(withIdentifier: "AddUpdateRecipient", sender: nil)
                    
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler:{action in
                    self.UserInfo = nil
                    alertController.dismiss(animated: true, completion: nil)
                })
                alertController.addAction(cancel)
                alertController.addAction(alertaction)
                self.present(alertController, animated: true, completion: nil)
                
            }else{
//                        setPaymentMethod(sender)
                self.UserInfo = self.FilteredArray[sender.tag]
                self.performSegue(withIdentifier: "Review", sender: nil)
            }
                }else
                {
//                    setPaymentMethod(sender)
                   self.UserInfo = self.FilteredArray[sender.tag]
                   self.performSegue(withIdentifier: "Review", sender: nil)
                }
            }
            //addedd
            else
            {
//                setPaymentMethod(sender)
               self.UserInfo = self.FilteredArray[sender.tag]
               self.performSegue(withIdentifier: "Review", sender: nil)
            }
        }
        else
        {
            setPaymentMethod(sender)
            UserInfo  = self.FilteredArray[sender.tag]
            self.performSegue(withIdentifier: "RecipientSelected", sender: sender.tag)
        }
        mypopover.dismiss()
    }
    
    func setPaymentMethod(_ sender: UIButton){
        if let pMetho = self.FilteredArray[sender.tag]?.BenePayMethod {
            var pMethodN = ""
            switch pMetho {
            case 9:
                pMethodN = "Cash"
            case 10:
                pMethodN = "Bank"
            case 751:
                pMethodN = "Wallet"
            case 752:
                pMethodN = "Prepaid"
            case 753:
                pMethodN = "Bill Payment"
            default:
                print("default")
            }
            Initial_Payment_Method = pMethodN
            PayoutMethodobj = "\(pMetho)"
        }
    }
}

extension RecipientViewController {
    
    func deleteRecipient(_ sender:UIButton){
        
        
        // /api/benes/deletebene
//        Token:{{Token}}
//        ID:{{ID}}
//        BeneID:1304125
        
         let recipientObject = self.FilteredArray[sender.tag]
        let beneId = recipientObject?.BeneID
        
       
        let parms =  ["ID":(uc.getAuthToken()?.AuthToken?.user_id)!,
                      "Token":ApiUrls.AuthToken,"BeneID":beneId!]as [String : Any]
        
        print(parms)
        uc.webServicePosthttp(urlString: ApiUrls.DeleteRecipient, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.deleteRecipient(sender)
                return
            }
            self.recipientResponce = RecipientList(JSONString:result)
            
            if self.recipientResponce?.myAppResult?.Code == 0 {
                
                
                if let index = self.FilteredArray.firstIndex(where:
                    {$0?.BeneID == recipientObject?.BeneID}) {
                    self.FilteredArray.remove(at: index)
                    self.tblRecipient.reloadData()
                } else {
                   // item could not be found
                }
   
                
            }else if self.recipientResponce?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else if self.recipientResponce?.myAppResult?.Code == 102 {
                
                self.tblRecipient.isHidden = true
                self.noRecipientView.isHidden = false
                
            }else{
                
                if(self.recipientResponce?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("", (self.recipientResponce?.myAppResult?.Message)!, self)
                }
            }
        }
    }

}
