//
//  ViewController.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import LocalAuthentication
import MaterialControls

class ViewController: UIViewController,UITextFieldDelegate {

    
    
    @IBOutlet weak var txtEmail: JVFloatLabeledTextField!
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgorPassword: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
 
    @IBOutlet weak var TouchorFaceIDLabl: UILabel!
    @IBOutlet weak var BtnTouchorFaceID: UIButton!
    @IBOutlet weak var TouchIDVOew: UIView!
    @IBOutlet weak var ViewPasswordBtn: UIButton!
    
    let txtFieldSetting = textFieldSetting()
    let round = RoundedCorner()
    
    let uc = UtilitySoftTechMedia()
    //saving api response
    var loginResponse:AppUser!
    var PassView:Bool = false
    let currentType = LAContext().biometricType
    
    var Toast = MDToast()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.04705882353, blue: 0.1490196078, alpha: 1)
        self.emailView.transform = CGAffineTransform(scaleX: 0,y: 0)
        self.passwordView.transform = CGAffineTransform(scaleX: 0,y: 0)

        if (UserDefaults.standard.value(forKey: "AllCountriesList") as? String) != nil {
            
            print("Already Call Country List")
            
        }else{
            
            self.AllCountryList()
        }
        
        self.animation()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    func animation(){
        UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: .calculationModeLinear, animations: {
            //self.emailView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.emailView.transform = CGAffineTransform(scaleX: 1,y: 1)
            self.passwordView.transform = CGAffineTransform(scaleX: 1,y: 1)
           // self.passwordView.shake(count: 50, for: 1.5, withTranslation: 5)
            //self.emailView.frame.size.width += 360
            //self.emailView.center.x -= self.view.bounds.width
            //self.emailView.center.y -= self.view.bounds.height
            //self.passwordView.center.x -= self.view.bounds.width
            //self.passwordView.center.y -= self.view.bounds.height
            
        }, completion:{finished in
            
            self.view.layer.removeAllAnimations()
        })
    }
    override func viewWillAppear(_ animated: Bool) {
       
        DispatchQueue.main.async {

            guard let username = UserDefaults.standard.value(forKey: "Email") as? String else {
                return
            }

                        guard let password = UserDefaults.standard.value(forKey:"Password") as? String else {
                return
            }
            
            self.txtEmail.text = username
           self.txtPassword.text = password

        }
        
        
        if(currentType == .none)
         {
            TouchIDVOew.isHidden = true
        }
        else if(currentType == .touchID)
         {
            TouchorFaceIDLabl.text = "Fast Login With Touch"
            BtnTouchorFaceID.setImage(UIImage(named: "fingerFastPace.png"), for: .normal)
         }
        else if(currentType == .faceID)
          {
            TouchorFaceIDLabl.text = "Fast Login With Face ID"
            BtnTouchorFaceID.setImage(UIImage(named: "Face Icon.png"), for: .normal)
          }
        if (UserDefaults.standard.value(forKey: "Email") as? String == nil)
        {
            TouchIDVOew.isHidden = true
        }
        else
        {
            TouchIDVOew.isHidden = false
        }
    }
    
    
    
    
    @IBAction func TouchIdorfaceID(_ sender: UIButton) {
        if UserDefaults.standard.value(forKey: "Email") as? String != nil
        {
            self.authenticationWithTouchID()
        }
    }
    
    func authenticationWithTouchID() {
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
        {
            let reason = "Identify yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in
                
                DispatchQueue.main.async
                    {
                        if success
                        {
                            self!.LoginUser2()
                        }
                        else
                        {
                            // error
                            let ac = UIAlertController(title: "Authentication failed", message: "You could not be verified; please try again.", preferredStyle: .alert)
                            ac.addAction(UIAlertAction(title: "OK", style: .default))
                            self!.present(ac, animated: true)
                        }
                }
            }
        }
        else
        {
            // no biometry
            DispatchQueue.main.async {
                let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(ac, animated: true)
            }
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnLoginClick(_ sender: Any) {
        // forcedly crash for checking crashlytics.
//        fatalError()
        
        if(self.txtEmail.text?.isEmpty)!{

            uc.errorSuccessAler("Alert", "Email required!", self)

        }else if !(uc.isValidEmail(testStr:self.txtEmail.text!)){

            uc.errorSuccessAler("Alert", "Valid Email required!", self)

        }else if(self.txtPassword.text?.isEmpty)!{

            uc.errorSuccessAler("Alert", "Password required!", self)

        }else if ((self.txtPassword.text?.count)! < 6){

            self.uc.errorSuccessAler("Alert", "Password must have a length between 6 and 25!", self)

        }else{

            LoginUser()
        }
        
    }
    
    
    @IBAction func btnForgorPasswordClick(_ sender: Any) {
        
    }
    
    @IBAction func btnSignUpClick(_ sender: Any) {
        
    }
    
    func errorLogin(){
        UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: .calculationModeLinear, animations: {
        self.passwordView.shake(count: 5, for: 0.2, withTranslation: 5)
        self.emailView.shake(count: 5, for: 0.2, withTranslation: 5)
           }, completion:{finished in
               self.view.layer.removeAllAnimations()
           })
    }
    //getDashboard
    func LoginUser(){
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["DeviceType":"I","DeviceToken":firebaseToken,"AppID":ApiUrls.AppID,"Token":token,"Password":self.txtPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines),"Email":self.txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.LoginUser, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.LoginUser()
                return
            }
            
            self.loginResponse = AppUser(JSONString:result)
            
            if self.loginResponse != nil && self.loginResponse.myAppResult != nil {
                
                if(self.loginResponse.myAppResult?.Code == 0){
                    let NewKey = "\(self.loginResponse.AuthToken!.auth_key!)\(self.loginResponse.AuthToken!.user_id!)"
                    ApiUrls.AuthToken = NewKey.hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
                    UserDefaults.standard.set(self.txtEmail.text, forKey: "Email")
                    UserDefaults.standard.set(self.txtPassword.text, forKey: "Password")
                    self.uc.saveAuthToken(result: result)
                    self.performSegue(withIdentifier: "Dashbaord", sender: nil)
                }else{
                    
                    if(self.loginResponse == nil || self.loginResponse.myAppResult == nil){
                        self.Toast.text = "Wrong password entered."
                        self.Toast.show()
                        self.Toast.duration = 3
                        //self.uc.errorSuccessAler("Error", result, self)
                        self.errorLogin()
                    }else{
                        self.Toast.text = "Wrong password entered."
                        self.Toast.show()
                        self.Toast.duration = 3
                        //self.uc.errorSuccessAler("Error", (self.loginResponse.myAppResult?.Message)!, self)
                        self.errorLogin()
                    }
                }
                
            }else{
                
                if(self.loginResponse == nil || self.loginResponse.myAppResult == nil){
                
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (self.loginResponse.myAppResult?.Message)!, self)
                }
            }
            
        }
        
    }
    
    func LoginUser2(){
        
       guard let email = UserDefaults.standard.object(forKey: "Email") as? String else
        {
            return
        }
        guard let password = UserDefaults.standard.object(forKey: "Password") as? String else
        {
            return
        }
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["DeviceType":"I","DeviceToken":firebaseToken,"AppID":ApiUrls.AppID,"Token":token,"Password":password,"Email":email]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.LoginUser, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.LoginUser2()
                return
            }
            
            self.loginResponse = AppUser(JSONString:result)
            
            if self.loginResponse != nil && self.loginResponse.myAppResult != nil {
                
                if(self.loginResponse.myAppResult?.Code == 0){
                    let NewKey = "\(self.loginResponse.AuthToken!.auth_key!)\(self.loginResponse.AuthToken!.user_id!)"
                    ApiUrls.AuthToken = NewKey.hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
                    self.uc.saveAuthToken(result: result)
                    self.performSegue(withIdentifier: "Dashbaord", sender: nil)
                }else{
                    
                    if(self.loginResponse == nil || self.loginResponse.myAppResult == nil){
                        
                        self.uc.errorSuccessAler("Error", result, self)
                        
                    }else{
                        
                        self.uc.errorSuccessAler("Error", (self.loginResponse.myAppResult?.Message)!, self)
                    }
                }
            }else{
                
                if(self.loginResponse == nil || self.loginResponse.myAppResult == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    
                    self.uc.errorSuccessAler("Error", (self.loginResponse.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    func AllCountryList(){
        
        
        let token = "\(ApiUrls.AppID)".hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
        let parms =  ["AppID":ApiUrls.AppID,"Token":token,"Type":"3"]as [String : Any]
        
        
        uc.webServicePosthttp(urlString: ApiUrls.GetAllCountryList, params:parms , message: "Loading...", currentController: self){result in
            
            if(result == "fail")
            {
                self.AllCountryList()
                return
            }
            let AllCountiresResponse = AppUser(JSONString:result)
            
            if AllCountiresResponse?.myAppResult?.Code == 0 {
                
                UserDefaults.standard.set(result, forKey: "AllCountriesList")
                
            }else if AllCountiresResponse?.myAppResult?.Code == 101 {
                
                self.uc.logout(self)
                
            }else{
                
                if(AllCountiresResponse?.myAppResult?.Message == nil){
                    
                    self.uc.errorSuccessAler("Error", result, self)
                    
                }else{
                    self.uc.errorSuccessAler("Error", (AllCountiresResponse?.myAppResult?.Message)!, self)
                }
            }
        }
    }
    
    
    @IBAction func ChangePasswordPreferences(_ sender: UIButton) {
        
//        self.PassView = !self.PassView
//        if self.PassView {
//           let passwodImageHide = UIImage(named: "ico_view_new" + ".png")
//                      ViewPasswordBtn.setImage(passwodImageHide, for: .normal)
//                      ViewPasswordBtn.imageEdgeInsets = UIEdgeInsets(top: 6, left: 3, bottom: 6, right: 3)
//                      self.ViewPasswordBtn.layoutIfNeeded()
//                      self.ViewPasswordBtn.setNeedsDisplay()
//                      txtPassword.isSecureTextEntry = false
//        } else {
//           let passwodImageHide = UIImage(named: "ico_eye_line" + ".png")
//                      ViewPasswordBtn.setImage(passwodImageHide, for: .normal)
//                      ViewPasswordBtn.imageEdgeInsets = UIEdgeInsets(top: 6, left: 3, bottom: 6, right: 3)
//                      self.ViewPasswordBtn.layoutIfNeeded()
//                      self.ViewPasswordBtn.setNeedsDisplay()
//                      txtPassword.isSecureTextEntry = true
//        }

    }
}


extension LAContext {
    enum BiometricType: String {
        case none
        case touchID
        case faceID
    }
    
    var biometricType: BiometricType {
        var error: NSError?
        
        guard self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            // Capture these recoverable error thru Crashlytics
            return .none
        }
        
        if #available(iOS 11.0, *) {
            switch self.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            @unknown default:
                return .none
            }
        } else {
            return  self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchID : .none
        }
    }
}
public extension UIView {

    func shake(count : Float = 4,for duration : TimeInterval = 0.5,withTranslation translation : Float = 5) {

        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.values = [translation, -translation]
        layer.add(animation, forKey: "shake")
    }
}
