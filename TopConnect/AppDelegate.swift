//
//  AppDelegate.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 19/12/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import DropDown
import IQKeyboardManagerSwift
import Firebase
import FirebaseCore
import FirebaseMessaging
import UserNotifications
import Fabric
import Crashlytics

var firebaseToken = String()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate{
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        DropDown.startListeningToKeyboard()
        registerForPushNotifications()
        Fabric.with([Crashlytics.self])
        
        if let authToken = UserDefaults.standard.value(forKey: "AuthToken") as? String, !authToken.isEmpty {
            
            let isLoginEnable = UserDefaults.standard.bool(forKey: "isLoginEnable")
            
            if isLoginEnable {
                //Proceed to Dashboard
                let uc = UtilitySoftTechMedia()
                //ApiUrls.AuthToken = uc.getAuthToken()?.AuthToken
                let NewKey = "\(String(describing: uc.getAuthToken()!.AuthToken!.auth_key!))\(String(describing: uc.getAuthToken()!.AuthToken!.user_id!))"
                
                //let NewKey = "\(self.loginResponse.AuthToken!.auth_key!)\(self.loginResponse.AuthToken!.user_id!)"
                ApiUrls.AuthToken = NewKey.hmac(algorithm: .sha256, key: ApiUrls.PublicKey)
                
                
                
                let storyb = UIStoryboard(name: "Main", bundle: nil)
                let nav = storyb.instantiateViewController(withIdentifier: "Start") as! UINavigationController
                let dashBoardVC = storyb.instantiateViewController(withIdentifier: "DashBoardID")
                nav.viewControllers = [dashBoardVC]
                nav.navigationBar.isHidden = true
                //let navVC = UINavigationController(rootViewController: )
                //let navVC = UINavigationController(rootViewController: DashboardViewController())
                
                //navVC.navigationBar.isHidden = true
                
                self.window?.rootViewController = nav
//                self.performSegue(withIdentifier: "Dashbaord", sender: nil)
                //                self.navigationController.modalPresentationStyle = .fullScreen
                //                self.navigationController.pushViewController(DashboardViewController(), animated: true)
            }
        }
        
        return true
    }
    
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
        GetPushToken()
    }
    func GetPushToken() {
        if let token = Messaging.messaging().fcmToken {
            firebaseToken = token
            print(firebaseToken)
        }
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
        
        if let ComplaintPush = userInfo["key_1"] as? String {
            if(ComplaintPush == "message")
            {
                IsComplaintRead = false
                NotificationCenter.default.post(name: ApiUrls.ComplaintNotification, object: nil, userInfo: nil)
            }
        }
        
        switch UIApplication.shared.applicationState {
        case .active:
            //app is currently active, can update badges count here
            
            break
        case .inactive:
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            UIApplication.shared.applicationIconBadgeNumber = 0
            break
        case .background:
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            break
        default:
            break
        }
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        GetPushToken()
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        
        guard var Badge = UserDefaults.standard.value(forKey: "Badge") as? Int else {
            UserDefaults.standard.set(UIApplication.shared.applicationIconBadgeNumber, forKey: "Badge")
            return
        }
        Badge += 1
        UserDefaults.standard.set(Badge, forKey: "Badge")
        UIApplication.shared.applicationIconBadgeNumber = Badge
        if let ComplaintPush = userInfo["key_1"] as? String {
            if(ComplaintPush == "message")
            {
                IsComplaintRead = false
                NotificationCenter.default.post(name: ApiUrls.ComplaintNotification, object: nil, userInfo: nil)
                completionHandler([.sound])
            }
        }
        else
        {
            completionHandler([.alert,.badge,.sound])
        }
        
    }
    
    @available(iOS 10.0, *)
    private func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("[UserNotificationCenter] applicationState: \(applicationStateString) didReceiveResponse: \(userInfo)")
        guard var Badge = UserDefaults.standard.value(forKey: "Badge") as? Int else {
            UserDefaults.standard.set(UIApplication.shared.applicationIconBadgeNumber, forKey: "Badge")
            return
        }
        Badge += 1
        UserDefaults.standard.set(Badge, forKey: "Badge")
        UIApplication.shared.applicationIconBadgeNumber = Badge
        
        //TODO: Handle background notification
        completionHandler([.alert,.badge,.sound])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        UIApplication.shared.applicationIconBadgeNumber = 0
        UserDefaults.standard.set(0, forKey: "Badge")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        UserDefaults.standard.set(0, forKey: "Badge")
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
        UserDefaults.standard.set(0, forKey: "Badge")
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.set(0, forKey: "Badge")
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    
}
