//
//  ApiUrls.swift
//  MTGlobal
//
//  Created by Softtech Media on 19/07/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import Foundation

class ApiUrls: NSObject {
    
        static let ClientId                              =        "abc132xyz"
        static let client_secret                         =        "secret123"
        static let grant_type                            =        "password"
        static let serverUrl                             =        "http://api.remitcontinental.co.uk/api/"
        static let AppID                                 =        "6ca8c12f-2eb9-42cd-a99a-b6378a4d9608"
        static let PublicKey                             =        "fOO1K5zjtJ58WT49zEazrth.leYDYfrP3dPv.pIWXKg~"
        static let DomianID                              =        0
        static var AuthToken                             =        ""
        static let DeviceType                            =        "IOS"
        static let AuthKey                               =        ""
        static let RegisterUser                          =        "users/register"
        static let LoginUser                             =        "users/login"
        static let ForgetPassword                        =        "users/forgotpassword"
        static let GetDashbaord                          =        "users/getdashboard"
        static let ReceivingCountryList                  =        "aceUtils/GetReceivingCountryList"
        static let SendingCountryList                    =        "aceUtils/GetSendingCountryList"
        static let GetCountryPayerList                   =        "utils/payerlist"
        static let GetExchangeRate                       =        "aceUtils/GetExchangeRate"
        static let GetPublicExchangeRate                 =        "utils/publicexchangerate"
        static let GetAllCountryList                     =        "utils/countrylist"
        static let GetProfile                            =        "users/profile"
        static let UpdateProfile                         =        "users/updateprofile"
        static let GetTransactionList                    =        "trans/translist"
        static let GetRecipientList                      =        "benes/benelist"
        static let AddRecipient                          =        "benes/createbene"
        static let UpdateRecipient                       =        "benes/updatebene"
        static let CountryBankList                       =        "utils/countrybanklist"
        static let GetServiceCharges                     =        "utils/servicecharges"
        static let getDocumentList                       =        "docs/doclist"
        static let AddDocumentList                       =        "docs/createdoc"
        static let UpdateDocument                        =        "docs/updatedoc"
        static let InsertTransaction                     =        "trans/createtrans"
        static let UpdateTransaction                     =        "trans/update"
        static let GetLists                              =        "utils/lists"
        static let AllowPaymentMethod                    =        "utils/allowedpayments"
        static let UpdateProfilePicture                  =        "users/updatephoto"
        static let PostCodeFinder                        =        "postcode/finder"
        static let CreateComplaince                      =        "trans/createcomplaint"
        static let getComplaince                         =        "trans/getcomplaints"
        static let SendEmail                             =        "trans/sendemail"
        static let changepassword                        =        "users/changepassword"
        static let sendingmethods                        =        "utils/sendingmethods"
        static let Canceltransaction                     =        "trans/canceltrans"
        static let payinlocationlist                     =        "utils/payinlocationlist"
        static let GetPublicServiceCharges               =        "utils/publicservicecharges"
    
    
        static let SendOTP                               =        "users/sendotp"
        static let VerifyOTP                             =        "users/verifyotp"

    
        static let DepositBankList                       =        "utils/depositbanklist"
    
        static let InsertComplaint                       =        "complaint/insertcomplaint"
        static let GetComplaintlist                      =        "complaint/getcomplaintlist"
        static let InsertComplaintMsg                    =        "complaint/insertcomplaintlog"
        static let GetComplaintMsglist                   =        "complaint/getcomplaintloglist"
    
        static let NotificationName                      =        "ChooseCountry"
        static let NotificationPayerList                 =        "ChoosePayer"
        static let NotificationPayerBranchList           =        "ChoosePayerBranch"
        
        static let kNotification = Notification.Name("kNotification")
        static let PNotification = Notification.Name("PNotification")
        static let PostCodeNotification = Notification.Name("PostCodeNotification")
        static let ComplaintNotification = Notification.Name("CommplaintNotification")
        static let ShowDashboardNotification = Notification.Name("ShowDashboardNotification")
        static let FromDashboardNotification = Notification.Name("FromDashboardNotification")
        
        static let getPayerBranchList                    =         "utils/payerbranchlist"
    
        static var TotalDoc                              =          0
        static var paymentMethodOptions                  =   "utils/paymentmethodoptions"
        static var DeleteRecipient                       =   "benes/deletebene"
        static let pdfNotification = Notification.Name("pdfNotification")
        
        
  
    
}
