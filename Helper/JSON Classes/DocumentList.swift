//
//  DocumentList.swift
//  MTGlobal
//
//  Created by Softtech Media on 09/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//


import UIKit
import ObjectMapper
class DocumentList: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var AceDocList                         :     [DocumentListResult]?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                                   <-      map["result"]
        AceDocList                                    <-      map["data"]
        
        
    }
}

