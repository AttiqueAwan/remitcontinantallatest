//
//  AppResult.swift
//  MTGlobal
//
//  Created by Softtech Media on 20/07/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import
ObjectMapper
class AppResult: Mappable {
    
    var Code: Int?
    var Description: String?
    var Message: String?
    var Rflag: Int?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        Code                <- map["Code"]
        Description         <- map["Description"]
        Message             <- map["Message"]
        Rflag               <- map["Rflag"]
        
    }
    
}
