//
//  DashboardDetail.swift
//  MTGlobal
//
//  Created by Softtech Media on 27/07/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class DashboardDetail: Mappable {
    
    var TotalTransections              :     Int!
    var CompletedTransections          :     Int!
    var InCompletedTransections        :     Int!
    var TotalIDoc                      :     Int!
    var TotalBene                      :     Int!
    var TotalAmount                    :     Double!
    var TotalUnreadMessages            :     Int!
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        TotalTransections              <-   map["TotalTransections"]
        CompletedTransections          <-   map["CompletedTransections"]
        InCompletedTransections        <-   map["InCompletedTransections"]
        TotalIDoc                      <-   map["TotalIDoc"]
        TotalBene                      <-   map["TotalBene"]
        TotalAmount                    <-   map["TotalAmount"]
        TotalUnreadMessages            <-   map["TotalUnreadMessages"]
        
        
    }
    
}
