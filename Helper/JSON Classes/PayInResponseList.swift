//
//  PayInResponseList.swift
//  AyanExpress
//
//  Created by Softech Media on 08/04/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class PayInResponseList: Mappable {
    
    
    var LocationId                      :  Int?
    var Phone                           :  String?
    var Address                         :  String?
    var CellPhone                       :  String?
    var BusinessName                    :  String?
    var LocationName                    :  String?
    var Email                           :  String?
    var Country                         :  String?
    var HouseNo                         :  String?
    var PostCode                        :  String?
    var City                            :  String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        LocationId                          <- map["LocationId"]
        Phone                               <- map["Phone"]
        Address                             <- map["Address"]
        CellPhone                           <- map["CellPhone"]
        BusinessName                        <- map["BusinessName"]
        LocationName                        <- map["LocationName"]
        Email                               <- map["Email"]
        Country                             <- map["Country"]
        HouseNo                             <- map["HouseNo"]
        PostCode                            <- map["PostCode"]
        City                                <- map["City"]
    }
}
