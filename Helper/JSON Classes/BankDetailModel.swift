//
//  BankDetailModel.swift
//  UKAsiaRemitt
//
//  Created by Mac on 02/08/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class BankDetailModel: Mappable {
    
    var BankID                        : Int?
    var BankName                      : String?
    var AccountTitle                  : String?
    var AccountNumber                 : String?
    var IBAN                          : String?
    var SwiftCode                     : String?
    var SortCode                      : String?
   
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        BankID                        <- map["BankID"]
        BankName                      <- map["BankName"]
        AccountTitle                  <- map["AccountTitle"]
        AccountNumber                 <- map["AccountNumber"]
        IBAN                          <- map["IBAN"]
        SwiftCode                     <- map["SwiftCode"]
        SortCode                      <- map["SortCode"]
    }
    
}
