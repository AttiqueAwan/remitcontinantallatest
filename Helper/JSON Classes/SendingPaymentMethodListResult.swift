//
//  SendingPaymentMethodListResult.swift
//  AyanExpress
//
//  Created by Softech Media on 20/03/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class SendingPaymentMethodListResult: Mappable {
    
    
    var myAppResult                 :     AppResult?
    var PaymentMethodList           :     [SendingPaymentMethodList]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        myAppResult                 <-      map["result"]
        PaymentMethodList           <-      map["data"]
        
    }
}

