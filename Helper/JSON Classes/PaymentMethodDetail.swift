//
//  PaymentMethodDetail.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 07/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class PaymentMethodDetail: Mappable {
    
    
    var PaymentMethodID              :     Int?
    var PaymentMethodName            :     String?
    var PaymentMethodCode            :     String?
    var PaymentTypeId                :     String?
    var PaymentCompanyName           :     String?
   
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
       PaymentMethodID       <-      map["PaymentMethodID"]
       PaymentMethodName     <-      map["PaymentMethodName"]
       PaymentMethodCode     <-      map["PaymentMethodCode"]
       PaymentTypeId         <-      map["MobilePaymentTypeId"]
       PaymentCompanyName    <-      map["MobileCompanyName"]
        
    }
}
