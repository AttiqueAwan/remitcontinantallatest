//
//  RecipientList.swift
//  MTGlobal
//
//  Created by Softtech Media on 07/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class RecipientList: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var AceBeneList                        :     [RecipientListResult]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        AceBeneList                            <-      map["data"]
        
    }
}
