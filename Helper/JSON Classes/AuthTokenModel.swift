//
//  AuthTokenModel.swift
//  MTGlobal
//
//  Created by Softtech Media on 28/09/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class AuthTokenModel: Mappable {
    
    var auth_key             :     String?
    var auth_token           :     String?
    var expire_on            :     String?
    var issue_on             :     String?
    var user_id              :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        auth_key                <-      map["auth_key"]
        auth_token              <-      map["auth_token"]
        expire_on               <-      map["expire_on"]
        issue_on                <-      map["issue_on"]
        user_id                 <-      map["user_id"]
    }
}

