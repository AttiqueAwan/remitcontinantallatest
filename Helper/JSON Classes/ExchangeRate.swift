//
//  ExchangeRate.swift
//  MTGlobal
//
//  Created by Softtech Media on 03/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class ExchangeRate: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var AceExchangeRateResponce            :     [ExchangeRateDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        AceExchangeRateResponce                <-      map["data"]
        
    }
}

