//
//  BankList.swift
//  MTGlobal
//
//  Created by Softtech Media on 08/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class BankList: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var AceCountryBankList                 :     [BankResult]?
    var AceChargesResponce                 :     BankResult?
    var AcePayerBranchList                 :     [BankResult]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                                   <-      map["result"]
        AceCountryBankList                            <-      map["data"]
        AceChargesResponce                            <-      map["data"]
        AcePayerBranchList                            <-      map["data"]
        
        
    }
}

