//
//  DocumentListResult.swift
//  MTGlobal
//
//  Created by Softtech Media on 09/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class DocumentListResult: Mappable {
    
    var DocID                              : Int?
    var DocType                            : String?
    var DocNumber                          : String?
    var DocIssueDate                       : String?
    var DocExpireDate                      : String?
    var DocAddedDate                       : String?
    var DocFileUrl                         : String?
    var DocFileBackUrl                     : String?
    var DocBody                            : String?
    var DocBodyBack                        : String?
    var DocTable                           : String?
    var DocTypeOther                       : String?
    var DocTypeName                        : String?
    var IssuerCountryIsoCode               : String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        DocID                                 <- map["DocID"]
        DocType                               <- map["DocType"]
        DocNumber                             <- map["DocNumber"]
        DocIssueDate                          <- map["DocIssueDate"]
        DocExpireDate                         <- map["DocExpireDate"]
        DocAddedDate                          <- map["DocAddedDate"]
        DocFileUrl                            <- map["DocBodyURL"]
        DocFileBackUrl                        <- map["DocBodyBackURL"]
        DocBody                               <- map["DocBody"]
        DocBodyBack                           <- map["DocBodyBack"]
        DocTable                              <- map["DocTable"]
        DocTypeOther                          <- map["DocTypeOther"]
        DocTypeName                           <- map["DocTypeName"]
        IssuerCountryIsoCode                  <- map["IssuerCountryIsoCode"]
        
    }
    
}

