//
//  CancelRequestResponse.swift
//  AyanExpress
//
//  Created by Softech Media on 08/04/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class CancelRequestResponse: Mappable {
    
    
    var myAppResult             :     AppResult?
    var data                    :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        data                        <-      map["data"]
        
        
    }
}
