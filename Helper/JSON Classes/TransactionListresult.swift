//
//  TransactionListresult.swift
//  MTGlobal
//
//  Created by Softtech Media on 06/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class TransactionListresult: Mappable {
    
    var PaymentID                             : Int?
    var PaymentNumber                         : String?
    var PaymentMethod                         : String?
    var SendingPaymentMethod                  : String?
    var PaymentDate                           : String?
    var SendingCountry                        : String?
    var SendingCountryIso3Code                : String?
    var ReceivingCountry                      : String?
    var ReceivingCountryIso3Code              : String?
    var SendingCurrency                       : String?
    var ReceivingCurrency                     : String?
    var PayInAmount                           : String?
    var PayOutAmount                          : String?
    var ServiceCharges                        : String?
    var ExchangeRate                          : String?
    var SenderName                            : String?
    var BeneName                              : String?
    var BenePhone                             : String?
    var PaymentStatus                         : String?
    var PayerID                               : String?
    var PayerName                             : String?
    var PayerBranchCode                       : String?
    var BeneID                                : Int?
    var PayoutBranchCode                      : String?
    var PayoutBranchName                      : String?
    var PayoutBranchAddress                   : String?
    var SendingReason                         : String?
    var ReceiverBankAccountNumber             : String?
    var ReceiverBankAccountTitle              : String?
    var ReceiverBankIBAN                      : String?
    var ReceiverBankCode                      : String?
    var ReceiverBankName                      : String?
    var ReceiverBranchName                    : String?
    var ReceiverBankBranchCode                : String?
    var MobileAppPayment                      : String?
    var PaymentPageURL                        : String?
    var PaymentTime                           : String?
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
       PaymentID                              <- map["PaymentID"]
       PaymentNumber                          <- map["PaymentNumber"]
       PaymentMethod                          <- map["PaymentMethod"]
       SendingPaymentMethod                   <- map["SendingPaymentMethod"]
       PaymentDate                            <- map["PaymentDate"]
       PaymentTime                            <- map["PaymentTime"]
       SendingCountry                         <- map["SendingCountry"]
       SendingCountryIso3Code                 <- map["SendingCountryIso3Code"]
       ReceivingCountry                       <- map["ReceivingCountry"]
       ReceivingCountryIso3Code               <- map["ReceivingCountryIso3Code"]
       SendingCurrency                        <- map["SendingCurrency"]
       ReceivingCurrency                      <- map["ReceivingCurrency"]
       PayInAmount                            <- map["PayInAmount"]
       PayOutAmount                           <- map["PayOutAmount"]
       ServiceCharges                         <- map["ServiceCharges"]
       ExchangeRate                           <- map["ExchangeRate"]
       SenderName                             <- map["SenderName"]
       BeneName                               <- map["BeneName"]
       BenePhone                              <- map["BenePhone"]
       PaymentStatus                          <- map["PaymentStatus"]
       PayerID                                <- map["PayerID"]
       PayerName                              <- map["PayerName"]
       PayerBranchCode                        <- map["PayerBranchCode"]
       BeneID                                 <- map["BeneID"]
       PayoutBranchCode                       <- map["PayoutBranchCode"]
       PayoutBranchName                       <- map["PayoutBranchName"]
       PayoutBranchAddress                    <- map["PayoutBranchAddress"]
       SendingReason                          <- map["SendingReason"]
       ReceiverBankAccountNumber              <- map["ReceiverBankAccountNumber"]
       ReceiverBankAccountTitle               <- map["ReceiverBankAccountTitle"]
       ReceiverBankIBAN                       <- map["ReceiverBankIBAN"]
       ReceiverBankCode                       <- map["ReceiverBankCode"]
       ReceiverBankName                       <- map["ReceiverBankName"]
       ReceiverBranchName                     <- map["ReceiverBranchName"]
       ReceiverBankBranchCode                 <- map["ReceiverBankBranchCode"]
       MobileAppPayment                       <- map["MobileAppPayment"]
       PaymentPageURL                         <- map["PaymentPageURL"]
        
    }
    
}
