//
//  CountryListResult.swift
//  MTGlobal
//
//  Created by Softtech Media on 24/07/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class CountryListResult: Mappable {
    
    
    var myAppResult                   :     AppResult?
    var AceCountryList                :     [CountryList]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        AceCountryList              <-      map["data"]
        
    }
}
