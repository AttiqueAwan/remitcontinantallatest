//
//  AddRecipientResponce.swift
//  TopConnect
//
//  Created by Apple on 20/05/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class AddRecipientResponce: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var Recipient                        :     RecipientListResult?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        Recipient                            <-      map["data"]
        
    }
}
