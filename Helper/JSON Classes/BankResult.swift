//
//  BankResult.swift
//  MTGlobal
//
//  Created by Softtech Media on 08/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class BankResult: Mappable {
    
    
    var BankName                        :     String?
    var BankCode                        :     String?
    var Charges                         :     Int?
    var BranchCode                      :     Int?
    var BranchAddress                   :     String?
    var BranchPayCode                   :     String?
    var BranhCity                       :     String?
    var ReceivingCountryIsoCode         :     String?
    var ReceivingCurrencyIsoCode        :     String?
    var BranchName                      :     String?
    var ChargesType                     :     Int?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        BankName                            <-      map["BankName"]
        BankCode                            <-      map["BankCode"]
        BranchName                          <-      map["BranchName"]
        Charges                             <-      map["Charges"]
        BranchCode                          <-      map["BranchCode"]
        BranchAddress                       <-      map["BranchAddress"]
        BranchPayCode                       <-      map["BranchPayCode"]
        BranhCity                           <-      map["BranhCity"]
        ReceivingCountryIsoCode             <-      map["ReceivingCountryIsoCode"]
        ReceivingCurrencyIsoCode            <-      map["ReceivingCurrencyIsoCode"]
        ChargesType                         <-      map["ChargesType"]
    }
}

