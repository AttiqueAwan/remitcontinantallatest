//
//  UserLoginInfo.swift
//  MTGlobal
//
//  Created by Softtech Media on 20/07/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class UserloginInfo: Mappable {
    
    var ID                    : Int?
    var FullName              : String?
    var FirstName             : String?
    var MiddleName             : String?
    var LastName             : String?
    var Email                 : String?
    var Phone                 : String?
    var DOB                   : String?
    var BirthDay              : Int?
    var BirthMonth            : Int?
    var BirthYear             : Int?
    var Gender                : String?
    var CountryIsoCode        : String?
    var CurrencyIsoCode       : String?
    var NationalityIsoCode    : String?
    var CountryOfBirthIsoCode : String?
    var HouseNo               : String?
    var Address               : String?
    var City                  : String?
    var Occupation            : String?
    var Employer              : String?
    var PostalCode            : String?
    var CustomerID            : Int?
    var DeviceType            : String?
    var ReferralCode          : String?
    var AgentPromoCode        : String?
    var Photo                 : String?
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        ID                           <- map["ID"]
        FullName                     <- map["FullName"]
        FirstName                    <- map["FirstName"]
        MiddleName                   <- map["MiddleName"]
        LastName                     <- map["LastName"]
        Email                        <- map["Email"]
        Phone                        <- map["Phone"]
        DOB                          <- map["DOB"]
        BirthDay                     <- map["BirthDay"]
        BirthMonth                   <- map["BirthMonth"]
        BirthYear                    <- map["BirthYear"]
        Gender                       <- map["Gender"]
        CountryIsoCode               <- map["CountryIsoCode"]
        CurrencyIsoCode              <- map["CurrencyIsoCode"]
        NationalityIsoCode           <- map["NationalityIsoCode"]
        CountryOfBirthIsoCode        <- map["CountryOfBirthIsoCode"]
        HouseNo                      <- map["HouseNo"]
        Address                      <- map["Address"]
        City                         <- map["City"]
        Occupation                   <- map["Occupation"]
        Employer                     <- map["Employer"]
        PostalCode                   <- map["PostalCode"]
        CustomerID                   <- map["CustomerID"]
        DeviceType                   <- map["DeviceType"]
        Address                      <- map["Address"]
        ReferralCode                 <- map["ReferralCode"]
        AgentPromoCode               <- map["AgentPromoCode"]
        Photo                        <- map["Photo"]
        
    }
}

