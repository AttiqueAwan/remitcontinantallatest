//
//  RecipientListResult.swift
//  MTGlobal
//
//  Created by Softtech Media on 07/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class RecipientListResult: Mappable {
    
    var BeneID                              : Int?
    var BeneName                            : String?
    var BeneAddress                         : String?
    var BeneCity                            : String?
    var BenePostCode                        : String?
    var BeneCountryIsoCode                  : String?
    var BenePhone                           : String?
    var BeneBankName                        : String?
    var BeneBranchName                      : String?
    var BeneAccountNumber                   : String?
    var BeneIBAN                            : String?
    var BeneBankCode                        : String?
    var BeneBankBranchCode                  : String?
    var BeneRelationWithSender              : String?
    var BenePayMethod                       : Int?
    var CountryName                         : String?
    var BeneFirstName                       : String?
    var BeneLastName                        : String?
    var MobileCompanyName                   : String?
    var PrepaidCardNumber                   : String?

    
    
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        BeneID                                     <- map["BeneID"]
        BeneName                                   <- map["BeneName"]
        BeneAddress                                <- map["BeneAddress"]
        BeneCity                                   <- map["BeneCity"]
        BenePostCode                               <- map["BenePostCode"]
        BeneCountryIsoCode                         <- map["BeneCountryIsoCode"]
        BenePhone                                  <- map["BenePhone"]
        BeneBankName                               <- map["BeneBankName"]
        BeneBranchName                             <- map["BeneBranchName"]
        BeneAccountNumber                          <- map["BeneAccountNumber"]
        BeneIBAN                                   <- map["BeneIBAN"]
        BeneBankCode                               <- map["BeneBankCode"]
        BeneBankBranchCode                         <- map["BeneBankBranchCode"]
        BeneRelationWithSender                     <- map["BeneRelationWithSender"]
        BenePayMethod                              <- map["BenePayMethod"]
        CountryName                                <- map["CountryName"]
        BeneFirstName                              <- map["BeneFirstName"]
        BeneLastName                               <- map["BeneLastName"]
        MobileCompanyName                          <- map["MobileCompanyName"]
        PrepaidCardNumber                          <- map["PrepaidCardNumber"]
    
    }
    
}
