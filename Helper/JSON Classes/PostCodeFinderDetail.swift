//
//  PostCodeFinderDetail.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 17/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class PostCodeFinderDetail: Mappable {
    
    
    var ResultId                        :     Int?
    var HouseNo                         :     Int?
    var Address                         :     String?
    var City                            :     String?
    var PostCode                        :     String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        ResultId                               <-      map["ResultId"]
        HouseNo                                <-      map["HouseNo"]
        Address                                <-      map["Address"]
        City                                   <-      map["City"]
        PostCode                               <-      map["PostCode"]
            
        
        
        
    }
}

