//
//  SendingPaymentMethodList.swift
//  AyanExpress
//
//  Created by Softech Media on 20/03/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class SendingPaymentMethodList: Mappable {
    
    
    var PaymentMethodID                 :  Int?
    var PaymentMethodName               :  String?
    var PaymentMethodCode               :  String?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        PaymentMethodID                     <- map["PaymentMethodID"]
        PaymentMethodName                   <- map["PaymentMethodName"]
        PaymentMethodCode                   <- map["PaymentMethodCode"]

    }
}
