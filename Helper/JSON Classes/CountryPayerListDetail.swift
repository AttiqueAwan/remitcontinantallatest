//
//  CountryPayerLIstDetail.swift
//  MTGlobal
//
//  Created by Softtech Media on 03/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class CountryPayerListDetail: Mappable {
    
    var PayerId                         : Int?
    var PayerName                       : String?
    var CurrencyIsoCode                 : String?
    var PaymentMethod                   : String?
    var PaymentNumberTotalDigits        : String?
    var PaymentNumberPrefix             : String?
    var MinimumAmount                   : Double?
    var MaximumAmount                   : Double?
    var ExchangeRate                    : Double?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
    PayerId                         <- map["PayerId"]
    PayerName                       <- map["PayerName"]
    CurrencyIsoCode                 <- map["CurrencyIsoCode"]
    PaymentMethod                   <- map["PaymentMethod"]
    PaymentNumberTotalDigits        <- map["PaymentNumberTotalDigits"]
    PaymentNumberPrefix             <- map["PaymentNumberPrefix"]
    MinimumAmount                   <- map["MinimumAmount"]
    MaximumAmount                   <- map["MaximumAmount"]
    ExchangeRate                    <- map["ExchangeRate"]
        
    }
    
}

