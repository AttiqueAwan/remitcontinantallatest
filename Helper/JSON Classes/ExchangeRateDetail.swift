//
//  ExchangeRateDetail.swift
//  MTGlobal
//
//  Created by Softtech Media on 03/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper

class ExchangeRateDetail: Mappable {
    
    var ExchangeRate                         : Double?
    var RecievingCountryName                 : String?
    var RecievingCurrencyyISOCode            : String?
    var SendingCountryName                   : String?
    var SendingCurrencyISOCode               : String?
    var SendingCountryISOCode                : String?
    var PayerID                              : Int?
    var PayerName                            : String?
    var CreationDate                         : String?
    
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        ExchangeRate                         <- map["ExchangeRate"]
        RecievingCountryName                 <- map["RecievingCountryName"]
        RecievingCurrencyyISOCode            <- map["RecievingCurrencyyISOCode"]
        SendingCountryName                   <- map["SendingCountryName"]
        SendingCurrencyISOCode               <- map["SendingCurrencyISOCode"]
        SendingCountryISOCode                <- map["SendingCountryISOCode"]
        PayerID                              <- map["PayerID"]
        PayerName                            <- map["PayerName"]
        CreationDate                         <- map["CreationDate"]
        
        
    }
    
}

