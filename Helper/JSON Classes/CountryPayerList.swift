//
//  CountryPayerList.swift
//  MTGlobal
//
//  Created by Softtech Media on 03/08/2018.
//  Copyright © 2018 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class CountryPayerList: Mappable {
    
    
    var myAppResult             :     AppResult?
    var AcePayerList            :     [CountryPayerListDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                 <-      map["result"]
        AcePayerList                <-      map["data"]
        
    }
}
