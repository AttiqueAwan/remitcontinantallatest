//
//  BankDetailResponse.swift
//  UKAsiaRemitt
//
//  Created by Mac on 02/08/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//


import Foundation
import UIKit
import ObjectMapper
class BankDetailResponse: Mappable {
    
    
    var myAppResult                            :     AppResult?
    var BankDetail                             :     [BankDetailModel]?
   
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        BankDetail                             <-      map["data"]
        
    }
}
