//
//  PaymentMethodresult.swift
//  UKAsiaRemitt
//
//  Created by Softtech Media on 07/01/2019.
//  Copyright © 2019 Softtech Media. All rights reserved.
//

import UIKit
import ObjectMapper
class PaymentMethodresult: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var PaymentMethodList                  :     [PaymentMethodDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        PaymentMethodList                      <-      map["data"]
        
    }
}

class PaymentMethodOptionsResult: Mappable {
    
    
    var myAppResult                        :     AppResult?
    var PaymentMethodOptionsList           :     [PaymentMethodDetail]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        
        myAppResult                            <-      map["result"]
        PaymentMethodOptionsList               <-      map["data"]
        
    }
}

