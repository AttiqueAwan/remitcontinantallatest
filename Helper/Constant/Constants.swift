//
//  Constants.swift
//  TopConnect
//
//  Created by Soft Tech Media on 18/02/2020.
//  Copyright © 2020 Softtech Media. All rights reserved.
//

import Foundation

class Constants :NSObject{
    
    static let PASSPORT_ID : String = "1"
    static let DRIVING_LIC_ID : String = "2"
    static let NATIONAL_CARD_ID : String = "3"
    static let RESIDENT_CARD_ID : String = "91"
    static let BANK_STATEMENT_ID : String = "171"
    static let UTILITY_BILLS_ID : String = "172"
    static let CREDIT_CARD_ID : String = "173"
    static let OTHERS_ID : String = "4"
}
