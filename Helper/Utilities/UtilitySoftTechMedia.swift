//
//  UtilitySoftTechMedia.swift
//  UtilitySofttechMedia
//
//  Created by Softech Media on 15/01/2018.
//  Copyright © 2018 Softtechmedia. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import MaterialControls

public typealias SelectionClosure = (DataResponse<Any>) -> Void
class UtilitySoftTechMedia: NSObject {

   public var webserviceResponse: SelectionClosure?
    
    func AspectRatio(view:UIView)->Void{
        
        for vi in view.subviews {
         
            
            var  x:CGFloat = 0.0,y:CGFloat = 0.0,w:CGFloat = 0.0, h:CGFloat = 0.0
            if vi.tag == 0 {
                x = getWidth(width: vi.frame.origin.x)
                y = getHeight(width: vi.frame.origin.y)
                w = getWidth(width: vi.frame.size.width)
                h = getHeight(width: vi.frame.size.height)
            }
                
            else if(vi.tag==1) //without height
                
            {
                x = getWidth(width: vi.frame.origin.x)
                y = getHeight(width: vi.frame.origin.y)
                w = getWidth(width: vi.frame.size.width)
                h = vi.frame.size.height
                
            }
                
            else if (vi.tag==2)//without width
            {
                x = getWidth(width: vi.frame.origin.x)
                y = getHeight(width: vi.frame.origin.y)
                w = vi.frame.size.width
                h = getHeight(width: vi.frame.size.height)
                
                
                
                
                
            }
                
            else if(vi.tag==3)//without y orgin move
            {
                
                x = getWidth(width: vi.frame.origin.x)
                y =  vi.frame.origin.y
                w = getWidth(width: vi.frame.size.width)
                h = getHeight(width: vi.frame.size.height)
            }
            else if(vi.tag==4)//without y orgin expandable height
            {
                
                x = getWidth(width: vi.frame.origin.x)
                y =  vi.frame.origin.y
                w = getWidth(width: vi.frame.size.width)
                h = getHeight(width:vi.frame.size.height)+getHeight(width:vi.frame.origin.y)
            }
                
            else if(vi.tag==5){
                
                x = getWidth(width: vi.frame.origin.x)
                y = view.frame.size.height-getHeight(width:vi.frame.size.height)
                w = getWidth(width:vi.frame.size.width)
                h = getHeight(width: vi.frame.size.height)
                
            }
            
            vi.frame = CGRect(x:x,y:y,width:w,height:h)
                
                
        
            
            if  let s = vi as? UIButton{
                s.titleLabel?.font = UIFont(name:(s.titleLabel?.font.fontName)!, size:self.SetFontSize(font: (s.titleLabel?.font.pointSize)!))

            }
                
            else if let s = vi as? UITextField{
                s.font = UIFont(name:(s.font?.fontName)!, size: self.SetFontSize(font:(s.font?.pointSize)!))

            }
                
            else if let s = vi as? UILabel{
                s.font = UIFont(name:s.font.fontName, size:self.SetFontSize(font:s.font.pointSize))
              
                
            }
                
            else if let s = vi as? UITextView {
              
                s.font = UIFont(name:(s.font?.fontName)!, size:self.SetFontSize(font:(s.font?.pointSize)!))
               
                
            }
            
        }
        
    }
    func getWidth(width:CGFloat)->CGFloat{
        
        
        if UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiom.pad{
        
        
        return (width/768.0)*UIScreen.main.bounds.size.width
        }
        else{
        return (width/320.0)*UIScreen.main.bounds.size.width
        }
    }
    func getHeight(width:CGFloat)->CGFloat{
        
        
        if UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiom.pad{
            
            
            return (width/1025.0)*UIScreen.main.bounds.size.height
        }
        else{
            return (width/568.0)*UIScreen.main.bounds.size.height
        }
    }
    func SetFontSize(font:CGFloat)->CGFloat{
        var font1: CGFloat = font
        if UI_USER_INTERFACE_IDIOM() ==  UIUserInterfaceIdiom.pad{
            
           font1 = font+((font/100)*1)+(UIScreen.main.bounds.size.width/100)*0.1;
          
            
        }
            
        else if(UIScreen.main.bounds.size.height>568){
            
            font1 = font+((font/100)*10)+(UIScreen.main.bounds.size.width/100)*0.5;
            
            
        }
        return font1;
    }
    
    func saveLogininfo(result:String){
        
        UserDefaults.standard.set(result, forKey: "UserLogin")
    }
    
//    func getLoginInfo()->AppUser{
//        let result:String = UserDefaults.standard.value(forKey: "UserLogin") as! String
//        return AppUser(JSONString:result)!
//
//    }
//    func getUserInfo()->subAppUser{
//         let result:String = UserDefaults.standard.value(forKey: "UserLogin") as! String
//
//        return (AppUser(JSONString:result)?.myAppUser!)!
//
//    }
    func makeAlert(title:String,message:String,button:Array<Any>, delegate:UIViewController){
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        for  b in button{
            let action = UIAlertAction.init(title: b as? String, style: UIAlertAction.Style.default, handler: { s in
                alertController.dismiss(animated: true, completion: nil)
            })
            alertController .addAction(action)
        }
        delegate.present(alertController, animated: true, completion: nil)
        
    }
    
    func makeToast(message:String)->Void{

        let s=MDToast()
        s.text = message as String
        s.backgroundColor = UIColor.blue
        s.frame = CGRect(x:0,y:0,width:300,height:40)
        s.layer.cornerRadius = 6.0
        s.backgroundColor = UIColor.black
        s.textColor = UIColor.white
        s.show()


    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //alert Message for error or Success
    func errorSuccessAler(_ title:String,_ message:String,_ sender:UIViewController) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
         alertController.addAction(action)
         sender.present(alertController, animated: true, completion: nil)
    
    }
    
    //getting user login info
    func getUserInfo()->AppUser?{

        if(UserDefaults.standard.value(forKey: "UserLogin") == nil){
            return nil
        }
        let result:String = UserDefaults.standard.value(forKey: "UserLogin") as! String
        return AppUser(JSONString:result)!

    }
    
    //Get Dashboard Info
    func getDashboardInfo()->Dashboardresult?{
        
        if(UserDefaults.standard.value(forKey: "UserLogin") == nil){
            
            return nil
        }
        let result:String = UserDefaults.standard.value(forKey: "UserLogin") as! String
        
        return Dashboardresult(JSONString:result)!
        
    }
    
    //Saving Auth Token
    func saveAuthToken(result:String){
        
        UserDefaults.standard.set(result, forKey: "AuthToken")
    }
    
    //getting Auth Token info
    func getAuthToken()->AppUser?{
        
        if(UserDefaults.standard.value(forKey: "AuthToken") == nil){
            return nil
        }
        let result:String = UserDefaults.standard.value(forKey: "AuthToken") as! String
        return AppUser(JSONString:result)!
    }
    
    
    //getting current Date
    func getCurrentDate()->String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
        
    }
    
    func performSegue(_ viewController:UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        _ = storyboard.instantiateViewController(withIdentifier: "Home")
        
        
    }
    
    func logout(_ viewController: UIViewController){        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "Start")
//        viewController.present(controller, animated: false, completion: nil)
//        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
//        self.navigationController?.popToRootViewController(animated: true)
          viewController.navigationController?.popToRootViewController(animated: true)

    }
    
    
    func webServicePosthttp22(urlString:String, params:Parameters,message:String,currentController:UIViewController,completion:  @escaping (_ result: String) -> Void){
        
        var myurlString =  urlString
        var headers: HTTPHeaders? = nil
        headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization":"bearer \(ApiUrls.AuthToken)"
        ]
        if !(urlString.contains("http")){
            myurlString = ApiUrls.serverUrl + urlString
        }
        
        print(myurlString)
        print(ApiUrls.AuthToken)
    
        print(params)
        
        Alamofire.request(myurlString, method: HTTPMethod.post, parameters: params,encoding: URLEncoding(destination: .httpBody), headers: headers).responseJSON {
            response in
            switch response.result {
            case .success:
                do {
                    
                    let convertedString = String(data: response.data!, encoding: String.Encoding.utf8)
                    
                    print(response.result.value ?? "0")
                    
                    if((convertedString?.contains("Authorization has been denied for this request"))!){
                        
                        let vc = UIApplication.shared.keyWindow?.rootViewController
                        self.logout(vc!)
                        
                    }else{
                        
                        completion(convertedString!)
                    }
                    
                }catch{
                    
                    completion("")
                }
                break
            case .failure(let error):
                
                
                if(error.localizedDescription == "The Internet connection appears to be offline."){
                    // other error statement
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline. Please try again.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                            completion("fail")
                        }))
                        currentController.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: "Internal server error occured. Please try again.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                            completion("fail")
                        }))
                        currentController.present(alert, animated: true, completion: nil)
                    }
                }
                
                
                print(error)
            }
      
        }
        
    }
    
    
    func webServicePosthttp(urlString:String, params:Parameters,message:String,currentController:UIViewController,completion:  @escaping (_ result: String) -> Void){
        
        var myurlString =  urlString
        var headers: HTTPHeaders? = nil
        headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization":"bearer \(ApiUrls.AuthToken)"
        ]
        if !(urlString.contains("http")){
            myurlString = ApiUrls.serverUrl + urlString
        }
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.gradient)
        
        
        
        Alamofire.request(myurlString, method: HTTPMethod.post, parameters: params,encoding: URLEncoding(destination: .httpBody), headers: headers).responseJSON {
            response in
            switch response.result {
            case .success:
                do {
                    
                    let convertedString = String(data: response.data!, encoding: String.Encoding.utf8)
                    
                    print(response.result.value ?? "0")
                    
                    if((convertedString?.contains("Authorization has been denied for this request"))!){
                        
                        let vc = UIApplication.shared.keyWindow?.rootViewController
                        self.logout(vc!)
                        
                    }else{
                        
                        completion(convertedString!)
                    }
                    
                }catch{
                    
                    completion("")
                }
                SVProgressHUD.dismiss()
                break
            case .failure(let error):
                
                if(error.localizedDescription == "The Internet connection appears to be offline."){
                    // other error statement
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline. Please try again.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                            completion("fail")
                        }))
                        currentController.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: "Internal server error occured. Please try again.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                            completion("fail")
                        }))
                        currentController.present(alert, animated: true, completion: nil)
                    }
                }
                SVProgressHUD.dismiss()
                
                print(error)
            }
            
        }
        
    }
    
    func webServicePosthttpUpload(urlString:String, params:Parameters,message:String,currentController:UIViewController,completion:  @escaping (_ result: String) -> Void){
        
        SVProgressHUD.show()
        let myurlString = ApiUrls.serverUrl + urlString
        
        
        //Header HERE
        let headers = [
            "Content-type": "multipart/form-data",
            "Content-Disposition" : "form-data"
        ]
        
        let imgData = params["ImageData"] as? Data
        let DocType = params["DocTypeName"] as? String
        let Doc1FileDataType = params["Doc1FileDataType"] as? String
        let Doc2FileDataType = params["Doc2FileDataType"] as? String
        
        
        var parms = params
        parms["ImageData"] = ""
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            if(Doc1FileDataType == "pdf")
            {
                multipartFormData.append(imgData!, withName: "DocBody",fileName: (params["DocBody"] as? String)! , mimeType: "file/pdf")
            }else{
               multipartFormData.append(imgData!, withName: "DocBody",fileName: (params["DocBody"] as? String)! , mimeType: "image/png")
            }
            
            
            multipartFormData.append(imgData!, withName: "DocBody",fileName: (params["DocBody"] as? String)! , mimeType: "image/png")
            
            if(DocType == "Driving License" || DocType == "National ID Card") || DocType == "Resident Card"{
                
                if(Doc2FileDataType == "pdf")
                {
                    let imgData2 = params["ImageData2"] as? Data
                    multipartFormData.append(imgData2!, withName: "DocBodyBack",fileName: (params["DocBodyBack"] as? String)! , mimeType: "file/pdf")
                    parms["ImageData2"] = ""
                }else{
                    let imgData2 = params["ImageData2"] as? Data
                    if imgData2 != nil {
                        //crash here 
                        multipartFormData.append(imgData2!, withName: "DocBodyBack",fileName: (params["DocBodyBack"] as? String)! , mimeType: "image/png")
                    }
                    
                    parms["ImageData2"] = ""
                }
                
                
//                let imgData2 = params["ImageData2"] as? Data
//                multipartFormData.append(imgData2!, withName: "DocBodyBack",fileName: (params["DocBodyBack"] as? String)! , mimeType: "image/png")
//                parms["ImageData2"] = ""
                
            }
            for (key, value) in parms
            {
                multipartFormData.append(((value as? String)?.data(using: String.Encoding.utf8)!)!, withName: key)
            }
            
        }, usingThreshold:UInt64.init(),
           to: myurlString, //URL Here
            method: .post,
            headers: headers, //pass header dictionary here
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    print("the status code is :")
                    
                    upload.uploadProgress(closure: { (progress) in
                        if #available(iOS 11.0, *) {
                            print(progress.fractionCompleted)
                            // SVProgressHUD.showProgress(Float(progress.fractionCompleted))
                            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading....")
                            
                            print("Completed \((progress.completedUnitCount))")
                            
                        } else {
                            // Fallback on earlier versions
                        }
                    })
                    
                    upload.responseJSON { response in
                        if (response.description.contains("response : nil") || response.description.contains("The request timed out.") || response.description.contains("The network connection was lost.") || response.description.contains("The Internet connection appears to be offline."))
                        {
                            SVProgressHUD.dismiss()
                            print(response.description)
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "", message: "The request time out due to slow internet connection, Please try again.", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                    completion("fail")
                                }))
                                currentController.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        else
                        {
                            print("the resopnse code is : \((response.response?.statusCode)!)")
                            print("the response is : \(response)")
                            let convertedString = String(data: response.data!, encoding: String.Encoding.utf8)
                            completion(convertedString!)
                            SVProgressHUD.dismiss()
                        }
                    }
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    if(error.localizedDescription == "The Internet connection appears to be offline."){
                        // other error statement
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "Internal server error occured. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    SVProgressHUD.dismiss()
                    print("the error is  : \(error.localizedDescription)")
                    break
                }
                SVProgressHUD.dismiss()
                
        })
        
    }
    
    
    
    func webServicePosthttpUploadPhoto(urlString:String, params:Parameters,message:String,currentController:UIViewController,completion:  @escaping (_ result: String) -> Void){
        
        SVProgressHUD.show()
        let myurlString = ApiUrls.serverUrl + urlString
        
        
        //Header HERE
        let headers = [
            "Content-type": "multipart/form-data",
            "Content-Disposition" : "form-data"
        ]
        
        let imgData = params["ImageData"] as? Data
        
        var parms = params
        parms["ImageData"] = ""
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(imgData!, withName: "PhotoBody",fileName: (params["PhotoBody"] as? String)! , mimeType: "image/png")
            
            
            for (key, value) in parms
            {
                print("Key : \(key) and Value : \(value)")
                multipartFormData.append(((value as? String)?.data(using: String.Encoding.utf8)!)!, withName: key)
            }
            
        }, usingThreshold:UInt64.init(),
           to: myurlString, //URL Here
            method: .post,
            headers: headers, //pass header dictionary here
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    print("the status code is :")
                    
                    upload.uploadProgress(closure: { (progress) in
                        if #available(iOS 11.0, *) {
                            print(progress.fractionCompleted)
                            // SVProgressHUD.showProgress(Float(progress.fractionCompleted))
                            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading....")
                            
                            print("Completed \((progress.completedUnitCount))")
                            
                        } else {
                            // Fallback on earlier versions
                        }
                    })
                    
                    upload.responseJSON { response in
                        if (response.description.contains("response : nil") || response.description.contains("The request timed out.") || response.description.contains("The network connection was lost.") || response.description.contains("The Internet connection appears to be offline."))
                        {
                            SVProgressHUD.dismiss()
                            print(response.description)
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "", message: "The request time out due to slow internet connection, Please try again.", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                    completion("fail")
                                }))
                                currentController.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        else
                        {
                            print("the resopnse code is : \((response.response?.statusCode)!)")
                            print("the response is : \(response)")
                            let convertedString = String(data: response.data!, encoding: String.Encoding.utf8)
                            completion(convertedString!)
                            SVProgressHUD.dismiss()
                        }
                    }
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    if(error.localizedDescription == "The Internet connection appears to be offline."){
                        // other error statement
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: "Internal server error occured. Please try again.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                                completion("fail")
                            }))
                            currentController.present(alert, animated: true, completion: nil)
                        }
                    }
                    SVProgressHUD.dismiss()
                    print("the error is  : \(error.localizedDescription)")
                    break
                }
                SVProgressHUD.dismiss()
                
        })
        
    }

    
    
    func getMonth(month:Int)-> String{
        
        
        if(month == 1){
            
            return "Jan"
            
        }else if(month == 2){
            
             return "Feb"
            
        }else if(month == 3){
            
             return "Mar"
            
        }else if(month == 4){
            
             return "Apr"
            
        }else if(month == 5){
            
             return "May"
            
        }else if(month == 6){
            
             return "Jun"
            
        }else if(month == 7){
            
             return "Jul"
            
        }else if(month == 8){
            
             return "Aug"
            
        }else if(month == 9){
            
             return "Sep"
            
        }else if(month == 10){
            
             return "Oct"
            
        }else if(month == 11){
            
             return "Nov"
            
        }else if(month == 12){
            
             return "Dec"
            
        }
        
        return ""
    }
    
    
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd/MM/yyyy"
        var birthdayDate = dateFormater.date(from: birthday)
        
        if(birthdayDate == nil){
            
            dateFormater.dateFormat = "MM/dd/yyyy"
            birthdayDate = dateFormater.date(from: birthday)
            
        }
        
        
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
    }
    
    func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,12}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    
    func ProfileCompleteCheck() -> Bool{
        
        let userInfo = self.getUserInfo()
        
        guard let _  = userInfo?.UserInfo?.Address , userInfo?.UserInfo?.Address != "" else {
            
            return false
        }
        
        guard let _ = userInfo?.UserInfo?.HouseNo , userInfo?.UserInfo?.HouseNo != "" else {
            
            return false
        }
        
        guard let _ = userInfo?.UserInfo?.City , userInfo?.UserInfo?.City != "" else {
            
            return false
        }
        
        guard let _ = userInfo?.UserInfo?.PostalCode , userInfo?.UserInfo?.PostalCode != "" else {
            
            return false
        }
        
        guard  let _ = userInfo?.UserInfo?.NationalityIsoCode , userInfo?.UserInfo?.NationalityIsoCode != "" else {
            
            return false
            
        }
        
        guard let _ = userInfo?.UserInfo?.CountryOfBirthIsoCode , userInfo?.UserInfo?.CountryOfBirthIsoCode != "" else {
            
            return false
            
        }
        
        guard let _ = userInfo?.UserInfo?.Email , userInfo?.UserInfo?.Email != "" else {
            
            return false
        }
        
        guard let _ = userInfo?.UserInfo?.Phone , userInfo?.UserInfo?.Phone != "" else {
            
            return false
        }
        
        guard let _ = userInfo?.UserInfo?.FullName , userInfo?.UserInfo?.FullName != "" else {
            
            return false
        }
        
        return true
    }

    
    
//    func primaryColor()->UIColor{
//
//        return UIColorHelper.color(withRGBA: "#b4a98d")
//    }
//
//    func primaryDarkColor() -> UIColor {
//
//        return UIColorHelper.color(withRGBA:"#aa9f83")
//    }
    
    func isValidPassword(_ password:String)-> Bool{
        
        let letters = CharacterSet.letters
        let number = CharacterSet.decimalDigits
        let symbol = CharacterSet.punctuationCharacters

        var isLettersAvailable = false
        var isDigitAvailable = false
        var isSymbolAvailable = false
        var isValidLength = false
        
        //let phrase = "Test case"
        let rangeNumber = password.rangeOfCharacter(from: number)
        let rangeLetters = password.rangeOfCharacter(from: letters)
        let rangeSymbol = password.rangeOfCharacter(from: symbol)

        // range will be nil if no letters is found
        if let test = rangeNumber {
           isDigitAvailable = true
        }
        else {
            isDigitAvailable = false
        }
        
        if let test = rangeLetters {
           isLettersAvailable = true
        }
        else {
            isLettersAvailable = false
        }
        
        if let test = rangeSymbol {
           isSymbolAvailable = true
        }
        else {
            isSymbolAvailable = false
        }
        
        if(password.count  < 6 )
        {
            isValidLength = false
        }else{
            isValidLength = true
            }
        
        if(isDigitAvailable && isLettersAvailable && isSymbolAvailable && isValidLength)
        {
            return true
        }else{
            return false
        }
    }
    
    func getCountryByISO3(_ countryIso3:String)->CountryList {
        
        let result:String = UserDefaults.standard.value(forKey: "AllCountriesList") as! String
        
        var country: CountryList? = nil
        
        if(result != "")
        {
            let AllCountries = CountryListResult(JSONString: result)
            for i in 0..<(AllCountries?.AceCountryList?.count)!
            {
                if(AllCountries?.AceCountryList![i].Iso3Code == countryIso3) //uc.getUserInfo().UserInfo.CountryIsoCode)
                {
                    country = AllCountries?.AceCountryList![i]
                    break
                }
            }
        }
        
        return country!
        
    }
    
}
